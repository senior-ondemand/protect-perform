var disqus_url;

function loadDisqus(category) {
	/* * * CONFIGURATION VARIABLES: THIS CODE IS ONLY AN EXAMPLE * * */
	var disqus_shortname = 'anymalstory';
	disqus_url = 'http://127.0.0.1:9090/disqus.html?detailView=' + category;

    /* * * DON'T EDIT BELOW THIS LINE * * */
    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
};
var eb = null;
var userUrl = '';
var userEmail = '';

$(function() {
	console.log("ready");
	open();
});

function open() {
	eb = new vertx.EventBus('http://protectnperform.com/eventbus');

	eb.onopen = function() {
		status();
		isSignIn();
	};

	eb.onclose = function() {
		eb = null;
	};
}

function status() {
	eb.send('campudus.session', {
		action : 'status',
		report : 'connections'
	}, function(reply) {
		if (reply.status === 'ok') {
			console.log('Connections: ' + reply.openSessions);
		} else {
			console.error('Failed' + reply.message);
		}
	});
}

function isSignIn() {
	eb.send('campudus.session', {
		action : 'get',
		sessionId : $.cookie('sessionId'),
		fields : 'data'
	}, function(reply) {
		if (reply.status === 'ok') {
			console.log(reply.data.data);
			$('#signUpInLink').fadeOut();
			$('#signOutLink').fadeIn();
			$('#deactivateLink').fadeIn();

			userEmail = reply.data.data.userEmail;
			userUrl = reply.data.data.userUrl;
			makeSignInScanTypeList();
			$('#url').val(userUrl);
		} else {
			console.log('failed: ' + reply.message);
			$('#signUpInLink').fadeIn();
			$('#signOutLink').fadeOut();
			$('#deactivateLink').fadeOut();
			makeSignOffScanTypeList();
		}
	});
}
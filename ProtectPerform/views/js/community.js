$(function() {
	socket.emit('getCategories');
	socket.emit('getPerformRank');
});

function communitySearchEffect() {
	
	socket.emit('community_search', {
		'searchType' : $('#search_select').val(),
		'keyword' : $('#search_keyword').val()
	});
	
	$(".marketing_div").hide('fade', 200, function() {
		$(".progress_div").show('fade', 200, function() {
			setTimeout(function() {
				$(".progress_div").hide('fade', 200, function() {
					$("#url_submit_btn").button('reset');
				});
			}, 2000);
		});
		$(".result_div").show('fade', 200);
	});
}

$("#community_search_btn").click(function() {
	
	$(".marketing_div").hide();
	$('#detail_view').hide();
	$('#search_list').hide();
	$('#protectDetailView').hide();
	$("#performDetailView").hide();
	$('#detailView').show().hide();
	
	communitySearchEffect();
	return false;
});

function runCategoriesMapProtect(data) {
	var categories_protect = $('#categoriesMap_protect');
	var content = "";
	var categoryCount = data.protectCategories.length;
	var testCaseCount;
	
	for (var i = 0; i < categoryCount; i++) {
		content += "<div class=\"panel panel-default\">"
				+ "<div class=\"panel-heading\">"
				+ "<h4 class=\"panel-title\">"
				+ "<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse"+i+"\">";
		
		// Rule title
		content += data.protectCategories[i].ruleName;
		
		content += "</a>"
				+ "</h4>"
				+ "</div>"
				+ "<div id=\"collapse"+i+"\" class=\"panel-collapse collapse\">"
				+ "<div class=\"panel-body\">";
		
		
		// inner list
		content += "<div class=\"list-group\">"
		
		testCaseCount = data.protectCategories[i].testCaseNameArray.length;  	
		for (var j = 0; j < testCaseCount; j++) {
			content += "<a onclick='communityProtectDetailView(\"";
			
			// url link in inner list
			content += data.protectCategories[i].testCaseNameArray[j];
			
			content += "\");' class=\"list-group-item\">";
			
			// testcase title in inner list
			content += data.protectCategories[i].testCaseNameArray[j];
			
			content += "</a>";
		}
		
		content += "</div>"
				+ "</div>"
				+ "</div>"
				+ "</div>";
	}
	
	categories_protect.append(content);
};

function runCategoriesMapPerform(data) {
	var categories_protect = $('#categoriesMap_perform');
	var content = "";
	var collapseNumber = 0;
	var categoryCount = data.performCategories.length;
	
	for (var i = 0; i < categoryCount; i++) {
		content += "<a onclick='communityPerformDetailView(\"";
		
		// url link
		content += data.performCategories[i].category;
		
		content += "\");' class=\"list-group-item\">";
			
		// Rule title
		content += data.performCategories[i].ruleName;
			
		content += "</a>";
	}
	
	categories_protect.append(content);
};

function runSearch(data) {
	var search_list = $('#search_list');
	var content = "";
	var collapseNumber = 0;
	var protectResultDataCount = data.protectResultDataList.length;
	var performResultDataCount = data.performResultDataList.length;
	
	search_list.show();
		
	if (protectResultDataCount > 0) {
		content += "<h3>Protect</h3>"
				+ "<blockquote>"
				+ "<ul>";
		for (var i = 0; i < protectResultDataCount; i++) {
			content += "<li>"
					+ "<a onclick='communityProtectDetailView(\""; 
			
			// URL link
			content += data.protectResultDataList[i].category;
			
			content += "\");'><H4>";
			
			// Rule title
			content += data.protectResultDataList[i].title;
			
			content += "</H4></a>"
					+ "<small>";
			
			// Rule Description
			content += data.protectResultDataList[i].discription;
			
			content += "</small>";
			
			if (i <performResultDataCount-1)
				content += "<br><br>"
				
			content += "</li>"
		}
		content += "</ul>"
				+ "</blockquote>";
	}
	
	if (performResultDataCount > 0) {
		content += "<h3>Perform</h3>"
				+ "<blockquote>"
				+ "<ul>";
		for (var i = 0; i < performResultDataCount; i++) {
			content += "<li>"
				+ "<a onclick='communityPerformDetailView(\"";
			
			// url link
			content += data.performResultDataList[i].category;
			
			content += "\");'><H4>";
			
			// Rule title
			content += data.performResultDataList[i].title;
			
			content += "</H4></a>"
					+ "<small>";
			
			// Rule Description
			content += data.performResultDataList[i].description;
			
			content += "</small>";
			
			if (i <performResultDataCount-1)
				content += "<br><br>"
				
			content += "</li>"
		}
		content += "</ul>"
				+ "</blockquote>";
	}
	
	if ((protectResultDataCount == 0) && (performResultDataCount == 0)) {
		content += "<h3>Not Found</h3>"
				+ "<h5>Search type : "+$('#search_select').val()+"</h5>"
				+ "<h5>Search Keyword : "+$('#search_keyword').val()+"</h5>";
	}
	search_list.html(content);
};

function communityProtectDetailView(category) {

	socket.emit('getDetailInfo', {
		'type' : 'PROTECT',
		'category' : category
	});
	
	$(".result_div").show('fade', 200);
	$(".marketing_div").hide();
	$('#search_list').hide();
	$("#performDetailView").hide();
	$('#detailView').show();
	$('#protectDetailView').show();
	
	$("#protectUrl").html("N/A");
}

function communityPerformDetailView(category) {

	socket.emit('getDetailInfo', {
		'type' : 'PERFORM',
		'category' : category
	});
	
	$("#performTitle").empty();
	$("#performRemedy").empty();
	
	$(".result_div").show('fade', 200);
	$(".marketing_div").hide();
	$('#search_list').hide();
	$('#protectDetailView').hide();
	$('#detailView').show();
	$("#performDetailView").show();
	
	$("#protectUrl").html("N/A");
}

function communityPerformUserRank(data) {
	var content = "";
	var performRankCount = data.performRank.length;

	content = "<li class=\"rank-list-group-item active\"><H4>User Site Top 10!</H4></li>"
	for ( var i = 0; i < performRankCount; i++) {
		content += "<li class=\"rank-list-group-item\"><img class=\"rank-img\" src=\"images/rank/rank_"
				+ (i + 1) + ".png\"><a target=\"_blank\" href=\"http://";
		content += data.performRank[i].key;
		content += "\">"
		content += data.performRank[i].value + " " + data.performRank[i].key;
		content += "</a></li>";
	}
	$("#user_site_top10").html(content);
}

function communityPerformSpecialRank(data) {
	var content = "";
	var performRankCount = data.performRank.length;

	content = "<li class=\"rank-list-group-item active\"><H4>World's Famous Top 10!</H4></li>"
	for ( var i = 0; i < performRankCount; i++) {
		content += "<li class=\"rank-list-group-item\"><img class=\"rank-img\" src=\"images/rank/rank_"
				+ (i + 1) + ".png\"><a target=\"_blank\" href=\"http://";
		content += data.performRank[i].key;
		content += "\">"
		content += data.performRank[i].value + " " + data.performRank[i].key;
		content += "</a></li>";
	}
	$("#world_famous_site_top10").html(content);
}
function scrolling(target_id){
	$('body, html').animate({ scrollTop: $(target_id).offset().top }, 1000);
};

$(function() {
	// turn treeview on. (settings, protect, perform tree)
	$("#setting_tree").treeview();
	$("#protect_tree").treeview();
	$("#perform_tree").treeview();
	
	// when you start protect&perform-processing
	function runEffect() {
		var options = {};

		var target_url = $("#url").val();
		var scan_type = $('#scanTypeDiv').children('Button').text().trim();
		var protect_list = [];
		var perform_list = [];
		var data;

		baseinfo["targetUrl"] = target_url;
		
		socket.emit('getHistoryData', {
			'user_email' : userEmail,
			'target_url' : baseinfo["targetUrl"]
		});
		
		$('#main_tab a[href="#protect_result_id"]').tab('show');
		
				// remove all trees
		$("#protect_tree").empty();
		$("#perform_tree").empty();
		$("#report_result_li_id").addClass("disabled");

		for (var i = 0; i < 10; i++) {
			if (scan_type == 'Perform') {
				protect_list.push(0);
			}
			else {
				if ($('#protect' + i).is(':checked'))
					protect_list.push(1);
				else
					protect_list.push(0);
			}			
		}
		
		for (var i = 0; i < 23; i++) {
			if (scan_type == 'Protect') {
				perform_list.push(0);
			}
			else {
				if ($('#perform' + i).is(':checked'))
					perform_list.push(1);
				else
					perform_list.push(0);
			}
		}
		data = {
			'testRules' : {
				'protect' : protect_list,
				'perform' : perform_list
			},
			'crawling' : 'options',
			'scheduling' : 'options'
		}
		
		console.log("start");
		socket.emit('search', {
			'user_id' : 'user id',
			'target_url' : target_url,
			'scan_type' : scan_type,
			'setting' : data
		});
		console.log("end");
		console.log("end");
		console.log("end");
		$(".marketing_div").hide('fade', 200, function() {
			$(".progress_div").show('fade', 200);
			$("#protectDetailView").hide();
			$("#performDetailView").hide();
			$(".result_div").show('fade', 200, function() {
				$("#protect_PPLogo").show('fade', 2000);
				$("#perform_PPLogo").show('fade', 2000);
			});
		});
	};
	
	function runCategoriesMap() {
		
	}

	$("#url_submit_btn").click(function() {
		$("#url_submit_btn").button('loading');
		
		$("#protectLogs").empty();
		
		runEffect();
		return false;
	});
	
	$("#scanner_btn").click(function() {
		location.href='/';
		return false;
	});
	
	$("#community_btn").click(function() {
		location.href='/community.html';
		return false;
	});
	
});

function protectTreeAdd(data) {
	var folder = $("#protect_tree").find("#ruleName" + data.ruleName).length;

	if (folder <= 0) {
		treeAddSubFolder('protect_tree', data);
	}
	protectTreeAddSubfile(data, 'subRuleName' + data.ruleName);
	
	$("#protect_tree").treeview();
}

function performTreeAdd(data) {
	var folder = $("#perform_tree").find("#title" + data.title).length;

	if (folder <= 0) {
		performTreeAddSubfile(data, 'perform_tree');
	}
}

function treeAddSubFolder(superId, content) {
	// add to "superId" div

	var newSub = $(
			"<li id='ruleName" + content.ruleName + "'><span class='folder'>"
					+ content.ruleTitle + "</span></li>")
			.appendTo("#" + superId);
	$("<ul id='subRuleName" + content.ruleName + "'></ul>").appendTo(
			"#ruleName" + content.ruleName);
	$("#" + superId).treeview({
		add : newSub
	});
};

function protectTreeAddSubfile(data, superId) {
	// add to "superId" div
	var gradeLevel = data.level;
	if (!data.isVerification) {
		gradeLevel = "Good";
	}
	
	var string = "<li><a onclick='protectDetailView(\"" + data.category
			+ "\");'><span class='file' style='background:url(images/"
			+ gradeLevel + ".png) 0 0 no-repeat;'>" + data.title
			+ "</span></a></li>";
	var newSub = $(string).appendTo("#" + superId);
	$("#" + superId).treeview({
		add : newSub
	});
};

function getGradeLevel(grade) {
	if (grade > 90) {
		return "Good";
	} else if (grade > 70) {
		return "Low";
	} else if (grade > 40) {
		return "Medium";
	} else {
		return "High";
	}
}

function performTreeAddSubfile(data, superId) {
	// add to "superId" div
	var gradeLevel = getGradeLevel(data.grade);
	
	var string = "<li><a onclick='performDetailView(\"" + data.category
			+ "\");'><span class='file' style='background:url(images/"
			+ gradeLevel + ".png) 0 0 no-repeat;'>" + data.title
			+ "</span></a></li>";
	var newSub = $(string).appendTo("#" + superId);
	$("#" + superId).treeview({
		add : newSub
	});
	
	console.log(string);
};

function protectDetailView(category) {
	// front -> back
	$("#protectUrl").html(protect[category].url);
	
	socket.emit('getDetailInfo', {
		'type' : 'PROTECT',
		'category' : protect[category].category
	});
	
	$("#protect_PPLogo").hide();
	$("#protectDetailView").show();
}

function performDetailView(category) {
	// front -> back
	var content = perform[category].title;
	var gradeLevel = getGradeLevel(perform[category].grade);
	if(gradeLevel == "High") {
		content += "<span style='float:right;' class=\"label label-danger\">Low</span>";
	} else if(gradeLevel == "Medium") {
		content += "<span style='float:right;' class=\"label label-warning\">Medium</span>";
	} else if(gradeLevel == "Low") {
		content += "<span style='float:right;' class=\"label label-info\">Good</span>";
	} else if(gradeLevel == "Good") {
		content += "<span style='float:right;' class=\"label label-success\">Excellent</span>";
	}
	
	$("#performTitle").html(content);
	$("#performResult").html(perform[category].result);
	$("#performGrade").html(" - " + perform[category].grade + "grade");
	$("#performRemedy").html(perform[category].solution);
	
	socket.emit('getDetailInfo', {
		'type' : 'PERFORM',
		'category' : perform[category].category
	});
	
	$("#perform_PPLogo").hide();
	$("#performDetailView").show();
}

function protectDetailViewAdd(data) {
	// back -> front
	var content;
	var classificationCount = data.classificationValueArray.length;
	var referenceCount = data.referenceValueArray.length;
	
	$("#loadingPerformDisqus").empty();
	$("#loadingProtectDisqus").html("<div id=\"disqus_thread\"></div>");
	loadDisqus(data.category);
	
	content = data.title;
	if(data.level == "High") {
		content += "<span style='float:right;' class=\"label label-danger\">High</span>";
	} else if(data.level == "Medium") {
		content += "<span style='float:right;' class=\"label label-warning\">Medium</span>";
	} else if(data.level == "Low") {
		content += "<span style='float:right;' class=\"label label-default\">Low</span>";
	}
		
	$("#protectTitle").html(content);
	$("#protectTestCaseTitle").text(data.testCaseTitle);
	$("#protectCategory").text(data.category);
	$("#protectAttackPattern").text(data.attackPattern);
	$("#protectVerificationPattern").text(data.verificationPattern);
	$("#protectDescription").html(data.description);
	$("#protectRemedy").html(data.remedy);
	
	content = "<TR><TH>#</TH><TH>Classification</TH></TR>";
	for (var i = 0; i < classificationCount; i++) {
		content += "<TR><TD>"
				+ (i+1)
				+ "</TD><TD>"
				+ "<a target=\"_blank\" href=\"";
		// URL link
		content += data.classificationValueArray[i];		
		content += "\">";
		// Rule title
		content += data.classificationNameArray[i];
		content += "</a>"
				+ "</TD></TR>";
	}
	$("#protectClassification").html(content);
	console.log(content);
	
	content = "<ul>";
	for (var i = 0; i < referenceCount; i++) {
		content += "<li>"
				+ "<a target=\"_blank\" href=\"";
		// URL link
		content += data.referenceValueArray[i];		
		content += "\">";
		// Rule title
		content += data.referenceNameArray[i];
		content += "</a>"
				+ "</li>";
	}
	content += "</ul>";
	$("#protectReference").html(content);
	
	console.log(content);
}

function performDetailViewAdd(data) {
	// back -> front
	var content;
	var referenceCount = data.referenceValueArray.length;
	
	$("#loadingProtectDisqus").empty();
	$("#loadingPerformDisqus").html("<div id=\"disqus_thread\"></div>");
	loadDisqus(data.category);
	
	console.log($("#performTitle"));
	
	if($("#performTitle").is(':empty'))
		$("#performTitle").html(data.title);
	$("#performCategory").html(data.category);
	$("#performDescription").html(data.description);
	if($("#performRemedy").is(':empty'))
		$("#performRemedy").html(data.remedy);
		
	content = "<ul>";
	for (var i = 0; i < referenceCount; i++) {
		content += "<li>"
				+ "<a target=\"_blank\" href=\"";
		// URL link
		content += data.referenceValueArray[i];		
		content += "\">";
		// Rule title
		content += data.referenceNameArray[i];
		content += "</a>"
				+ "</li>";
	}
	content += "</ul>";
	$("#performReference").html(content);
	
	console.log(content);
}

function addProtectLogs(data) {
	$("#protectLogs").append("<li>"+data+"</li>");
}

function updateProgressBar() {
	var testCases = baseinfo['receiveTestCases'];
	var percentages;
	
	testCases++;
	percentages = (testCases / baseinfo['totalTestCases']) * 100;
	
	$(".progress-bar").css("width", percentages+"%");
	
	if(percentages < baseinfo['totalTestCases'])
		baseinfo['receiveTestCases'] = testCases;
	else{
		$(".progress-bar").css("width", 0);
		$(".progress_div").hide();
		$("#url_submit_btn").button('reset');
		isCreated = 0;
		$("#report_result_li_id").removeClass("disabled");
		
		socket.emit('search_finished', {
		});
	}
}
var isCreated = 0;

$(function() {
			
	$("#report_result_button").click(function() {
		
		if(isCreated == 0) {
			$( "#report_result_id" ).empty();
			$( "#report_result_id" ).load( "sub/report.html", function() {
				
				var detected_pro_list = "";
				var unit = new Object();
				var test_data = [ ['rule1', 20], ['rule2', 20], ['rule3', 20], ['rule4', 20], ['rule5', 20] ];
				var test_bar_data3 = [{
	                name: 'Your test site',
	                data: [5, 5, 10, 10, 5, 5, 10, 10, 5, 5]
	            }, {
	                name: 'All tests',
	                data: [10, 10, 5, 5, 10, 10, 5, 5, 10, 10]
	            }, {
	                name: 'Best sites',
	                data: [5, 5, 10, 10, 5, 5, 10, 10, 5, 5]
	            }];
				var test_bar_data2 = [{
					name: 'Your test site',
					data: [90, 80, 70, 60, 50, 40, 30, 20, 10, 0]
				}];
				var test_bar_data = [];
				
				var protect_rule = {};
				var protect_total_rule_data = [];
				var protect_level = [];
				var protect_total_level_data = [];
				var protect_score = 0;
				var categories = [];
				var perform_score = 0;
				var protect_categories = [];
				var size_categories = ['Javascript', 'CSS', 'Image'];
				var perform_score_data = [];
				var score_data = {};
				var perform_request = [];
				var jsAvgSize = 0;
				var cssAvgSize = 0;
				var imgAvgSize = 0;
				var avg_data = [];
				
				var test_bar_data_our = {};
				var test_bar_data_all = {};
				
				test_bar_data_our.name = 'Your test site';
				test_bar_data_all.name = 'All sites';
				test_bar_data_our.data = [];
				test_bar_data_all.data = [3,5,2,4,4,3,1,2,2,1];
				
				score_data.data = [];
				score_data.name = "Score";
				
				for( unit in perform ){
					categories.push(perform[unit].title);
					score_data.data.push(parseInt(perform[unit].grade));
					perform_score = parseInt(perform_score) + parseInt(perform[unit].grade);
				}
				
				for( unit in protect ){
					if( protect[unit].isVerification == true){
						if( protect[unit].ruleTitle in protect_rule ){
							protect_rule[protect[unit].ruleTitle]++;
						}
						else
						{
							protect_rule[protect[unit].ruleTitle] = 1;
						}
						
						if( protect[unit].level in protect_level ){
							protect_level[protect[unit].level]++;
						}
						else
						{
							protect_level[protect[unit].level] = 1;
						}
					}
					else{
						protect_score++;
					}
				}
				
				//calc score
				
				protect_score = protect_score / Object.keys(protect).length * 100;
				protect_score = Math.round(protect_score);
				perform_score = parseInt(perform_score) / Object.keys(perform).length;
				perform_score = Math.round(perform_score);
				
				for( unit in protect_rule ){
					protect_categories.push(unit);
					protect_total_rule_data.push([unit, protect_rule[unit]]);
					test_bar_data_our.data.push(protect_rule[unit]);
				}
				
				for( var i = 0 ; i < 10 - test_bar_data_our.data.length; i++ ){
					test_bar_data_all.data.pop();
				}
				
				test_bar_data.push(test_bar_data_our);
				test_bar_data.push(test_bar_data_all);
				
				for( unit in protect_level ){
					protect_total_level_data.push([unit, protect_level[unit]]);
				}
				
				// report_perform_requests
				var total_requests = contentsList['jsList'].length + contentsList['cssList'].length + contentsList['imgList'].length;
				perform_request.push(['Javascript', contentsList['jsList'].length/total_requests*100]);
				perform_request.push(['CSS', contentsList['cssList'].length/total_requests*100]);
				perform_request.push(['Image', contentsList['imgList'].length/total_requests*100]);
				
				perform_score_data.push(score_data);
				
				for( var resource in contentsList['jsList']){
					jsAvgSize = parseInt(jsAvgSize) + parseInt(contentsList['jsList'][resource].size);
				}
				
				jsAvgSize = parseInt(jsAvgSize) / parseInt(contentsList['jsList'].length);
				
				for( var resource in contentsList['imgList']){
					imgAvgSize = parseInt(imgAvgSize) + parseInt(contentsList['imgList'][resource].size);
				}
				
				imgAvgSize = parseInt(imgAvgSize) / parseInt(contentsList['imgList'].length);
				
				for( var resource in contentsList['cssList']){
					cssAvgSize = parseInt(cssAvgSize) + parseInt(contentsList['cssList'][resource].size);
				}
				
				cssAvgSize = parseInt(cssAvgSize) / parseInt(contentsList['cssList'].length);
				
				avg_data = [{name: 'Average Size(Byte)', data: [Math.round(jsAvgSize), Math.round(cssAvgSize), Math.round(imgAvgSize)]}];
				
				////////////////////////////////////////////////////////////////////////////////////
				
				$("#report_total_score").append( (parseInt(protect_score) + parseInt(perform_score)) / 2 );
				$("#report_start_time").append(baseinfo['start']);
				$("#report_server_info").append(baseinfo['serverInfo']);
				$("#report_finish_time").append(baseinfo['finished_time']);
				
				//protect
				$("#report_protect_total_score").append(protect_score);
				create_pie_chart('report_total_rule', 'Rule Ratio', 800, protect_total_rule_data);
				create_pie_chart('report_level_ratio', 'Level Ratio', 0, protect_total_level_data);
				create_bar_chart('report_all_rule', 'The number of Detected Test Cases per Rule(Your test site vs All test)', 700, protect_categories, test_bar_data);
				
				
				//perform
				$("#report_perform_total_score").append(perform_score);
				create_bar_chart('report_perform_score', 'Perform Scores', 800, categories, perform_score_data);
				create_pie_chart('report_perform_requests', 'Request Contents', 0, perform_request);
				create_bar_chart('report_perform_requests_size', 'Average Individual Response Size', 300, size_categories, avg_data);
				
				// Added by jeongho
				perform_score
				socket.emit('setPerformUserRank', {
					'url' : $("#url").val(),
					'grade' : perform_score
				});
				
				for(var idx in perform){
					var content_html = '';
					
					content_html += '<div class="panel panel-default">';
					content_html += '<div class="panel-heading">';
					content_html += '<h3 class="panel-title">'+perform[idx].title+'</h3>';
					content_html += '</div>';
					content_html += '<div class="panel-body">';
					content_html += "<p>Grade : "+perform[idx].grade+"</p>";
					content_html += "<p> Result : "+perform[idx].result+"</p>";
					content_html += "</div></div>";
					
					$("#report_test_rule_summary").append(content_html);
					
					categories.push(perform[idx].title);
				}
			
			});
			
			isCreated = 1;
		}	
			
		
	});
});

function create_pie_chart(target, title, width, data){
	var chart;
	var options = {
			chart: {
				borderWidth: 1,
				borderColor: '#e2e2e2',
				renderTo: '',
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            backgroundColor: '#f5f5f5',
	            width:400
	        },
	        credits: {
	        	enabled: false
	        },
	        title: {
	            text: ''
	        },
	        tooltip: {
	    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    color: '#000000',
	                    connectorColor: '#000000',
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
	                }
	            }
	        },
	        series: [{
	        	type: 'pie',
	        	name: 'Ratio',
	        	data: []
	        }]
	};
	
	options.chart.renderTo = target;
	options.title.text = title;
	options.series[0].data = data;
	if(width != 0)
		options.chart.width = width;
	
	chart = new Highcharts.Chart(options);
}

function create_bar_chart(target, title, height, categories, data){
	var chart;
	var options = {
		chart: {
			borderWidth: 1,
			borderColor: '#e2e2e2',
			renderTo: '',
	        type: 'bar',
	        backgroundColor: '#f5f5f5',
	        width:700
	    },
	    credits: {
        	enabled: false
        },
	    title: {
	        text: ''
	    },
	    xAxis: {
	        categories: [],
	        title: {
	            text: null
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {  
	            text: '',
	            align: 'high'
	        },
	        labels: {
	            overflow: 'justify'
	        }
	    },
	    tooltip: {
	        valueSuffix: ''
	    },
	    plotOptions: {
	        bar: {
	            dataLabels: {
	                enabled: true
	            }
	        },
	        series: {
	        	pointWidth: 10
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'top',
	        x: -40,
	        y: 100,
	        floating: true,
	        borderWidth: 1,
	        backgroundColor: '#FFFFFF',
	        shadow: true
	    },
	    credits: {
	        enabled: false
	    },
	    series: []
	};
	
	options.chart.renderTo = target;
	options.chart.height = height;
	options.title.text = title;
	options.series = data;
	options.xAxis.categories = categories;

	chart = new Highcharts.Chart(options);
}


function create_pie_chart(target, title, width, data){
	var chart;
	var options = {
			chart: {
				borderWidth: 1,
				borderColor: '#e2e2e2',
				renderTo: '',
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            backgroundColor: '#f5f5f5',
	            width:400
	        },
	        credits: {
	        	enabled: false
	        },
	        title: {
	            text: ''
	        },
	        tooltip: {
	    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    color: '#000000',
	                    connectorColor: '#000000',
	                    format: '<b>{point.name}</b>: {point.percentage:.1f}%'
	                }
	            }
	        },
	        series: [{
	        	type: 'pie',
	        	name: 'Ratio',
	        	data: []
	        }]
	};
	
	options.chart.renderTo = target;
	options.title.text = title;
	options.series[0].data = data;
	if(width != 0)
		options.chart.width = width;
	
	chart = new Highcharts.Chart(options);
}

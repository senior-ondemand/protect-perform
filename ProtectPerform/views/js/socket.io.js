var socket;
var protect = {};
var perform = {};
var baseinfo = {};
var contentsList = {};
var history = {};

socket_connect();

function socket_connect() {
	socket = io.connect('http://protectnperform.com:9191');
	
	socket.on('baseInfo', function(data) {
		baseinfo['receiveTestCases'] = 0;
		baseinfo['serverInfo'] = data.serverinfo;
		baseinfo['start'] = data.start;
		baseinfo['performNumTestCases'] = data.performNumTestCases;
		baseinfo['protectNumTestCases'] = data.protectNumTestCases;
		baseinfo['totalTestCases'] = parseInt(data.performNumTestCases) + parseInt(data.protectNumTestCases);
		console.log(baseinfo['totalTestCases']);
	});
	
	socket.on('contentsList', function(data) {
		contentsList = data;
	});
	
	// PNP Scanner
	socket.on('PROTECT', function(data) {
		protect[data.category] = data;
		updateProgressBar();
		
		protectTreeAdd(data);
		addProtectLogs(data.url);
	});

	socket.on('PERFORM', function(data) {
		perform[data.category] = data;
		updateProgressBar();
		
		performTreeAdd(data);
	});

	socket.on('invalidURL', function() {
		// Display Error message about invalidURL
		console.log("invalid URL");
	});
	
	// Community
	socket.on('protectCategories', function(data) {
		if (typeof(runCategoriesMapProtect) != "undefined")
			runCategoriesMapProtect(data);
		if(typeof(settingSelectProtectRules) != "undefined")
			setSelectProtectRules(data);
	});
	
	socket.on('performCategories', function(data) {
		if (typeof(runCategoriesMapProtect) != "undefined")
			runCategoriesMapPerform(data);
		if(typeof(settingSelectPerformRules) != "undefined")
			setSelectPerformRules(data);
	});
	
	socket.on('searchResultDataList', function(data) {
		runSearch(data);
	});
	
	// Common
	socket.on('protectDetailInfoResult', function(data) {
		protectDetailViewAdd(data);
	});
	
	socket.on('performDetailInfoResult', function(data) {
		performDetailViewAdd(data);
	});
	
	socket.on('finished_time', function(data) {
		baseinfo['finished_time'] = data;
	});
	
	socket.on('performUserRank', function(data) {
		console.log(data);
		communityPerformUserRank(data);
	});
	
	socket.on('performSpecialRank', function(data) {
		console.log(data);
		communityPerformSpecialRank(data);
	});
	
	socket.on('setHistoryData', function(data) {
		history = data;
		console.log(data);
	});
}

function makeSignOffScanTypeList() {
	console.log("signOff");
		
	$('#scanTypeDiv').children('ul').children('li').remove();
	$('#scanTypeDiv').children('ul').append('<li><a href="#">Perform</a></li>');
	$('#scanTypeDiv').children('ul').append('<li class="divider"></li>');
	$('#scanTypeDiv').children('ul').append('<li><a href="#">SignIn for Protect</a></li>');
	$('#scanTypeDiv').children('Button').html('Perform <span class="caret"></span>');
}

function makeSignInScanTypeList() {
	console.log("signOn");
		
	$('#scanTypeDiv').children('ul').children('li').remove();
	$('#scanTypeDiv').children('ul').append('<li><a href="#" onClick="scanTypeOnChange(\'Perform\')">Perform</a></li>');
	$('#scanTypeDiv').children('ul').append('<li><a href="#" onClick="scanTypeOnChange(\'Protect\')">Protect</a></li>');	
	$('#scanTypeDiv').children('ul').append('<li><a href="#" onClick="scanTypeOnChange(\'PNP\')">P&P</a></li>');	
	$('#scanTypeDiv').children('Button').html('Perform <span class="caret"></span>');
}

function scanTypeOnChange(scanType) {
	console.log('onChange ' + scanType);
	
	$('#scanTypeDiv').children('Button').html(scanType +' <span class="caret"></span>');
	
	if (scanType == 'PNP' || scanType == 'Protect') {
		console.log('Protect Scan Abled');
		
		$('#url').val(userUrl);
	}
	else {
		console.log('Protect Scan Disabled');
	}
}
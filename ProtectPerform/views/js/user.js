function submitForm(action) {
	console.log(action);
	$.post('/' + action, $('#' + action + 'Form').serialize(), function(data) {
		console.log('Data From Server: ' + data);

		if (isSuccessFul(data)) {
			location.href = '/';
		} else {
			clearMsg(action);
			showErrorMsg(action, data);
		}
	});
}

function isSuccessFul(data) {
	if (data[0] == 't') {
		return true;
	}

	return false;
}

function clearMsg(action) {
	var id = action;

	$('#' + id + 'UserEmail').parent().removeClass('has-error');
	$('#' + id + 'UserEmail').parent().children('label').html('');
}

function showErrorMsg(action, data) {
	var splitData = data.split(':');
	var id = action + splitData[1];
	var msg = splitData[2];

	$('#' + id).parent().addClass('has-error');
	$('#' + id).parent().children('label').html(msg);
}

$(function() {

	$("#history_result_button").click(function() {

		$("#history_result_id").empty();
		$("#history_result_id").load("sub/history.html", function() {
			
			var categories = [];
			var protect_score_data = [];
			var perform_score_data = [];
			var js_size_data = [];
			var js_num_data = [];
			var css_size_data = [];
			var css_num_data = [];
			var img_size_data = [];
			var img_num_data = [];
			var score_data = {};
			var score_data2 = {};

			// get history data
			
			console.log(userEmail + baseinfo["targetUrl"]);

			score_data.data = [];
			score_data.name = "Score";
			score_data2.data = [];
			score_data2.name = "Score";
			
			for( var unit in history['perform'] ){
				for( var unit2 in history['perform'][unit] ){
					categories.push(unit2);
					score_data.data.push(parseInt(history['perform'][unit][unit2]));
				}
			}
			
			perform_score_data.push(score_data);
			console.log(perform_score_data);
			
			for( var unit in history['protect'] ){
				for( var unit2 in history['protect'][unit] ){
					score_data2.data.push(parseInt(history['protect'][unit][unit2]));
				}
			}
			
			protect_score_data.push(score_data2);
			console.log(protect_score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Number of JSs";
			for( var unit in history['js_num'] ){
				for( var unit2 in history['js_num'][unit] ){
					score_data.data.push(parseInt(history['js_num'][unit][unit2]));
				}
			}
			js_num_data.push(score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Average Size of JSs";
			for( var unit in history['js_size'] ){
				for( var unit2 in history['js_size'][unit] ){
					score_data.data.push(parseInt(history['js_size'][unit][unit2]));
				}
			}
			js_size_data.push(score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Number of CSSs";
			for( var unit in history['css_num'] ){
				for( var unit2 in history['css_num'][unit] ){
					score_data.data.push(parseInt(history['css_num'][unit][unit2]));
				}
			}
			css_num_data.push(score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Average Size of CSSs";
			for( var unit in history['css_size'] ){
				for( var unit2 in history['css_size'][unit] ){
					score_data.data.push(parseInt(history['css_size'][unit][unit2]));
				}
			}
			css_size_data.push(score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Number of IMGs";
			for( var unit in history['img_num'] ){
				for( var unit2 in history['img_num'][unit] ){
					score_data.data.push(parseInt(history['img_num'][unit][unit2]));
				}
			}
			img_num_data.push(score_data);
			
			score_data = {};
			score_data.data = [];
			score_data.name = "Average Size of IMGs";
			for( var unit in history['img_size'] ){
				for( var unit2 in history['img_size'][unit] ){
					score_data.data.push(parseInt(history['img_size'][unit][unit2]));
				}
			}
			img_size_data.push(score_data);
			
			
			create_basic_chart('history_protect_score', 'Protect Total Score History', categories, protect_score_data, '#FF0000', 'Score');
			create_basic_chart('history_perform_score', 'Perform Total Score History', categories, perform_score_data, '#FF0000', 'Score');

			create_basic_chart('history_js_num', 'Perform Number of Javascript History', categories, js_num_data, '#FFE400', 'The number');
			create_basic_chart('history_js_size', 'Perform Size of Javascript History', categories, js_size_data, '#FFE400', 'Bytes');
			create_basic_chart('history_css_num', 'Perform Number of CSS History', categories, css_num_data, '#8041D9', 'The number');
			create_basic_chart('history_css_size', 'Perform Size of CSS History', categories, css_size_data, '#8041D9', 'Bytes');
			create_basic_chart('history_img_num', 'Perform Number of Image History', categories, img_num_data, '#670000', 'The number');
			create_basic_chart('history_img_size', 'Perform Size of Image History', categories, img_size_data, '#670000', 'Bytes');
		});

	});
});

function create_basic_chart(target, title, categories, data, color, ytitle) {
	var chart;
	var options = {
		chart : {
			renderTo : '',
		},
		title : {
			text : '',
			x : -20
		// center
		},
		credits : {
			enabled : false
		},
		xAxis : {
			categories : []
		},
		yAxis : {
			title : {
				text : ''
			},
			plotLines : [ {
				value : 0,
				width : 1
			} ]
		},
		plotOptions: {
            series: {
                lineColor: ''            
            }
        },
		tooltip : {
			valueSuffix : ' Score'
		},
		legend : {
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'middle',
			borderWidth : 0
		},
		series : []
	};
	
	options.chart.renderTo = target;
	options.title.text = title;
	options.series = data;
	options.xAxis.categories = categories;
	options.plotOptions.series.lineColor = color;
	options.yAxis.title.text = ytitle;

	chart = new Highcharts.Chart(options);
}

function create_pie_chart(target, title, width, data) {
	var chart;
	var options = {
		chart : {
			borderWidth : 1,
			borderColor : '#e2e2e2',
			renderTo : '',
			plotBackgroundColor : null,
			plotBorderWidth : null,
			plotShadow : false,
			backgroundColor : '#f5f5f5',
			width : 400
		},
		credits : {
			enabled : false
		},
		title : {
			text : ''
		},
		tooltip : {
			pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				dataLabels : {
					enabled : true,
					color : '#000000',
					connectorColor : '#000000',
					format : '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		},
		series : [ {
			type : 'pie',
			name : 'Ratio',
			data : []
		} ]
	};

	options.chart.renderTo = target;
	options.title.text = title;
	options.series[0].data = data;
	if (width != 0)
		options.chart.width = width;

	chart = new Highcharts.Chart(options);
}

function create_bar_chart(target, title, height, categories, data) {
	var chart;
	var options = {
		chart : {
			borderWidth : 1,
			borderColor : '#e2e2e2',
			renderTo : '',
			type : 'bar',
			backgroundColor : '#f5f5f5',
			width : 700
		},
		credits : {
			enabled : false
		},
		title : {
			text : ''
		},
		xAxis : {
			categories : [],
			title : {
				text : null
			}
		},
		yAxis : {
			min : 0,
			title : {
				text : '',
				align : 'high'
			},
			labels : {
				overflow : 'justify'
			}
		},
		tooltip : {
			valueSuffix : ''
		},
		plotOptions : {
			bar : {
				dataLabels : {
					enabled : true
				}
			},
			series : {
				pointWidth : 10
			}
		},
		legend : {
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'top',
			x : -40,
			y : 100,
			floating : true,
			borderWidth : 1,
			backgroundColor : '#FFFFFF',
			shadow : true
		},
		credits : {
			enabled : false
		},
		series : []
	};

	options.chart.renderTo = target;
	options.chart.height = height;
	options.title.text = title;
	options.series = data;
	options.xAxis.categories = categories;

	chart = new Highcharts.Chart(options);
}

function create_pie_chart(target, title, width, data) {
	var chart;
	var options = {
		chart : {
			borderWidth : 1,
			borderColor : '#e2e2e2',
			renderTo : '',
			plotBackgroundColor : null,
			plotBorderWidth : null,
			plotShadow : false,
			backgroundColor : '#f5f5f5',
			width : 400
		},
		credits : {
			enabled : false
		},
		title : {
			text : ''
		},
		tooltip : {
			pointFormat : '{series.name}: <b>{point.percentage:.1f}</b>'
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				dataLabels : {
					enabled : true,
					color : '#000000',
					connectorColor : '#000000',
					format : '<b>{point.name}</b>: {point.percentage:.1f}%'
				}
			}
		},
		series : [ {
			type : 'pie',
			name : 'Ratio',
			data : []
		} ]
	};

	options.chart.renderTo = target;
	options.title.text = title;
	options.series[0].data = data;
	if (width != 0)
		options.chart.width = width;

	chart = new Highcharts.Chart(options);
}

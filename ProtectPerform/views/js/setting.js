$(function() {
	socket.emit('getCategories');
});

$('#settingSelectPerformRules').hide();
$('#settingSelectScheduling').hide();
$('#settingSelectProtectRules').show('fade', 2000);

function loadDetailView(data) {
	if (data == "protectRules") {
		$('#settingSelectPerformRules').hide('fade', 200);
		$('#settingSelectScheduling').hide('fade', 200);
		$('#settingSelectProtectRules').show('fade', 500);
		console.log(data);
	} else if (data == "performRules") {
		$('#settingSelectProtectRules').hide('fade', 200);
		$('#settingSelectScheduling').hide('fade', 200);
		$('#settingSelectPerformRules').show('fade', 500);
		console.log(data);
	} else if (data == "Scheduling") {
		$('#settingSelectPerformRules').hide('fade', 200);
		$('#settingSelectProtectRules').hide('fade', 200);
		$('#settingSelectScheduling').show('fade', 500);
	}
}

function setSelectProtectRules(data) {
	var settingSelectRules = $('#settingSelectProtectRules');
	var content = "";
	var categoryCount = data.protectCategories.length;
	var testCaseCount;

	for (var i = 0; i < categoryCount; i++) {
		content += "<p>" + "<label><input id=\"protect" + i
				+ "\" type=\"checkbox\" checked />";

		// Rule title
		content += data.protectCategories[i].ruleName;

		content += "</label></p>";
	}
	settingSelectRules.html(content);
}

function setSelectPerformRules(data) {
	var settingSelectRules = $('#settingSelectPerformRules');
	var content = "";
	var categoryCount = data.performCategories.length;
	var testCaseCount;

	for (var i = 0; i < categoryCount; i++) {
		content += "<p>" + "<label><input id=\"perform" + i
				+ "\" type=\"checkbox\" checked />";

		// Rule title
		content += data.performCategories[i].ruleName;

		content += "</label></p>";
	}
	settingSelectRules.html(content);
}
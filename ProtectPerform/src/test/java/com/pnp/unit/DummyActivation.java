package com.pnp.unit;

import org.junit.Ignore;

import com.pnp.data.Activation;

@Ignore
public class DummyActivation {

    public static final String TEST_ACTIVATION_LINK = "testActivationLink";
    public  static final String TEST_USER_EMAIL = "test@pnp.com";

    public static Activation getInstance() {
	return new Activation(TEST_ACTIVATION_LINK, TEST_USER_EMAIL);
    }

}

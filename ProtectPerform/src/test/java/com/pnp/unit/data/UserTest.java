package com.pnp.unit.data;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.unit.DummyUser;

public class UserTest {

    private User user;

    @Before
    public void setUp() throws Exception {
	user = new User();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void newWithParameters() throws Exception {
	User user = new User(DummyUser.TEST_EMAIL, DummyUser.TEST_PASSWORD, DummyUser.TEST_NAME, DummyUser.TEST_URL, DummyUser.TEST_ACTIVITY,
		DummyUser.TEST_ACTIVATION_LINK, DummyUser.TEST_SIGNUP_DATETIME, DummyUser.TEST_ACTIVATION_DATETIME, DummyUser.TEST_SCHEDULE_INTERVAL);
	assertThat("인자와 생성 후 getEmail()", user.getEmail(), is(equalTo(DummyUser.TEST_EMAIL)));
	assertThat("인자와 생성 후 getPassword()", user.getPassword(), is(equalTo(DummyUser.TEST_PASSWORD)));
	assertThat("인자와 생성 후 getName()", user.getName(), is(equalTo(DummyUser.TEST_NAME)));
	assertThat("인자와 생성 후 getUrl()", user.getUrl(), is(equalTo(DummyUser.TEST_URL)));
	assertThat("인자와 생성 후 getActivation()", user.getActivity(), is(equalTo(DummyUser.TEST_ACTIVITY)));
	assertThat("인자와 생성 후 getActivationLink()", user.getActivationLink(), is(equalTo(DummyUser.TEST_ACTIVATION_LINK)));
	assertThat("인자와 생성 후 getSignUpDateTime()", user.getSignUpDateTime(), is(equalTo(DummyUser.TEST_SIGNUP_DATETIME)));
	assertThat("인자와 생성 후 getActivationDateTime()", user.getActivationDateTime(), is(equalTo(DummyUser.TEST_ACTIVATION_DATETIME)));
	assertThat("인자와 생성 후 getScheduleInterval()", user.getScheduleInterval(), is(equalTo(DummyUser.TEST_SCHEDULE_INTERVAL)));
    }

    @Test
    public void getEmailWhenEmpty() throws Exception {
	assertThat("생성 직후 getEmail()", user.getEmail(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getPasswordWhenEmpty() throws Exception {
	assertThat("생성 직후 getPassword()", user.getPassword(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getNameWhenEmtpy() throws Exception {
	assertThat("생성 직후 getName()", user.getName(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getUrlWhenEmpty() throws Exception {
	assertThat("생성 직후 getUrl()", user.getUrl(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getActivationWhenEmpty() throws Exception {
	assertThat("생성 직후 getActivation()", user.getActivity(), is(equalTo(Activity.Inactivated.toString())));
    }

    @Test
    public void getActivationLinkWhenEmpty() throws Exception {
	assertThat("생성 직후 getActivationLink()", user.getActivationLink(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getSignUpDateTimeWhenEmpty() throws Exception {
	assertThat("생성 직후 getSignUpDateTime()", user.getSignUpDateTime(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getActivationDateTimeWhenEmpty() throws Exception {
	assertThat("생성 직후 getActivationDateTime()", user.getActivationDateTime(), is(equalTo(StringUtils.EMPTY)));
    }

    @Test
    public void getScheduleIntervalWhenEmpty() throws Exception {
	assertThat("생성 직후 getScheduleInterval()", user.getScheduleInterval(), is(equalTo(StringUtils.EMPTY)));
    }

}

package com.pnp.unit.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.controller.IllegalUserDataException;
import com.pnp.controller.IllegalUserDataException.UserDataField;

public class IllegalUserDataExceptionTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
	try {
	    throw new IllegalUserDataException(UserDataField.UserEmail, "Invalid Email Format");
	} catch (IllegalUserDataException e) {
	    System.out.println(e.getMessage());
	}
    }

}

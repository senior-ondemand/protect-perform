package com.pnp.unit.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.controller.UserController;
import com.pnp.data.User;

public class UserControllerTest {

    private UserController userController;

    @Before
    public void setUp() throws Exception {
	userController = new UserController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void signUp() throws Exception {
	User user = new User();
	user.setEmail("abnormalEmail");
	userController.signUp(user);
    }
    @Test
    public void signIn() throws Exception {

    }
    @Test
    public void signOut() throws Exception {

    }
    @Test
    public void deactivate() throws Exception {

    }
    @Test
    public void setAccount() throws Exception {

    }

}

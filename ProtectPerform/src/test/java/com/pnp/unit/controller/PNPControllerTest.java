package com.pnp.unit.controller;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import com.pnp.controller.PNPController;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;

public class PNPControllerTest {


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void goWithAllTestcaseInProtectAndPerform() {
    }

    @Test
    public void goWithAllTestCaseInProtect() {

    }

    @Test
    public void goWithZeroTestCaseInProtect() {

    }

    @Test
    public void goWithAllTestCaseInPerform() {

    }

    @Test
    public void goWithZeroTestCaseInPerform() {

    }

}
package com.pnp.unit;

import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.mindrot.jbcrypt.BCrypt;

import com.pnp.data.User;
import com.pnp.data.User.Activity;

@Ignore
public class DummyUser {

    public static final String TEST_EMAIL = "test@protectnperform.com";
    public static final String TEST_PASSWORD = "password";
    public static final String TEST_NAME = "senior";
    public static final String TEST_URL = "http://protectnperform.com";
    public static final String TEST_ACTIVITY = Activity.Inactivated.toString();
    public static final String TEST_ACTIVATION_LINK = "testActivationLink";
    public static final String TEST_SIGNUP_DATETIME = "2013-11-03 15:20:56";
    public static final String TEST_ACTIVATION_DATETIME = StringUtils.EMPTY;
    public static final String TEST_SCHEDULE_INTERVAL = StringUtils.EMPTY;

    private static final String SALT;

    static {
	SALT = BCrypt.gensalt();
    }

    public static User getInstanceWithHashPassword() {
	String hashPassword = BCrypt.hashpw(TEST_PASSWORD, SALT);
	return generateUser(hashPassword);
    }

    public static User getInstance() {
	return generateUser(TEST_PASSWORD);
    }

    private static User generateUser(String password) {
	User user = new User();
	user.setEmail(TEST_EMAIL);

	user.setPassword(password);
	user.setRePassword(password);
	user.setName(TEST_NAME);
	user.setUrl(TEST_URL);
	user.setActivity(TEST_ACTIVITY);
	user.setActivationLink(TEST_ACTIVATION_LINK);
	user.setSignUpDateTime(TEST_SIGNUP_DATETIME);
	user.setActivationDateTime(TEST_ACTIVATION_DATETIME);
	user.setScheduleInterval(TEST_SCHEDULE_INTERVAL);

	return user;
    }

}

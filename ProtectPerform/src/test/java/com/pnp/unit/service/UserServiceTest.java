package com.pnp.unit.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.controller.IllegalUserDataException;
import com.pnp.dao.UserDAO;
import com.pnp.data.Activation;
import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.module.DateLib;
import com.pnp.service.UserService;
import com.pnp.unit.DummyUser;

public class UserServiceTest {

    UserService userService;

    @Before
    public void setUp() throws Exception {
	userService = new UserService();
    }

    @After
    public void tearDown() throws Exception {
	UserDAO userDAO  =new UserDAO();
	userDAO.deactivate(DummyUser.getInstance());
    }

    @Test
    public void signUp() throws Exception {
	User newUser = DummyUser.getInstance();
	// 회원 가입하면서 기존 비밀번호가 암호화 되므로 user 로 로그인하면 안된다.
	assertThat("회원가입", userService.signUp(newUser), is(equalTo(true)));

	User userWithPlainPassword = DummyUser.getInstance();
	userService.deactivate(userWithPlainPassword);
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithAbnormalEmail() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setEmail("abnormalEmail");

	assertThat("회원가입 - 비정상 email", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithEmptyName() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setName(StringUtils.EMPTY);

	assertThat("회원가입 - 빈 이름", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithTooShortName() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setName("abc");

	assertThat("회원가입 - 3자 이름", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithTooLongName() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setName("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");

	assertThat("회원가입 - 52자 이름", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithEmptyPassword() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setPassword(StringUtils.EMPTY);
	newUser.setRePassword(StringUtils.EMPTY);

	assertThat("회원가입 - 빈 비밀번호", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithTooShortPassword() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setPassword("abcdefg");
	newUser.setRePassword("abcdefg");

	assertThat("회원가입 - 7자 비밀번호", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected = IllegalUserDataException.class)
    public void signUpWithPassword() throws Exception {
	User newUser = DummyUser.getInstance();
	newUser.setRePassword("newPasword");

	assertThat("회원가입 - 불일치 비밀번호", userService.signUp(newUser), is(equalTo(false)));
    }

    @Test(expected=IllegalUserDataException.class)
    public void signInWithNonActivatedUser() throws Exception {
	signUpWithDummyUser();

	User userWithPlainPassword = DummyUser.getInstance();
	assertThat("가입후 비인증 로그인", userService.signIn(userWithPlainPassword), is(nullValue()));
	userService.deactivate(userWithPlainPassword);
    }

    @Test
    public void signInWithActivatedUser() throws Exception {
	User user = signUpWithDummyUser();
	UserDAO userDao = new UserDAO();

	user.setActivity(Activity.Activated.toString());
	user.setActivationDateTime(DateLib.currentDateString());
	userDao.setAccount(user);

	User userWithPlainPassword = DummyUser.getInstance();
	User userFromRedis = userService.signIn(userWithPlainPassword);
	assertThat("가입후 인증 로그인 - getActivation()", userFromRedis.getActivity(), is(equalTo(Activity.Activated.toString())));
	assertThat("가입후 인증 로그인 - getActivationDateTime", userFromRedis.getActivationDateTime(),
		is(greaterThanOrEqualTo(userFromRedis.getSignUpDateTime())));
	userService.deactivate(userWithPlainPassword);
    }

    @Test(expected=IllegalUserDataException.class)
    public void signInWithNonExistUser() throws Exception {
	User nonExistUser = DummyUser.getInstance();
	assertThat("없는 계정 로그인", userService.signIn(nonExistUser), is(nullValue()));
    }

    @Test(expected=IllegalUserDataException.class)
    public void signInWithWrongPassword() throws Exception {
	User newUser = signUpWithDummyUser();

	User userWithPlainPassword = DummyUser.getInstance();
	userWithPlainPassword.setPassword("wrongPassword");

	assertThat("틀린 비밀번호 로그인", userService.signIn(userWithPlainPassword), is(nullValue()));
	userService.deactivate(newUser);
    }

    @Test
    public void setAccount() throws Exception {
	signUpWithDummyUser();

	String newPassword = "newPassword";
	String newName = "newName";
	String newUrl = "http://new.pnp.com";

	User user = DummyUser.getInstance();
	user.setPassword(newPassword);
	user.setRePassword(newPassword);
	user.setName(newName);
	user.setUrl(newUrl);

	assertThat("정보 수정", userService.setAccount(user), is(equalTo(true)));
	userService.deactivate(user);
    }

    @Test
    public void confirmAccountWithRightLink() throws Exception {
	User user = signUpWithDummyUser();
	UserDAO userDao = new UserDAO();
	User userFromRedis = userDao.getUserByEmail(user);

	Activation activation = new Activation();
	activation.setActivationLink(userFromRedis.getActivationLink());
	activation.setUserEmail(userFromRedis.getEmail());

	assertThat("계정 활성화", userService.activateAccount(activation), is(equalTo(true)));
	userService.deactivate(user);
    }

    @Test
    public void confirmAccountWithWrongLink() throws Exception {

    }

    @Test
    public void deactivate() throws Exception {
	signUpWithDummyUser();

	User userWithPlainPassword = DummyUser.getInstance();
	assertThat("회원탈퇴", userService.deactivate(userWithPlainPassword), is(equalTo(true)));
    }

    private User signUpWithDummyUser() throws IllegalUserDataException {
	User newUser = DummyUser.getInstance();
	userService.signUp(newUser);

	return newUser;
    }

}

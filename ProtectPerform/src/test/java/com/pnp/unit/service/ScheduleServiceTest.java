package com.pnp.unit.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.pnp.dao.UserDAO;
import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.data.User.Interval;
import com.pnp.module.DateLib;
import com.pnp.service.ScheduleService;
import com.pnp.unit.DummyUser;

public class ScheduleServiceTest {

    private ScheduleService scheduleService;
    private UserDAO userDao;

    @Before
    public void setUp() throws Exception {
	scheduleService = new ScheduleService();
	userDao = new UserDAO();
	userDao.truncate();
    }

    @After
    public void tearDown() throws Exception {
    }

    private User signUpAndActivateAndDailyScheduleWithDummyUser() {
	User user = DummyUser.getInstance();
	user.setActivationDateTime(DateLib.currentDateString());
	user.setActivity(Activity.Activated.toString());
	user.setScheduleInterval(Interval.Daily.toString());
	userDao.signUp(user);

	return user;
    }

    @Test
    public void startDaily() {
	signUpAndActivateAndDailyScheduleWithDummyUser();
	int jobCnt = scheduleService.startDaily();

	assertThat("Daily 작업 갯수", jobCnt, is(equalTo(1)));
    }

    @Ignore
    @Test
    public void startWeekly() {
	signUpAndActivateAndDailyScheduleWithDummyUser();
	int jobCnt = scheduleService.startWeekly();

	assertThat("Daily 작업 갯수", jobCnt, is(equalTo(1)));
    }

    @Ignore
    @Test
    public void startMonthly() {
	signUpAndActivateAndDailyScheduleWithDummyUser();
	int jobCnt = scheduleService.startMonthly();

	assertThat("Daily 작업 갯수", jobCnt, is(equalTo(1)));
    }

}

package com.pnp.unit.module.perform.yslow.y01;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.MinimizeHTTPRequests;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class MinimizeHTTPRequestWithGmarketTest {

    private static Perform perform;
    private static final String targetUrl = "http://pnp.com/perform/y11";
    private static PerformPreParser performPreParser;
    private static MinimizeHTTPRequests y01;

    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("extJsLinkCnt", 30);
	countList.put("extCssLinkCnt", 3);
	countList.put("cssSpriteFromExtCnt", 192);
	countList.put("cssSpriteFromBodyCnt", 0);
	countList.put("imageMapCnt", 6);
	countList.put("inLineImageFromExtCnt", 0);
	countList.put("inLineImageFromBodyCnt", 0);
	countList.put("grade", 40);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y01;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	y01 = (MinimizeHTTPRequests) perform;
    }

    @Test
    public void countExtJsLink() throws Exception {
	assertThat("외부 Js 링크", y01.getExtJsLinkCnt(), is(equalTo(countList.get("extJsLinkCnt"))));
    }

    @Test
    public void countExtCssLink() throws Exception {
	assertThat("외부 Css 링크", y01.getExtCssLinkCnt(), is(equalTo(countList.get("extCssLinkCnt"))));
    }

    @Test
    public void countCssSpriteFromExt() throws Exception {
	assertThat("외부 Css Sprite", y01.getCssSpriteFromExtCnt(), is(equalTo(countList.get("cssSpriteFromExtCnt"))));
    }

    @Test
    public void countCssSpriteFromBody() throws Exception {
	assertThat("내부 Css Sprite", y01.getCssSpriteFromBodyCnt(), is(equalTo(countList.get("cssSpriteFromBodyCnt"))));
    }

    @Test
    public void countImageMap() throws Exception {
	assertThat("Image Map", y01.getImageMapCnt(), is(equalTo(countList.get("imageMapCnt"))));
    }

    @Test
    public void countInLineImageFromExt() throws Exception {
	assertThat("외부 InLine Image", y01.getInLineImageFromExtCnt(), is(equalTo(countList.get("inLineImageFromExtCnt"))));
    }

    @Test
    public void countInLineImageFromBody() throws Exception {
	assertThat("내부 InLine Image", y01.getInLineImageFromBodyCnt(), is(equalTo(countList.get("inLineImageFromBodyCnt"))));
    }

    @Test
    public void grade() throws Exception {
	assertThat("총점", y01.getGrade(), is(equalTo(countList.get("grade"))));
    }

}
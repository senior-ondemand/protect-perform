
package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.AvoidFilters;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class AvoidFiltersTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/y21/y21.html";
    private static PerformPreParser performPreParser;
    private static AvoidFilters Y21;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("extJsFindFilterCnt", 1);
	countList.put("extCssFindFilterCnt", 1);
	countList.put("mainBodyFindFilterCnt", 1);
	countList.put("grade", 70);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y21;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y21 = (AvoidFilters) perform;
    }
    
    @Test
    public void countExtJsFindFilter() throws Exception {
	assertThat("외부 Js에 있는 Filter 개수", Y21.getExtJsFindFilter(), is(equalTo(countList.get("extJsFindFilterCnt"))));
    }

    @Test
    public void countExtCssFindFilter() throws Exception {
	assertThat("외부 Css에 있는 Filter 개수", Y21.getExtCssFindFilter(), is(equalTo(countList.get("extCssFindFilterCnt"))));
    }
    
    @Test
    public void countMainBodyFindFilter() throws Exception {
	assertThat("내부 Filter 개수", Y21.getMainBodyFindFilter(), is(equalTo(countList.get("mainBodyFindFilterCnt"))));
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y21.getGrade(), is(equalTo(countList.get("grade"))));
    }
   
    public void summary() throws Exception {
	System.out.println(perform.getResult());
	System.out.println(perform.getSolution());
	System.out.println(perform.getGrade());
    }
}

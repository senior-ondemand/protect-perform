package com.pnp.unit.module.protect.analyzer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicStatusLine;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.ProtectResult;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.module.protect.Protect;
import com.pnp.module.protect.VulnAnalyzer;
import com.pnp.service.InspectionType;

public class VulnAnalyzerWithA3Test {

    private static ArrayList<TestCase> testCases;
    private static VulnAnalyzer vulnAnalyzer;
    private static HashMap<String, Map<String, String>> testCasesMap;
    private HttpResponse mockedHttpResponse;
    private static ProtectResult protectResult;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	InspectionDAO dao = InspectionDAOFactory.create(InspectionType.PROTECT);
	JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createProtectRule(Protect.Rule.A3));
	Map<String, Rule> rules = dao.get(userSetting);
	testCases = rules.get(Protect.Rule.A3.getRuleName()).getTestCases();
	vulnAnalyzer = new VulnAnalyzer();
	testCasesMap = new HashMap<>();
	protectResult = new ProtectResult();

	for (TestCase testCase : testCases) {
	    testCasesMap.put(testCase.getTestCaseCategory(), testCase.getTestCaseVerification());
	}
    }

    @Before
    public void setUp() throws Exception {
	mockedHttpResponse = mock(HttpResponse.class);
	protectResult.setResponse(mockedHttpResponse);
    }

    @After
    public void tearDown() throws Exception {
    }

    private void setResponseBody(String html) throws UnsupportedEncodingException {
	when(mockedHttpResponse.getEntity()).thenReturn(new StringEntity(html));
    }

    private void setStatusCode(int statusCode) {
	StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), statusCode, null);
	when(mockedHttpResponse.getStatusLine()).thenReturn(statusLine);
    }

    @Test
    public void A3_PRO_XSS_ATK_001() throws Exception {
	setResponseBody("<script>alert(1);</script>");

	String testCaseId = "PRO:XSS:ATK:001";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_002() throws Exception {
	setResponseBody("javascript:alert(1);");

	String testCaseId = "PRO:XSS:ATK:002";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_003() throws Exception {
	setResponseBody("</div><script>alert(1);</script><div>");

	String testCaseId = "PRO:XSS:ATK:003";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_004() throws Exception {
	setStatusCode(200);

	String testCaseId = "PRO:XSS:ATK:004";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_005() throws Exception {
	setResponseBody("<script>alert(1);</script>");

	String testCaseId = "PRO:XSS:ATK:005";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_006() throws Exception {
	setResponseBody("<script>alert(1);</script>");

	String testCaseId = "PRO:XSS:ATK:006";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_007() throws Exception {
	setResponseBody("</td><script>alert(1);</script><td>");

	String testCaseId = "PRO:XSS:ATK:007";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_008() throws Exception {
	setResponseBody("</strong><script>alert(1);</script><strong>");

	String testCaseId = "PRO:XSS:ATK:008";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_009() throws Exception {
	setResponseBody("<script>alert(1);</script>");

	String testCaseId = "PRO:XSS:ATK:009";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A3_PRO_XSS_ATK_010() throws Exception {
	setResponseBody("</h2><script>alert(1);</script><h2>");

	String testCaseId = "PRO:XSS:ATK:010";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

}
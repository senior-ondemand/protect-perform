package com.pnp.unit.module;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.DateLib;

public class DateLibTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void currentDateString() {
	String currentMonth = "2013-11-";
	assertThat("현재 시각", DateLib.currentDateString(), is(startsWith(currentMonth)));
	assertThat("현재 시각 문자열 길이", DateLib.currentDateString().length(), is(equalTo(19)));
    }

}

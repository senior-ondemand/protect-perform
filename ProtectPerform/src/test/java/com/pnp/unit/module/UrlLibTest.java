package com.pnp.unit.module;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;

import org.apache.commons.validator.routines.EmailValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.UrlLib;

public class UrlLibTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void isValidUrlWithFtpUrl() {
	String url = "ftp://pnp.com";
	assertThat("FTP 주소", UrlLib.isValidUrl(url), is(equalTo(false)));
    }

    @Test
    public void isValidUrlWithInvalidUrl() {
	String url = "httpp://pnp.com";
	assertThat("비정상 주소", UrlLib.isValidUrl(url), is(equalTo(false)));
    }

    @Test
    public void absUrlWithDoubleSlashes() throws Exception {
	String baseUri = "http://pnp.com";
	String uri = "//pnp.com/test/api";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/test/api")));
    }

    @Test
    public void absUrlWithOneSlash() throws Exception {
	String baseUri = "http://pnp.com";
	String uri = "/test/api";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/test/api")));
    }

    @Test
    public void absUrlWithRelativeUrl() throws Exception {
	String baseUri = "http://pnp.com";
	String uri = "test/api";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/test/api")));
    }

    @Test
    public void absUrlWithExtraRelativeUrl() throws Exception {
	String baseUri = "http://pnp.com/controller/model";
	String uri = "test/api";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/controller/test/api")));
    }

    @Test
    public void absUrlWithExtraRelativeUrlAndExtension() throws Exception {
	String baseUri = "http://pnp.com/controller/test.html";
	String uri = "url/index.html";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/controller/url/index.html")));
    }

    @Test
    public void absUrlWithEndingSlash() throws Exception {
	String baseUri = "http://pnp.com/";
	String uri = "url/index.html";

	assertThat("절대 주소", UrlLib.absUrl(baseUri, uri), is(equalTo("http://pnp.com/url/index.html")));
    }

    @Test
    public void getHost() throws Exception {
	String url = "http://pnp.com/test/api";
	assertThat("호스트 정보", UrlLib.getHost(url), is(equalTo("pnp.com")));
    }

    @Test
    public void getHostWithPNP() throws Exception {
	String url = "http://protectnperform.com";
	assertThat("호스트 정보", UrlLib.getHost(url), is(equalTo("protectnperform.com")));
    }

    @Test
    public void getWhoisWithNaver() throws Exception {
	String domain = "naver.com";
	assertThat("Whois 정보", UrlLib.getWhois(domain).trim(), is(startsWith("Whois")));
    }

    @Test
    public void getWhoisWithPNP() throws Exception {
	String domain = "protectnperform.com";
	assertThat("Whois 정보", UrlLib.getWhois(domain).trim(), is(startsWith("Whois")));
	assertThat("Whois 정보", UrlLib.getWhois(domain).trim(), is(containsString("legshort@gmail.com")));
    }

    @Test
    public void getEmail() throws Exception {
	String str = "NAVER Corp.	white4818@nhn.com 6 Buljung-ro, Bundang-gu, Seongnam-si, Gyeonggi-do, 463-867, Korea";
	ArrayList<String> emailList = UrlLib.getEmailList(str);

	for (String email : emailList) {
	    assertThat("이메일 주소 추출", EmailValidator.getInstance().isValid(email), is(true));
	}
    }

    @Test
    public void getEmailWithPNP() throws Exception {
	String url = "http://protectnperform.com";
	String host = UrlLib.getHost(url);
	String whois = UrlLib.getWhois(host);
	ArrayList<String> emailList = UrlLib.getEmailList(whois);

	assertThat("이메일 주소 추출 - legshort@gmail.com", emailList.contains("legshort@gmail.com"), is(equalTo(true)));
    }

}

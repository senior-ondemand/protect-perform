
package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.DoNotScaleImagesInHTML;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class DoNotScaleImagesInHTMLTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y22/Y22.html";
    private static PerformPreParser performPreParser;
    private static DoNotScaleImagesInHTML Y22;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("totalCount", 6);
	countList.put("failCount", 4);
	countList.put("grade", 33);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y22;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y22 = (DoNotScaleImagesInHTML) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y22.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void totalCount() throws Exception {
	assertThat("전체 img src 테그", Y22.getTotalCount(), is(equalTo(countList.get("totalCount"))));
    }
    
    @Test
    public void countTotalFail() throws Exception {
	assertThat("크기를 줄여 출력한 이미지들의 개수", Y22.getTotalFailCount(), is(equalTo(countList.get("failCount"))));
    }
    
    public void summary() throws Exception {
	System.out.println(perform.getResult());
	System.out.println(perform.getSolution());
	System.out.println(perform.getGrade());
    }
}

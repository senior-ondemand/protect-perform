package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.PreParsedResponse;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.ReduceNumberOfDOMElements;

@RunWith(Parameterized.class)
public class ReduceNumberOfDOMElementsTest {

    private static final HashMap<String, Integer> countList;
    private static final String TEST_Y17_PATH = "test/perform/y17";
    private static final String targetUrl = "http://pnp.com/test/perform/y01/y01.html";

    private Perform perform;
    private ReduceNumberOfDOMElements y17;

    private final String fileName;
    private final String html;

    static {
	countList = new HashMap<>();
	countList.put("gmarket.html", 1966);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters() throws IOException {
	Collection<Object[]> parameters = new ArrayList<Object[]>();
	File[] files = new File(TEST_Y17_PATH).listFiles();

	for (File file : files) {
	    if (file.isFile()) {
		parameters.add(new Object[] { file.getName(), FileUtils.readFileToString(file) });
	    }
	}

	return parameters;
    }

    public ReduceNumberOfDOMElementsTest(String fileName, String html) {
	this.fileName = fileName;
	this.html = html;
    }

    @Before
    public void setup() {
	runPerform();
    }

    private void runPerform() {
	runPerform(targetUrl);
    }

    private void runPerform(String targetUrl) {
	PerformRuleType performType = PerformRuleType.Y17;
	perform = PerformFactory.create(performType);

	perform.initialize(performType, targetUrl, mockedPreParsedResponse());
	perform.exe();

	y17 = (ReduceNumberOfDOMElements) perform;
    }

    private PreParsedResponse mockedPreParsedResponse() {
	PerformPreParsedResponse mockedPreParsedResponse = mock(PerformPreParsedResponse.class);
	Document document = Jsoup.parse(html);
	when(mockedPreParsedResponse.getDocument()).thenReturn(document);

	return mockedPreParsedResponse;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void countDomElementCnt() throws Exception {
	assertThat("Dom 객체", y17.getDomElementCnt(), is(equalTo(countList.get(fileName))));
    }

    @Test
    public void grade() throws Exception {
    }

}

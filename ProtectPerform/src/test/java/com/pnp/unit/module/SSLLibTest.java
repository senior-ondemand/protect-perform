package com.pnp.unit.module;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.SSlLib;
import com.pnp.module.SSlLib.Version;

public class SSLLibTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getSslVersionWithNonSsl() {
	String host = "naver.com";
	assertThat("Naver - No SSl", SSlLib.getSslVersion(host), isEmptyString());
    }

    @Test
    public void getSslVersionWithSsl() {
	String host = "github.com";
	assertThat("Github", SSlLib.getSslVersion(host), is(equalTo("TLSv1")));
    }

    @Test
    public void isSecureVersionWithSSL2() throws Exception {
	assertThat("SSL 2.0", SSlLib.isSecureVersion(Version.SSL_2.getSslVersion()), is(equalTo(false)));
    }

    @Test
    public void isSecureVersionWithSSL3() throws Exception {
	assertThat("SSL 3.0", SSlLib.isSecureVersion(Version.SSL_3.getSslVersion()), is(equalTo(true)));
    }

    @Test
    public void isSecureVersionWithTLS1() throws Exception {
	assertThat("TLS 1.0", SSlLib.isSecureVersion(Version.TLS1.getSslVersion()), is(equalTo(true)));
    }

    @Test
    public void isSecureVersionWithTLS1p1() throws Exception {
	assertThat("TLS 1.1", SSlLib.isSecureVersion("TLSv1.1"), is(equalTo(true)));
    }

    @Test
    public void isSecureVersionWithTLS1p2() throws Exception {
	assertThat("TLS 1.2", SSlLib.isSecureVersion("TLSv1.2"), is(equalTo(true)));
    }

    @Test
    public void isSecureVersionWithNonSSL() throws Exception {
	assertThat("Non SSL", SSlLib.isSecureVersion("No SSL"), is(equalTo(false)));
    }

    @Test
    public void isSecureVersionWithEmpty() throws Exception {
	assertThat("Empty SSL Result", SSlLib.isSecureVersion(StringUtils.EMPTY), is(equalTo(false)));
    }

}

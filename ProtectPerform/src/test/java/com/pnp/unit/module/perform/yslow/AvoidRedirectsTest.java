package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.AvoidRedirects;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class AvoidRedirectsTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y12/Y12.php";
    private static PerformPreParser performPreParser;
    private static AvoidRedirects Y12;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("isRedirected", 1);
	countList.put("grade", 0);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y12;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y12 = (AvoidRedirects) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y12.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void checkIsRedirected() throws Exception {
	assertThat("redirect 여부", Y12.getIsRedirected(), is(equalTo(countList.get("isRedirected"))));
    }

}

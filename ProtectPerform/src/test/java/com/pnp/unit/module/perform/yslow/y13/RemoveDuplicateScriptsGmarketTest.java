package com.pnp.unit.module.perform.yslow.y13;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.data.html.ExtResource;
import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.RemoveDuplicateScripts;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class RemoveDuplicateScriptsGmarketTest {

    private static Perform perform;
    private static final String targetUrl = "http://www.gmarket.co.kr";
    private static PerformPreParser performPreParser;
    private static RemoveDuplicateScripts y13;

    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("duplicateJsCnt", 1);
	countList.put("duplicateCssCnt", 1);
	countList.put("grade", 100);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y13;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	y13 = (RemoveDuplicateScripts) perform;
    }

    @Test
    public void countDuplicateJsList() throws IOException {
	HashMap<String, ArrayList<ExtResource>> extJsMap = y13.getExtJsMap();

	for (Entry<String, ArrayList<ExtResource>> entry : extJsMap.entrySet()) {
	    ArrayList<ExtResource> extResourceList = entry.getValue();
	    assertThat("중복 Js", extResourceList.size(), is(equalTo(countList.get("duplicateJsCnt"))));
	}
    }

    @Test
    public void countDuplicateCssList() throws IOException {
	HashMap<String, ArrayList<ExtResource>> extJsMap = y13.getExtCssMap();

	for (Entry<String, ArrayList<ExtResource>> entry : extJsMap.entrySet()) {
	    ArrayList<ExtResource> extResourceList = entry.getValue();
	    assertThat("중복 Css", extResourceList.size(), is(equalTo(countList.get("duplicateCssCnt"))));
	}
    }

    @Test
    public void grade() throws Exception {
	assertThat("총점", y13.getGrade(), is(equalTo(countList.get("grade"))));
    }

}
package com.pnp.unit.module.perform.yslow.y11;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.MinifyJavaScriptAndCSS;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class MinifyJavaScriptAndCSSWithNaverTest {

    private enum FileType {
	ExtJs, ExtCss, IntJs, IntCss
    }

    private static Perform perform;
    private static final String targetUrl = "http://www.naver.com";
    private static PerformPreParser performPreParser;
    private static MinifyJavaScriptAndCSS y11;

    static {
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y11;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	y11 = (MinifyJavaScriptAndCSS) perform;
    }

    @Test
    public void minifyExtJsAndCss() throws Exception {
    }

    @Test
    public void grade() throws Exception {
    }

}
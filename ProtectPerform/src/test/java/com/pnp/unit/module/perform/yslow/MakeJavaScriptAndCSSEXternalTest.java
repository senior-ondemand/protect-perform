
package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.MakeJavaScriptAndCSSExternal;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class MakeJavaScriptAndCSSEXternalTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y09/Y09.html";
    private static PerformPreParser performPreParser;
    private static MakeJavaScriptAndCSSExternal Y09;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("inlineJsSize", 122);
	countList.put("inlineCssSize", 63);
	countList.put("grade", 11);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y09;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y09 = (MakeJavaScriptAndCSSExternal) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y09.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void countInlineJsLink() throws Exception {
	assertThat("내부 script 개수", Y09.getInlineJsSize(), is(equalTo(countList.get("inlineJsSize"))));
    }

    @Test
    public void countInlineCssLink() throws Exception {
	assertThat("내부 Css 개수", Y09.getInlineCssSize(), is(equalTo(countList.get("inlineCssSize"))));
    }
    
    public void summary() throws Exception {
	System.out.println(perform.getResult());
	System.out.println(perform.getSolution());
	System.out.println(perform.getGrade());
    }
}

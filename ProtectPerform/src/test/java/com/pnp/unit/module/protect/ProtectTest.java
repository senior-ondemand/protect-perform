package com.pnp.unit.module.protect;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.PreParsedResponse;
import com.pnp.data.ProtectPreParsedResponse;
import com.pnp.data.ProtectTestCase;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.data.html.FormTag;
import com.pnp.data.html.InputTag;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.module.parser.PreParser;
import com.pnp.module.protect.Protect;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class ProtectTest {

    private String targetURL;
    private TestCase testCase;
    private PreParsedResponse preParsedResponse;
    private Protect protect;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void protectWithAllTestCaseAtNaver() {
	int INFORMATIONAL = 100;
	Protect protect;
	ArrayList<TestCase> testCases;
	String targetURL;
	PreParser preParser;

	preParser = PreParserFactory.create(InspectionType.PROTECT);
	targetURL = "http://www.naver.com";
	preParser.setTargetURL(targetURL);
	preParser.preParseResponse();

	InspectionDAO dao = InspectionDAOFactory.create(InspectionType.PROTECT);

	JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createWithAllTestCaseInProtectAndPerform());
	Map<String, Rule> rules = dao.get(userSetting);

	testCases = rules.get(Protect.Rule.A2.getRuleName()).getTestCases();

	for (TestCase testCase : testCases) {
	    protect = new Protect(targetURL, testCase, preParser.getParsedResponse());
	    protect.exe();

	    assertThat(testCase.getTestCaseName() + " : ", protect.getProtectResult().getResponse().getStatusLine().getStatusCode(),
		    is(greaterThanOrEqualTo(INFORMATIONAL)));
	}

    }

    @Test
    public void testStaticData() {
	// Set targetURL
	targetURL = "http://www.naver.com";

	// Set testCaseAttack
	testCase = new ProtectTestCase();
	Map<String, String> testCaseAttack = new HashMap<String, String>();
	testCaseAttack.put("method", "get");
	testCaseAttack.put("value", ") or (a=a");

	testCase.setTestCaseAttack(testCaseAttack);

	// Set preParsedResponse
	preParsedResponse = new ProtectPreParsedResponse();
	ArrayList<FormTag> formTag = new ArrayList<FormTag>();
	ArrayList<InputTag> inputTag = new ArrayList<InputTag>();
	FormTag ft = new FormTag();
	InputTag it = new InputTag();
	it.setName("id");
	it.setName("password");
	inputTag.add(it);
	ft.setInputTags(inputTag);
	formTag.add(ft);
	((ProtectPreParsedResponse) preParsedResponse).setFormTagList(formTag);

	protect = new Protect(targetURL, testCase, preParsedResponse);
	protect.exe();
	// assertThat("ResponseHeader is null", protectPNPRequest.getResponse(),
	// is(notNullValue()));
	// assertThat("ResponseBody is null",
	// protectPNPRequest.getResponseBody(), is(notNullValue()));

	assertThat("ResponseHeader is null", protect.getProtectResult().getResponse(), is(notNullValue()));
    }
}

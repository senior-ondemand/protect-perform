package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.UseCookieFreeDomainsForComponents;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class UseCookieFreeDomainsForComponentsTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y04/Y04.html";
    private static PerformPreParser performPreParser;
    private static UseCookieFreeDomainsForComponents Y20;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("totalCount", 3);
	countList.put("problemCount", 3);
	countList.put("grade", 0);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y20;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y20 = (UseCookieFreeDomainsForComponents) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y20.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void totalCount() throws Exception {
	assertThat("전체 external 파일", Y20.getTotalCount(), is(equalTo(countList.get("totalCount"))));
    }
    
    @Test
    public void problemCount() throws Exception {
	assertThat("ETage 포함 파일", Y20.getProblemCount(), is(equalTo(countList.get("problemCount"))));
    }

}

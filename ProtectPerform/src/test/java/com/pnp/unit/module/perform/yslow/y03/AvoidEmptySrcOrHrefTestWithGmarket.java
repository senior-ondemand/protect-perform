package com.pnp.unit.module.perform.yslow.y03;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.AvoidEmptySrcOrHref;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class AvoidEmptySrcOrHrefTestWithGmarket {

    private static Perform perform;
    private static final String targetUrl = "http://www.gmarket.co.kr";
    private static PerformPreParser performPreParser;
    private static AvoidEmptySrcOrHref y03;

    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("emptyLinkFromJsCnt", 0);
	countList.put("emptyLinkFromBodyCnt", 31);
	countList.put("grade", 0);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y03;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	y03 = (AvoidEmptySrcOrHref) perform;
    }

    @Test
    public void countFromJs() throws Exception {
	assertThat("외부 + 내부 Js Empty link", y03.getEmptyLinkFromJsCnt(), is(equalTo(countList.get("emptyLinkFromJsCnt"))));
    }

    @Test
    public void countFromBody() throws Exception {
	assertThat("내부 Emtpy link", y03.getEmptyLinkFromBodyCnt(), is(equalTo(countList.get("emptyLinkFromBodyCnt"))));
    }

    @Test
    public void grade() throws Exception {
	assertThat("총점", y03.getGrade(), is(equalTo(countList.get("grade"))));
    }

}
package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.No404s;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class No404sTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y18/Y18.html";
    private static PerformPreParser performPreParser;
    private static No404s Y18;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("totalCount", 3);
	countList.put("problemCount", 3);
	countList.put("grade", 0);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y18;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y18 = (No404s) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y18.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void totalCount() throws Exception {
	assertThat("전체 external 파일", Y18.getTotalCount(), is(equalTo(countList.get("totalCount"))));
    }
    
    @Test
    public void problemCount() throws Exception {
	assertThat("404오류 파일", Y18.getProblemCount(), is(equalTo(countList.get("problemCount"))));
    }

}

package com.pnp.unit.module.parser;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.parser.PostParser;
import com.pnp.module.parser.PostParserFactory;
import com.pnp.module.parser.ProtectPostParser;
import com.pnp.service.InspectionType;

public class ProtectPostParserTest {

	private InspectionType inspectionType;

	@Before
	public void setUp() throws Exception {
		inspectionType = InspectionType.PROTECT;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		HttpClient httpclient = new DefaultHttpClient();

		CookieStore cookieStore = new BasicCookieStore();

		// Create local HTTP context
		HttpContext localContext = new BasicHttpContext();
		// Bind custom cookie store to the local context
		localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

		HttpResponse response = null;
		HttpGet first = new HttpGet("http://www.daum.net");
		first.addHeader("User-Agent",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36");
		try {
			response = httpclient.execute(first, localContext);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<String, String> verification = new HashMap<String, String>();

		verification.put("header_param0", "Cache-Control");
		verification.put("header_value0", "no-cache");
		verification.put("status_code", "200");
		verification.put("body", "<input");

		PostParser protectPostParser = PostParserFactory.create(inspectionType);
		protectPostParser.parseResponse(response, verification);

		protectPostParser.getProtectPostParsedResponse();
	}
}

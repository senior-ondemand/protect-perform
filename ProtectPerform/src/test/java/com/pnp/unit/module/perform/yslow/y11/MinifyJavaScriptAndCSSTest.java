package com.pnp.unit.module.perform.yslow.y11;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.html.ExtResource;
import com.pnp.data.html.IntResource;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.MinifyJavaScriptAndCSS;

@RunWith(Parameterized.class)
public class MinifyJavaScriptAndCSSTest {

    private enum FileType {
	ExtJs, ExtCss, IntJs, IntCss
    }

    private static final String TEST_Y11_PATH = "test/perform/y11";
    private static final String targetUrl = "http://pnp.com/test/perform/y11/y11.html";
    private static final HashMap<String, Integer> compressionRatioList;

    private Perform perform;
    private MinifyJavaScriptAndCSS y11;

    private final FileType fileType;
    private final String fileName;
    private final String html;
    private final int grade;

    static {
	compressionRatioList = new HashMap<>();
	compressionRatioList.put("_munge.js", 46);
	compressionRatioList.put("_string_combo.js", 48);
	compressionRatioList.put("_syntax_error.js", 22);

	compressionRatioList.put("background-position.css", 17);
	compressionRatioList.put("border-none.css", 39);
	compressionRatioList.put("box-model-hack.css", 21);

	compressionRatioList.put("GmarketIntJs1.html", 0);
	compressionRatioList.put("GmarketIntJs2.html", 22);
	compressionRatioList.put("GmarketIntJs3.html", 6);
	compressionRatioList.put("GmarketIntJs4.html", 31);
	compressionRatioList.put("GmarketIntJs5.html", 29);
	compressionRatioList.put("GmarketIntJs6.html", 12);
	compressionRatioList.put("GmarketIntJs7.html", 21);
	compressionRatioList.put("GmarketIntJs8.html", 21);
	compressionRatioList.put("GmarketIntJs38.html", 21);
	compressionRatioList.put("GmarketIntJs39.html", 12);
	compressionRatioList.put("GmarketIntJs40.html", 16);
	compressionRatioList.put("GmarketIntJs41.html", 24);
	compressionRatioList.put("GmarketIntJs42.html", 19);
	compressionRatioList.put("GmarketIntJs43.html", 0);
	compressionRatioList.put("GmarketIntJs44.html", 13);

	compressionRatioList.put("GmarketIntCss1.html", 15);
	compressionRatioList.put("GmarketIntCss2.html", 8);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters() throws IOException {
	Collection<Object[]> parameters = new ArrayList<Object[]>();
	File[] files = new File(TEST_Y11_PATH).listFiles();

	for (File file : files) {
	    if (file.isFile()) {
		parameters.add(new Object[] { getFileType(file), file.getName(), FileUtils.readFileToString(file),
			compressionRatioList.get(file.getName()) });
	    }
	}

	return parameters;
    }

    private static FileType getFileType(File file) {
	FileType fileType = null;
	String extension = FilenameUtils.getExtension(file.getName());

	if (extension.equals("js")) {
	    fileType = FileType.ExtJs;
	} else if (extension.equals("css")) {
	    fileType = FileType.ExtCss;
	} else if (extension.equals("html") && file.getName().contains("IntJs")) {
	    fileType = FileType.IntJs;
	} else if (extension.equals("html") && file.getName().contains("IntCss")) {
	    fileType = FileType.IntCss;
	} else {
	    throw new IllegalArgumentException("테스트 파일 오류: " + file.getAbsolutePath());
	}

	return fileType;
    }

    public MinifyJavaScriptAndCSSTest(FileType fileType, String fileName, String html, int grade) {
	this.fileType = fileType;
	this.fileName = fileName;
	this.html = html;
	this.grade = grade;
    }

    @Before
    public void setUp() throws Exception {
	PerformRuleType performType = PerformRuleType.Y11;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, mockedPreParsedResponse());
	perform.exe();

	y11 = (MinifyJavaScriptAndCSS) perform;
    }

    private PerformPreParsedResponse mockedPreParsedResponse() throws FileNotFoundException, IOException {
	PerformPreParsedResponse mockedPreParsedResponse = mock(PerformPreParsedResponse.class);

	ArrayList<ExtResource> extJsResourceList = new ArrayList<>();
	ArrayList<ExtResource> extCssResourceList = new ArrayList<>();
	ArrayList<IntResource> intJsResourceList = new ArrayList<>();
	ArrayList<IntResource> intCssResourceList = new ArrayList<>();

	switch (fileType) {
	case ExtJs:
	    setExtJs(extJsResourceList);
	    break;
	case ExtCss:
	    setExtCss(extCssResourceList);
	    break;
	case IntJs:
	    setIntJs(intJsResourceList);
	    break;
	case IntCss:
	    setIntCss(intCssResourceList);
	    break;

	default:
	    throw new IllegalArgumentException("잘못 된 파일 확장자" + fileType);
	}

	when(mockedPreParsedResponse.getExtJsResourceList()).thenReturn(extJsResourceList);
	when(mockedPreParsedResponse.getExtCssResourceList()).thenReturn(extCssResourceList);
	when(mockedPreParsedResponse.getIntJsResourceList()).thenReturn(intJsResourceList);
	when(mockedPreParsedResponse.getIntCssResourceList()).thenReturn(intCssResourceList);

	return mockedPreParsedResponse;
    }

    private void setExtJs(ArrayList<ExtResource> extJsResourceList) {
	ExtResource extResource = new ExtResource();
	extResource.setFileName(fileName);
	extResource.setResponseBody(html);
	extJsResourceList.add(extResource);
    }

    private void setExtCss(ArrayList<ExtResource> extCssResourceList) {
	ExtResource extCssResource = new ExtResource();
	extCssResource.setFileName(fileName);
	extCssResource.setResponseBody(html);
	extCssResourceList.add(extCssResource);
    }

    private void setIntJs(ArrayList<IntResource> intJsResourceList) {
	IntResource intJsResource = new IntResource();
	intJsResource.setResponseBody(html);
	intJsResourceList.add(intJsResource);
    }

    private void setIntCss(ArrayList<IntResource> intCssResourceList) {
	IntResource intJsResource = new IntResource();
	intJsResource.setResponseBody(html);
	intCssResourceList.add(intJsResource);
    }

    @Test
    public void minifyExtJsAndCss() throws Exception {
	int compressionRatio = 100 - y11.getGrade();
	assertThat("\"" + fileName + "\": 압축률", compressionRatio, is(equalTo(grade)));
    }

    @Test
    public void grade() throws Exception {
	assertThat("\"" + fileName + "\": 최종 점수", y11.getGrade(), is(equalTo(100 - grade)));
    }

}
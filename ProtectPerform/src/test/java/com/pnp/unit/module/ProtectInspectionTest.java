package com.pnp.unit.module;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.Rule;
import com.pnp.data.UserSetting;
import com.pnp.module.Inspection;
import com.pnp.module.InspectionFactory;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.module.parser.PreParser;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class ProtectInspectionTest {

    private static final InspectionType inspectionType = InspectionType.PROTECT;

    private UserSetting userSetting;
    private InspectionDAO inspectionDAO;
    private PreParser protectPreParser;

    private Inspection protectInspection;
    private JsonObjectGenerator jsonObjectGenerator;

    private Map<String, Rule> rules;

    @Before
    public void setUp() throws Exception {
	jsonObjectGenerator = new JsonObjectGenerator();
	userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createWithAllTestCaseInProtectAndPerform());

	inspectionDAO = InspectionDAOFactory.create(inspectionType);
	protectPreParser = PreParserFactory.create(inspectionType);
	protectPreParser.setTargetURL(userSetting.getTargetURL());
	rules = inspectionDAO.get(userSetting);
	protectPreParser.preParseResponse();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void runA1ProtectRule() {
//	protectInspection.initialize(userSetting.getTargetURL(), rules.get("A1"), preParser.getParsedResponse());
    }

}

package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.PutStyleSheetsAtTop;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class PutStyleSheetsAtTopTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y06/Y06.html";
    private static PerformPreParser performPreParser;
    private static PutStyleSheetsAtTop Y06;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("extCssOtherCount", 4);
	countList.put("grade", 60);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y06;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y06 = (PutStyleSheetsAtTop) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y06.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void checkExtCssOtherCount() throws Exception {
	assertThat("총점", Y06.getExtCssOtherCount(), is(equalTo(countList.get("extCssOtherCount"))));
    }
    
    public void summary() throws Exception {
	System.out.println(perform.getResult());
	System.out.println(perform.getSolution());
	System.out.println(perform.getGrade());
    }
}

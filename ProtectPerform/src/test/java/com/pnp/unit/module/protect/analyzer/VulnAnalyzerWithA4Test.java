package com.pnp.unit.module.protect.analyzer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.ProtectResult;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.module.protect.Protect;
import com.pnp.module.protect.VulnAnalyzer;
import com.pnp.service.InspectionType;

public class VulnAnalyzerWithA4Test {

    private static final String PASSWD = "passwd";
    private static final String SYSTEM = "system.ini";
    private static final String WEB = "web.xml";

    private static ArrayList<TestCase> testCases;
    private static VulnAnalyzer vulnAnalyzer;
    private static HashMap<String, Map<String, String>> testCasesMap;
    private HttpResponse mockedHttpResponse;
    private static ProtectResult protectResult;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	InspectionDAO dao = InspectionDAOFactory.create(InspectionType.PROTECT);
	JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createProtectRule(Protect.Rule.A4));
	Map<String, Rule> rules = dao.get(userSetting);
	testCases = rules.get(Protect.Rule.A4.getRuleName()).getTestCases();
	vulnAnalyzer = new VulnAnalyzer();
	testCasesMap = new HashMap<>();
	protectResult = new ProtectResult();

	for (TestCase testCase : testCases) {
	    testCasesMap.put(testCase.getTestCaseCategory(), testCase.getTestCaseVerification());
	}
    }

    @Before
    public void setUp() throws Exception {
	mockedHttpResponse = mock(HttpResponse.class);
	protectResult.setResponse(mockedHttpResponse);
    }

    @After
    public void tearDown() throws Exception {
    }

    private void setDownloadHeader(String fileName) {
	Header[] headers = { new BasicHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"") };
	when(mockedHttpResponse.getAllHeaders()).thenReturn(headers);
    }

    @Test
    public void A4_PRO_IOR_ATK_001() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:001";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_002() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:002";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_003() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:003";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_004() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:004";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_005() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:005";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_006() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:006";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_007() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:007";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_008() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:008";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_009() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:009";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_010() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:010";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_011() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:011";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_012() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:012";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_013() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:013";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_014() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:014";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_015() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:015";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_016() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:016";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_017() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:017";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_018() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:018";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_019() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:019";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_020() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:020";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_021() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:021";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_022() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:022";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_023() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:023";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_024() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:024";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_025() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:025";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_026() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:026";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_027() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:027";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_028() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:028";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_029() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:029";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_030() throws Exception {
	setDownloadHeader(PASSWD);

	String testCaseId = "PRO:IOR:ATK:030";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_031() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:031";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_032() throws Exception {
	setDownloadHeader(SYSTEM);

	String testCaseId = "PRO:IOR:ATK:032";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_033() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:033";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A4_PRO_IOR_ATK_034() throws Exception {
	setDownloadHeader(WEB);

	String testCaseId = "PRO:IOR:ATK:034";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

}
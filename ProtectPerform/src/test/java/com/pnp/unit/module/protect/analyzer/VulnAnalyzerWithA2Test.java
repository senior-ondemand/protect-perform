package com.pnp.unit.module.protect.analyzer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.ProtectResult;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.module.SSlLib;
import com.pnp.module.protect.Protect;
import com.pnp.module.protect.VulnAnalyzer;
import com.pnp.service.InspectionType;

public class VulnAnalyzerWithA2Test {

    private static ArrayList<TestCase> testCases;
    private static VulnAnalyzer vulnAnalyzer;
    private static HashMap<String, Map<String, String>> testCasesMap;
    private HttpResponse mockedHttpResponse;
    private static ProtectResult protectResult;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	InspectionDAO dao = InspectionDAOFactory.create(InspectionType.PROTECT);
	JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createProtectRule(Protect.Rule.A2));
	Map<String, Rule> rules = dao.get(userSetting);
	testCases = rules.get(Protect.Rule.A2.getRuleName()).getTestCases();
	vulnAnalyzer = new VulnAnalyzer();
	testCasesMap = new HashMap<>();
	protectResult = new ProtectResult();

	for (TestCase testCase : testCases) {
	    testCasesMap.put(testCase.getTestCaseCategory(), testCase.getTestCaseVerification());
	}
    }

    @Before
    public void setUp() throws Exception {
	mockedHttpResponse = mock(HttpResponse.class);
	protectResult.setResponse(mockedHttpResponse);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void A2_PRO_BAS_ATK_001() throws Exception {
	Header[] headers = { new BasicHeader("Location", "http://protectnperform.com?sessionId=123456789") };
	when(mockedHttpResponse.getAllHeaders()).thenReturn(headers);

	String testCaseId = "PRO:BAS:ATK:001";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));

    }

    @Test
    public void A2_PRO_BAS_ATK_002() throws Exception {
	Header[] headers = { new BasicHeader("Location", "http://protectnperform.com?sessionId=123456789") };
	when(mockedHttpResponse.getAllHeaders()).thenReturn(headers);

	String testCaseId = "PRO:BAS:ATK:002";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));

    }

    @Test
    public void A2_PRO_BAS_ATK_003() throws Exception {
	File file = new File("test/protect/a2/autocomplete.html");
	when(mockedHttpResponse.getEntity()).thenReturn(new StringEntity(FileUtils.readFileToString(file)));

	String testCaseId = "PRO:BAS:ATK:003";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

    @Test
    public void A2_PRO_BAS_ATK_004() throws Exception {
	protectResult.setResultString(SSlLib.Version.TLS1.getSslVersion());
	
	String testCaseId = "PRO:BAS:ATK:004";
	boolean analyzeResult = vulnAnalyzer.analyze(protectResult, testCasesMap.get(testCaseId));
	assertThat(testCaseId, analyzeResult, is(equalTo(true)));
    }

}
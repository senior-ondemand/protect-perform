package com.pnp.unit.module.parser.perform;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.html.ExtResource;
import com.pnp.module.parser.PerformPreParser;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class ExtResourceManagerTest {

    private PerformPreParser performPreParser;
    private final String targetURL = "http://www.naver.com";

    @Before
    public void setUp() throws Exception {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetURL);
	performPreParser.preParseResponse();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void makeExtJSLink() throws Exception {
	PerformPreParsedResponse parsedResponse = (PerformPreParsedResponse) performPreParser.getParsedResponse();

	for (ExtResource extResource : parsedResponse.getExtJsResourceList()) {
	    assertThat("외부 JS 파일 URL", extResource.getUrl().isEmpty(), is(equalTo(false)));
	    assertThat("외부 JS 파일 내용", extResource.getResponseBody().isEmpty(), is(equalTo(false)));
	}
    }

    @Test
    public void makeExtCSSLink() throws Exception {
	PerformPreParsedResponse parsedResponse = (PerformPreParsedResponse) performPreParser.getParsedResponse();

	for (ExtResource extResource : parsedResponse.getExtCssResourceList()) {
	    assertThat("외부 CSS 파일 URL", extResource.getUrl().isEmpty(), is(equalTo(false)));
	    assertThat("외부 CSS 파일 내용", extResource.getResponseBody().isEmpty(), is(equalTo(false)));
	}
    }

}

package com.pnp.unit.module.parser;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.data.html.ExtResource;
import com.pnp.module.parser.PerformPreParser;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class PerformPreParserTest {

    private PerformPreParser preParser;
    private final String targetURL = "http://www.gmarket.co.kr/";

    @Before
    public void setUp() throws Exception {
	preParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	preParser.setTargetURL(targetURL);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setTargetURL() throws Exception {
	preParser.setTargetURL(targetURL);
    }

    @Test
    public void parseByLine() throws Exception {
	// preParser.preParseResponse();
    }

    @Test
    public void parseByJsoup() throws Exception {

    }

}
package com.pnp.unit.module;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.data.User;
import com.pnp.module.UrlLib;
import com.pnp.module.Validator;
import com.pnp.unit.DummyUser;

public class ValidatorTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void allUserInputWithNormalEmail() throws Exception {
	User user = DummyUser.getInstance();
	assertThat("정상 email", Validator.isValidUserEmail(user), is(equalTo(true)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithAbnormalEmail() throws Exception {
	User user = DummyUser.getInstance();
	user.setEmail("abnormalEmail");
	assertThat("비정상 email", Validator.isValidUserEmail(user), is(equalTo(false)));
    }

    public void allUserInputNormalName() throws Exception {
	User user = DummyUser.getInstance();
	assertThat("정상 이름", Validator.isValidUserName(user), is(equalTo(true)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithEmptyName() throws Exception {
	User user = DummyUser.getInstance();
	user.setName(StringUtils.EMPTY);
	assertThat("빈 이름", Validator.isValidUserName(user), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithTooShortName() throws Exception {
	User user = DummyUser.getInstance();
	user.setName("abc");
	assertThat("3글자 이름", Validator.isValidUserName(user), is(equalTo(false)));
    }

    public void allUserInputWithNormalPassword() throws Exception {
	User user = DummyUser.getInstance();
	assertThat("정상 비밀번호", Validator.isValidUserPassword(user), is(equalTo(true)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithEmptyPassword() throws Exception {
	User user = DummyUser.getInstance();
	user.setPassword(StringUtils.EMPTY);
	user.setRePassword(StringUtils.EMPTY);
	assertThat("빈 비밀번호", Validator.isValidUserPassword(user), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithTooShortPassword() throws Exception {
	User user = DummyUser.getInstance();
	user.setPassword("short");
	user.setRePassword("short");
	assertThat("5자 비밀번호", Validator.isValidUserPassword(user), is(equalTo(false)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void allUserInputWithNotEqualName() throws Exception {
	User user = DummyUser.getInstance();
	user.setRePassword("newPassword");
	assertThat("불일치 비밀번호", Validator.isValidUserPassword(user), is(equalTo(false)));
    }

    @Test
    public void allUserInputWithNormalUrl() throws Exception {
	User user = DummyUser.getInstance();
	assertThat("정상 Url", UrlLib.isValidUrl(user.getUrl()), is(equalTo(true)));
    }

    @Test
    public void allUserInputWithAbnormalUrl() throws Exception {
	User user = DummyUser.getInstance();
	user.setUrl("ftp://pnp.com");
	assertThat("비정상 Url", UrlLib.isValidUrl(user.getUrl()), is(equalTo(false)));
    }

}

package com.pnp.unit.module.perform.yslow.y08;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.AvoidCSSExpressions;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class AvoidCSSExpressionsWithGmarketTest {

    private static Perform perform;
    private static final String targetUrl = "http://www.gmarket.co.kr";
    private static PerformPreParser performPreParser;
    private static AvoidCSSExpressions y08;

    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("cssExpFromExtCssCnt", 0);
	countList.put("cssExpFromBodyCnt", 0);
	countList.put("grade", 100);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y08;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	y08 = (AvoidCSSExpressions) perform;
    }

    @Test
    public void countFromExtCss() throws Exception {
	assertThat("외부 Css Expression", y08.getCssExpFromExtCssCnt() , is(equalTo(countList.get("cssExpFromExtCssCnt"))));
    }

    @Test
    public void countFromBody() throws Exception {
	assertThat("내부 Css Expression", y08.getCssExpFromBodyCnt(), is(equalTo(countList.get("cssExpFromBodyCnt"))));
    }

    @Test
    public void grade() throws Exception {
	assertThat("총점", y08.getGrade(), is(equalTo(countList.get("grade"))));
    }
}
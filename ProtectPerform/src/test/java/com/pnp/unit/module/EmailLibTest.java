package com.pnp.unit.module;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.EmailLib;

public class EmailLibTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void sendEmailByGmail() {
	String to = "legshort@gmail.com";
	String text = "Dear Mail Crawler," + "\n\n No spam to my email, please!";

	assertThat("이메일 전송", EmailLib.sendEmailByGmail(to, text), is(equalTo(true)));
    }
}

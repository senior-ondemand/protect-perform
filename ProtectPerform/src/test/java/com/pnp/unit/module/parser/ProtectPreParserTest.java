package com.pnp.unit.module.parser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.module.parser.PreParser;
import com.pnp.module.parser.ProtectPreParser;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class ProtectPreParserTest {

    private InspectionType inspectionType;

    @Before
    public void setUp() throws Exception {
	inspectionType = InspectionType.PROTECT;
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
	PreParser protectPreParser = PreParserFactory.create(inspectionType);

	protectPreParser.setTargetURL("http://naver.com");
	protectPreParser.preParseResponse();
    }

}

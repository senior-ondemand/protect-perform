
package com.pnp.unit.module.perform.yslow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.perform.yslow.MakeFaviconIcoSmallAndCacheable;
import com.pnp.service.InspectionType;
import com.pnp.service.PreParserFactory;

public class MakeFaviconIcoSmallAndCacheableTest {

    private static Perform perform;
    private static final String targetUrl = "http://127.0.0.1/Y23";
    private static PerformPreParser performPreParser;
    private static MakeFaviconIcoSmallAndCacheable Y23;
    
    private static final HashMap<String, Integer> countList;

    static {
	countList = new HashMap<>();
	countList.put("faviconSize", 30894);
	countList.put("isFaviconCacheable", 1);
	countList.put("grade", 22);
    }

    @BeforeClass
    public static void oneTimeSetUp() {
	runPerform();
    }

    private static void runPerform() {
	runPerform(targetUrl);
    }

    private static void runPerform(String targetUrl) {
	performPreParser = (PerformPreParser) PreParserFactory.create(InspectionType.PERFORM);
	performPreParser.setTargetURL(targetUrl);
	performPreParser.preParseResponse();

	PerformRuleType performType = PerformRuleType.Y23;
	perform = PerformFactory.create(performType);
	perform.initialize(performType, targetUrl, performPreParser.getParsedResponse());
	perform.exe();

	Y23 = (MakeFaviconIcoSmallAndCacheable) perform;
    }
    
    @Test
    public void grade() throws Exception {
	assertThat("총점", Y23.getGrade(), is(equalTo(countList.get("grade"))));
    }
    
    @Test
    public void checkFaviconSize() throws Exception {
	assertThat("Favicon size", Y23.getFaviconSize(), is(equalTo(countList.get("faviconSize"))));
    }
    
    @Test
    public void checkIsFaviconCacheable() throws Exception {
	assertThat("favicon.ico가 캐시상태", Y23.getIsFaviconCacheable(), is(equalTo(countList.get("isFaviconCacheable"))));
    }
    
    public void summary() throws Exception {
	System.out.println(perform.getResult());
	System.out.println(perform.getSolution());
	System.out.println(perform.getGrade());
    }
}

package com.pnp.unit.module.parser.perform;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.html.IntResource;
import com.pnp.module.parser.perform.IntResourceManager;

@RunWith(Parameterized.class)
public class IntResourceManagerTest {

    private static final String TEST_HTML_PATHA = "test/perform/ResourceManager";
    private static final HashMap<String, Integer> intJsResourceCntList;
    private static final HashMap<String, Integer> intCssResourceCntList;
    private final String fileName;
    private final String html;

    private IntResourceManager intResourceManager;

    static {
	intJsResourceCntList = new HashMap<>();
	intJsResourceCntList.put("gmarket.html", 44);

	intCssResourceCntList = new HashMap<>();
	intCssResourceCntList.put("gmarket.html", 2);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters() throws IOException {
	Collection<Object[]> parameters = new ArrayList<Object[]>();
	File[] files = new File(TEST_HTML_PATHA).listFiles();

	for (File file : files) {
	    parameters.add(new Object[] { file.getName(), FileUtils.readFileToString(file) });
	}

	return parameters;
    }

    public IntResourceManagerTest(String fileName, String html) {
	this.fileName = fileName;
	this.html = html;
    }

    private PerformPreParsedResponse mockedPreParsedResponse() {
	PerformPreParsedResponse mockedPreParsedResponse = mock(PerformPreParsedResponse.class);
	when(mockedPreParsedResponse.getDocument()).thenReturn(Jsoup.parse(html));

	return mockedPreParsedResponse;
    }

    @Before
    public void setUp() throws Exception {
	intResourceManager = new IntResourceManager(mockedPreParsedResponse());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void makeIntJsList() {
	ArrayList<IntResource> intJsResourceList = intResourceManager.makeIntJsList();

	assertThat("inLine Js 파일 갯수", intJsResourceList.size(), is(equalTo(intJsResourceCntList.get(fileName))));
    }

    @Test
    public void makeIntCssList() {
	ArrayList<IntResource> intCssResourceList = intResourceManager.makeIntCssList();

	assertThat("inLine Js 파일 갯수", intCssResourceList.size(), is(equalTo(intCssResourceCntList.get(fileName))));
    }

}
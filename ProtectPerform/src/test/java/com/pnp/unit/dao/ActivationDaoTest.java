package com.pnp.unit.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.ActivationDao;
import com.pnp.data.Activation;
import com.pnp.unit.DummyActivation;

public class ActivationDaoTest {

    private ActivationDao activationDao;

    @Before
    public void setUp() throws Exception {
	activationDao = new ActivationDao();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void existsActivationLinkWithNonExist() {
	Activation activation = new Activation();
	activation.setActivationLink("testActivationLink");

	assertThat("비존재하는 Activation 링크", activationDao.existsActivationLink(activation), is(equalTo(false)));
    }

    @Test
    public void existsActivationLinkWithExist() {
	Activation activation = DummyActivation.getInstance();

	activationDao.insertActivation(activation);
	assertThat("존재하는 Activation 링크", activationDao.existsActivationLink(activation), is(equalTo(true)));

	activationDao.delete(activation);
    }

    @Test
    public void insertActivation() throws Exception {
	Activation activation = DummyActivation.getInstance();

	activationDao.insertActivation(activation);
	assertThat("Action 삽입", activationDao.existsActivationLink(activation), is(equalTo(true)));

	activationDao.delete(activation);
    }

    @Test
    public void getActivationByLinkWithNonExist() throws Exception {
	assertThat("비존재 Action", activationDao.getActivationByLink(DummyActivation.getInstance()), is(nullValue()));
    }

    @Test
    public void getActivationByLinkWithExist() throws Exception {
	Activation activation = DummyActivation.getInstance();
	activationDao.insertActivation(activation);
	Activation activationFromRedis = activationDao.getActivationByLink(activation);

	assertThat("존재하는 Action - getActivationLink()", activationFromRedis.getActivationLink(), is(equalTo(DummyActivation.TEST_ACTIVATION_LINK)));
	assertThat("존재하는 Action - getUserEmail()", activationFromRedis.getUserEmail(), is(equalTo(DummyActivation.TEST_USER_EMAIL)));

	activationDao.delete(activation);
    }
}

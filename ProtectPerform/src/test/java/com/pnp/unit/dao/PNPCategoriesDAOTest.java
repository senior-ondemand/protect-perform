package com.pnp.unit.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Map;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.PNPCategoriesDAO;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;

public class PNPCategoriesDAOTest {

    private PNPCategoriesDAO pnpCategoriesDAO;
    private static Logger logger = LoggerFactory.getLogger(PNPCategoriesDAO.class);

    @Before
    public void setUp() throws Exception {
	pnpCategoriesDAO = new PNPCategoriesDAO();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getWithUserSetting() {

	Map<String, Rule> protectRules = pnpCategoriesDAO.getProtectCategories();
	Map<String, Rule> performRules = pnpCategoriesDAO.getPerformCategories();

	for (String key : protectRules.keySet()) {
	    Rule rule = protectRules.get(key);
	    ArrayList<TestCase> testCaseList = rule.getTestCases();
	    logger.info("ruleName : " + rule.getTitle());

	    for (TestCase testCase : testCaseList) {
		logger.info("testCaseName : " + testCase.getTestCaseName());
	    }
	}

	for (String key : performRules.keySet()) {
	    Rule rule = performRules.get(key);
	    logger.info("ruleName : " + rule.getTitle());
	    logger.info("ruleType : " + rule.getType().toString());
	}
//	assertThat("Protect Rule 개수", protectRulesMap.size(), is(greaterThan(0)));
    }

}
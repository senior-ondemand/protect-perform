package com.pnp.unit.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.UserDAO;
import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.data.User.Interval;
import com.pnp.module.DateLib;
import com.pnp.unit.DummyUser;

public class UserDAOTest {

    private UserDAO userDao;

    @Before
    public void setUp() throws Exception {
	userDao = new UserDAO();
	userDao.truncate();
    }

    @After
    public void tearDown() throws Exception {
    }

    private User signUpWithDummyUser() {
	User user = DummyUser.getInstanceWithHashPassword();
	userDao.signUp(user);

	return user;
    }

    private User signUpAndActivateDummyUser() {
	User user = DummyUser.getInstanceWithHashPassword();
	user.setActivity(Activity.Activated.toString());
	user.setActivationDateTime(DateLib.currentDateString());
	userDao.signUp(user);

	return user;
    }

    private User signUpAndActivateAndDailyScheduleWithDummyUser() {
	User user = DummyUser.getInstance();
	user.setActivationDateTime(DateLib.currentDateString());
	user.setActivity(Activity.Activated.toString());
	user.setScheduleInterval(Interval.Daily.toString());
	userDao.signUp(user);

	return user;
    }

    @Test
    public void signUp() throws Exception {
	User user = DummyUser.getInstanceWithHashPassword();
	assertThat("회원 가입", userDao.signUp(user), is(equalTo(true)));

	User signInUser = userDao.signIn(user);
	assertThat("가입후 getEmail()", signInUser.getEmail(), is(equalTo(DummyUser.TEST_EMAIL)));
	assertThat("가입후 getName()", signInUser.getName(), is(equalTo(DummyUser.TEST_NAME)));
	assertThat("가입후 getUrl()", signInUser.getUrl(), is(equalTo(DummyUser.TEST_URL)));
	assertThat("가입후 getActivation()", signInUser.getActivity(), is(equalTo(DummyUser.TEST_ACTIVITY)));
	assertThat("가입후 getActivationLink()", signInUser.getActivationLink(), is(equalTo(DummyUser.TEST_ACTIVATION_LINK)));
	assertThat("가입후 getSignUpDateTime()", signInUser.getSignUpDateTime(), is(equalTo(DummyUser.TEST_SIGNUP_DATETIME)));
	assertThat("가입후 getActivationDateTime()", signInUser.getActivationDateTime(), is(equalTo(DummyUser.TEST_ACTIVATION_DATETIME)));
    }

    @Test
    public void signInWithExistUser() throws Exception {
	User user = signUpWithDummyUser();

	User signInUser = userDao.signIn(user);
	assertThat("정상 로그인 - getEmail()", signInUser.getEmail(), is(equalTo(DummyUser.TEST_EMAIL)));
	assertThat("정상 로그인 - getName()", signInUser.getName(), is(equalTo(DummyUser.TEST_NAME)));
	assertThat("정상 로그인 - getUrl()", signInUser.getUrl(), is(equalTo(DummyUser.TEST_URL)));
	assertThat("정상 로그인 - getActivation()", signInUser.getActivity(), is(equalTo(DummyUser.TEST_ACTIVITY)));
	assertThat("정상 로그인 - getActivationLink()", signInUser.getActivationLink(), is(equalTo(DummyUser.TEST_ACTIVATION_LINK)));
	assertThat("정상 로그인 - getSignUpDateTime()", signInUser.getSignUpDateTime(), is(equalTo(DummyUser.TEST_SIGNUP_DATETIME)));
	assertThat("정상 로그인 - getActivationDateTime()", signInUser.getActivationDateTime(), is(equalTo(DummyUser.TEST_ACTIVATION_DATETIME)));
    }

    @Test
    public void signInWithNonExistUser() throws Exception {
	User user = new User();
	user.setPassword(DummyUser.TEST_PASSWORD);
	User signInUser = userDao.signIn(user);

	assertThat("없는 계정 로그인", signInUser, is(nullValue()));
    }

    @Test
    public void setAccount() throws Exception {
	User user = signUpWithDummyUser();

	String newPassword = "newPassword";
	String newName = "newName";
	String newUrl = "http://new.pnp.com";

	user.setPassword(newPassword);
	user.setName(newName);
	user.setUrl(newUrl);
	userDao.setAccount(user);

	User signInUser = userDao.signIn(user);
	assertThat("수정후 - getName()", signInUser.getName(), is(equalTo(newName)));
	assertThat("수정후 - getUrl()", signInUser.getUrl(), is(equalTo(newUrl)));
    }

    @Test
    public void deactivate() throws Exception {
	User user = signUpWithDummyUser();

	assertThat("회원 삭제", userDao.deactivate(user), is(equalTo(true)));
    }

    @Test
    public void getScheduleJobByIntervalWithDaily() throws Exception {
	User user = signUpAndActivateAndDailyScheduleWithDummyUser();

	assertThat("ScheduleJob User 목록", userDao.getScheduleJobByInterval(Interval.Daily).size(), is(equalTo(1)));
    }

    @Test
    public void truncate() throws Exception {
	signUpWithDummyUser();

	assertThat("Truncate User", userDao.truncate(), is(equalTo(1)));
	assertThat("Truncate User", userDao.count(), is(equalTo(0)));
    }

    @Test
    public void countWhenEmtpy() throws Exception {
	assertThat("비어있을때 유저수", userDao.count(), is(equalTo(0)));
    }

    @Test
    public void countAfterSignUp() throws Exception {
	signUpWithDummyUser();

	assertThat("가입후 유저수", userDao.count(), is(equalTo(1)));
    }

}

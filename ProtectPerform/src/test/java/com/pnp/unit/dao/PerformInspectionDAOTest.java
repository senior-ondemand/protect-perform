package com.pnp.unit.dao;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.data.Rule;
import com.pnp.data.UserSetting;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;
import com.pnp.service.InspectionType;

public class PerformInspectionDAOTest {

    Map<String, Rule> rules;

    @Before
    public void setUp() throws Exception {
	// Set userSetting
	JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createWithAllTestCaseInProtectAndPerform());

	// Set DAO
	InspectionDAO dao = InspectionDAOFactory.create(InspectionType.PERFORM);

	// Set Rules
	rules = dao.get(userSetting);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
	for (String key : rules.keySet()) {
	    Rule rule = rules.get(key);
//	    System.out.println(rule.getTitle());
//	    System.out.println(rule.getType());
	}
    }
}

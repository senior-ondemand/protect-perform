package com.pnp.unit.dao;

import java.util.ArrayList;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.ProtectDetailInfoDAO;
import com.pnp.data.DetailInfo;
import com.pnp.data.Rule;

public class ProtectDetailInfoDAOTest {

    Map<String, Rule> rules;

    @Before
    public void setUp() throws Exception {
	
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getWithUserSetting() {
	ProtectDetailInfoDAO protectDetailInfoDAO = new ProtectDetailInfoDAO();
	DetailInfo protectDetailInfo = protectDetailInfoDAO.get("PRO:INJ:ATK:001");
	
	ArrayList<String> classficationNameList = protectDetailInfo.getClassificationNameList();
	ArrayList<String> classficationValueList = protectDetailInfo.getClassificationValueList();
	ArrayList<String> referenceNameList = protectDetailInfo.getReferenceNameList();
	ArrayList<String> referenceValueList = protectDetailInfo.getReferenceValueList();
	
//	System.out.println(protectDetailInfo.getTitle());
//	System.out.println(protectDetailInfo.getTestCaseTitle());
//	System.out.println(protectDetailInfo.getCategory());
//	System.out.println(protectDetailInfo.getAttackPattern());
//	System.out.println(protectDetailInfo.getVerificationPattern());
//	System.out.println(protectDetailInfo.getDescription());
//	System.out.println(protectDetailInfo.getRemedy());
//	
//	for (String value : classficationNameList) {
//	    System.out.println(value);
//	}
//	for (String value : classficationValueList) {
//	    System.out.println(value);
//	}
//	for (String value : referenceNameList) {
//	    System.out.println(value);
//	}
//	for (String value : referenceValueList) {
//	    System.out.println(value);
//	}
	
//	assertThat("Protect Rule 개수", protectRulesMap.size(), is(greaterThan(0)));
    }

}
package com.pnp.unit.dao;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.pnp.dao.ProtectScanResultDAO;
import com.pnp.dao.ScanResultDAOFactory;
import com.pnp.data.ProtectScanResult;
import com.pnp.data.ScanResult;
import com.pnp.service.InspectionType;

public class ProtectScanResultDAOTest {

	private ProtectScanResultDAO protectScanResultDAO;
	private ScanResult scanResult;

	@Before
	public void setUp() throws Exception {
		
		protectScanResultDAO = (ProtectScanResultDAO) ScanResultDAOFactory.create(InspectionType.PROTECT);
		scanResult = new ProtectScanResult();
		
		scanResult.setScore(10);
		scanResult.setTargetUrl("www.naver.com");
		scanResult.setTestCase("PRO:BAS:ATK:001");
		
	}

	@Test
	public void test() {
	}

}

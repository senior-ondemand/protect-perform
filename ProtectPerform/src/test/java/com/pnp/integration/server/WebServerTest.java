package com.pnp.integration.server;

import static org.hamcrest.Matchers.*;
import static org.vertx.testtools.VertxAssert.*;

import org.hamcrest.core.*;
import org.junit.Ignore;
import org.junit.Test;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.testtools.TestVerticle;
import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Handler;

public class WebServerTest extends TestVerticle {

    @Test
    public void testServer() {

	testComplete();
    }

    @Override
    public void start() {
	initialize();
	container.deployModule(System.getProperty("vertx.modulename"), new AsyncResultHandler<String>() {
	    @Override
	    public void handle(AsyncResult<String> asyncResult) {
		assertThat("비동기식 호출", asyncResult.succeeded(), is(equalTo(true)));
		assertThat("deploymentID should not be null", asyncResult.result(), is(notNullValue()));
		startTests();
	    }
	});
    }
}
package com.pnp.controller;

public class IllegalUserDataException extends Exception {

    private static final String FALSE = "f:";

    public enum UserDataField {
	UserEmail, UserPassword, UserRePassword, UserName, UserUrl
    }

    public IllegalUserDataException(UserDataField userDataField, String message) {
	super(FALSE + userDataField.toString() + ":" + message);
    }

}

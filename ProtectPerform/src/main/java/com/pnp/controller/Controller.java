package com.pnp.controller;

import java.util.HashMap;

import org.vertx.java.core.http.HttpServerRequest;

import com.pnp.server.SessionHelper;
import com.pnp.socketIO.SocketIOCallback;

public abstract class Controller {

    protected SocketIOCallback socketIOCallback;
    protected HttpServerRequest request;
    protected SessionHelper sessionHelper;
    protected HashMap<String, String> postMap;

    public Controller() {

    }

    public Controller(SocketIOCallback socketIOCallback, SessionHelper sessionHelper) {
	this.socketIOCallback = socketIOCallback;
	this.sessionHelper = sessionHelper;
    }

    public void initialize(HttpServerRequest request, SessionHelper sessionHelper) {
	this.request = request;
	this.sessionHelper = sessionHelper;
    }

}
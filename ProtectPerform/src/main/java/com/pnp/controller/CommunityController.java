package com.pnp.controller;

import java.util.ArrayList;
import java.util.Map;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;
import org.vertx.java.core.json.JsonObject;

import com.pnp.data.Rule;
import com.pnp.data.SearchResultData;
import com.pnp.module.PNPConverter;
import com.pnp.service.CommunityService;
import com.pnp.service.RankService;
import com.pnp.socketIO.SocketIOCallback;

public class CommunityController extends Controller {

    private static Logger logger = LoggerFactory.getLogger(CommunityController.class);

    private CommunityService communityService;

    public CommunityController() {
    }

    public CommunityController(SocketIOCallback socketIOCallback, JsonObject jsonObject) {
	this.socketIOCallback = socketIOCallback;
	this.communityService = new CommunityService(jsonObject);
    }

    public CommunityController(SocketIOCallback socketIOCallback) {
	this.socketIOCallback = socketIOCallback;
	this.communityService = new CommunityService();
    }

    public void search() {
	ArrayList<SearchResultData> searchResultDataList = communityService.getSearchData();
	socketIOCallback.emit("searchResultDataList", PNPConverter.searchResultDataListToJson(searchResultDataList));
    }

    public void getCategories() {
	logger.info("CommunityController-search service Start");

	Map<String, Rule> protectCategories = communityService.getProtectCategories();
	Map<String, Rule> performCategories = communityService.getPerformCategories();

	socketIOCallback.emit("protectCategories", PNPConverter.protectCategoriesToJson(protectCategories));
	socketIOCallback.emit("performCategories", PNPConverter.performCategoriesToJson(performCategories));
    }

    public void getPerformRank() {
	RankService rankService = new RankService();
	Map<String, Integer> performUserRank = rankService.getPerformRank("USR");
	Map<String, Integer> performSpecialRank = rankService.getPerformRank("SPC");

	socketIOCallback.emit("performUserRank", PNPConverter.performRankToJson(performUserRank));
	socketIOCallback.emit("performSpecialRank", PNPConverter.performRankToJson(performSpecialRank));
    }

    public void setPerformUserRank(JsonObject jsonObject) {
	RankService rankService = new RankService(jsonObject);
	rankService.setPerformUserRank();
    }
}
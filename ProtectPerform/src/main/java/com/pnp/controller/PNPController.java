package com.pnp.controller;

import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.dao.IndexDAO;
import com.pnp.data.BaseStatus;
import com.pnp.data.DetailInfo;
import com.pnp.data.UserSetting;
import com.pnp.module.PNPConverter;
import com.pnp.server.SessionHelper;
import com.pnp.service.BaseStatusService;
import com.pnp.service.DetailInfoService;
import com.pnp.service.InspectionService;
import com.pnp.service.PerformInspectionService;
import com.pnp.service.ProtectInspectionService;
import com.pnp.socketIO.SocketIOCallback;

public class PNPController extends Controller {

    private static Logger logger = LoggerFactory.getLogger(PNPController.class);

    private UserSetting userSetting;
    public IndexDAO indexDAO;

    public PNPController(SocketIOCallback socketIOCallback, SessionHelper sessionHelper) {
	super(socketIOCallback, sessionHelper);
	indexDAO = new IndexDAO();
    }

    public void go() {
	logger.info("PNPController Start");

	Integer index = 1;
	index = indexDAO.getIndex("LOG:" + userSetting.getTargetURL() + ":*");
	indexDAO.setBaseInfo(index, userSetting);

	InspectionService protectInspectionService = new ProtectInspectionService(userSetting, socketIOCallback, index);
	InspectionService performInsepctionService = new PerformInspectionService(userSetting, socketIOCallback, index);

	new Thread((Runnable) protectInspectionService).start();
	new Thread((Runnable) performInsepctionService).start();
    }

    public void getDetailInfo(JsonObject jsonObject) {
	logger.info("PNPController-detailinfo service Start");

	DetailInfo detailInfo;
	DetailInfoService detailInfoService = new DetailInfoService(jsonObject);

	detailInfo = detailInfoService.getDetailInfo();

	socketIOCallback.emit(detailInfo.getEventName(), PNPConverter.detailInfoToJson(detailInfo));
    }

    public void getBaseStatus(JsonObject jsonObject) {
	logger.info("PNPController-baseStatus service Start");

	userSetting = PNPConverter.JsonToUserSetting(jsonObject);

	BaseStatus baseStatus;
	BaseStatusService baseStatusService = new BaseStatusService(userSetting);

	baseStatus = baseStatusService.getBaseStatus();

	socketIOCallback.emit("baseInfo", PNPConverter.baseInfoToJson(baseStatus));
    }
}
package com.pnp.controller;

import org.apache.commons.lang3.StringUtils;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.Activation;
import com.pnp.data.User;
import com.pnp.service.UserService;

public class UserController extends Controller {
    private static final String TRUE = "t:";
    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    public UserController() {
	userService = new UserService();
    }

    public String signUp(User user) {
	logger.info("SignUp");
	String resultString = StringUtils.EMPTY;

	try {
	    userService.signUp(user);
	    resultString = TRUE;
	} catch (IllegalUserDataException e) {
	    logger.info("User Exception: " + e.getMessage());
	    resultString = e.getMessage();
	}

	return resultString;
    }

    public User signIn(User user) throws IllegalUserDataException {
	logger.info("SignIn");
	return userService.signIn(user);
    }

    public boolean activateAccount(Activation activation) {
	logger.info("ActivationAccount");
	return userService.activateAccount(activation);
    }

    public boolean deactivate(User user) {
	logger.info("Decativate");
	return userService.deactivate(user);
    }

    public String setAccount(User user) {
	logger.info("SetAccount");
	String resultString = StringUtils.EMPTY;

	try {
	    userService.setAccount(user);
	    resultString = TRUE;
	} catch (IllegalUserDataException e) {
	    logger.info("User Exception: " + e.getMessage());
	    resultString = e.getMessage();
	}

	return resultString;
    }

}
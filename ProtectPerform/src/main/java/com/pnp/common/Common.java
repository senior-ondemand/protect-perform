package com.pnp.common;

import io.netty.handler.codec.http.Cookie;
import io.netty.handler.codec.http.CookieDecoder;

import java.io.IOException;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.nhncorp.mods.socket.io.SocketIOSocket;

public class Common {

    public static HttpResponse exePNPRequest(String url) {
	// HttpClient httpclient = new DefaultHttpClient();
	HttpClient httpclient = HttpClientBuilder.create().build();
	HttpGet httpGet = new HttpGet(url);
	HttpResponse response = null;

	httpGet.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
	// httpGet.setHeader("Accept-Encoding", "gzip,deflate,sdch");
	httpGet.setHeader("User-Agent", "Protect & Perform / SW maestro #4 project");
	httpGet.setHeader("X-Scanner", "Protect & Perform / SW maestro #4 project");
	httpGet.setHeader("Accept-Language", "ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");

	// HttpHost proxy = new HttpHost("localhost", 8888);
	// httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY,
	// proxy);

	try {
	    response = httpclient.execute(httpGet);
	} catch (ClientProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return response;
    }

    public static HttpResponse exePNPRequest(String url, Header[] headers) {
	HttpClient httpclient = HttpClientBuilder.create().build();
	HttpGet httpGet = new HttpGet(url);
	HttpResponse response = null;

	httpGet.setHeader("X-Scanner", "Protect & Perform / SW maestro #4 project");
	httpGet.setHeaders(headers);

	try {
	    response = httpclient.execute(httpGet);
	} catch (ClientProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return response;
    }

    public static String getServerInfoFromURL(String targetURL) {
	String serverInfo = "";

	HttpClient httpclient = HttpClientBuilder.create().build();
	HttpGet httpGet = new HttpGet(targetURL);
	HttpResponse response = null;
	Header[] headers = null;

	try {
	    response = httpclient.execute(httpGet);
	} catch (ClientProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	if (response != null)
	    headers = response.getAllHeaders();

	if (headers != null)
	    for (Header header : headers) {
		if (header.getName().equalsIgnoreCase("server")) {
		    serverInfo = header.getValue();
		} else if (header.getName().equalsIgnoreCase("x-powered-by")) {
		    serverInfo.concat(" " + header.getValue());
		}
	    }

	return serverInfo;
    }

    public static String httpResponseToResponseBody(HttpResponse response) {
	try {
	    return EntityUtils.toString(response.getEntity());
	} catch (ParseException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public static String sessionIdFromCookie(SocketIOSocket socket) {
	String sessionId = "";

	String value = socket.handshakeData().getHeaders().get("Cookie");
	if (value != null) {
	    Set<Cookie> cookies = CookieDecoder.decode(value);
	    for (final Cookie cookie : cookies) {
		if (cookie.getName().equals("sessionId")) {
		    sessionId = (cookie.getValue().isEmpty()) ? "" : cookie.getValue();
		    break;
		}
	    }
	}

	return sessionId;
    }
}

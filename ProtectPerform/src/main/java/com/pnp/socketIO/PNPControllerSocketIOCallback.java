package com.pnp.socketIO;

import org.vertx.java.core.json.JsonObject;

import com.nhncorp.mods.socket.io.SocketIOServer;

public class PNPControllerSocketIOCallback implements SocketIOCallback {

	private final SocketIOServer io;

	public PNPControllerSocketIOCallback(SocketIOServer io) {
		this.io = io;
	}

	@Override
	public synchronized void emit(String eventName) {
		io.sockets().emit(eventName);
	}

	@Override
	public synchronized void emit(String eventName, JsonObject obj) {
		io.sockets().emit(eventName, obj);
	}

}

package com.pnp.socketIO;

import java.util.Date;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;
import org.vertx.java.platform.Verticle;

import com.nhncorp.mods.socket.io.SocketIOServer;
import com.nhncorp.mods.socket.io.SocketIOSocket;
import com.nhncorp.mods.socket.io.impl.Configurer;
import com.nhncorp.mods.socket.io.impl.DefaultSocketIOServer;
import com.pnp.common.Common;
import com.pnp.controller.CommunityController;
import com.pnp.controller.PNPController;
import com.pnp.dao.HistoryDAO;
import com.pnp.data.UserSetting;
import com.pnp.module.PNPConverter;
import com.pnp.server.SessionHelper;

public class SocketIO extends Verticle {

    private static Logger logger = LoggerFactory.getLogger(SocketIO.class);

    private final int port;
    private final SessionHelper sessionHelper;

    public SocketIO(Vertx vertx, int port, SessionHelper sessionHelper) {
	this.vertx = vertx;
	this.port = port;
	this.sessionHelper = sessionHelper;
    }

    @Override
    public void start() {
	HttpServer server = vertx.createHttpServer();
	final SocketIOServer io = new DefaultSocketIOServer(vertx, server);

	io.configure(new Configurer() {
	    @Override
	    public void configure(JsonObject config) {
		config.putString("transports", "websocket,flashsocket,xhr-polling,jsonp-polling,htmlfile");
	    }
	});

	io.sockets().onConnection(new Handler<SocketIOSocket>() {
	    @Override
	    public void handle(final SocketIOSocket socket) {

		// on url submit
		socket.on("search", new Handler<JsonObject>() {
		    @Override
		    public void handle(final JsonObject searchData) {
			logger.info("I received " + searchData);

			sessionHelper.withSessionData(Common.sessionIdFromCookie(socket), "data", new Handler<JsonObject>() {

			    @Override
			    public void handle(JsonObject sessionData) {
				UserSetting userSetting = PNPConverter.JsonToUserSetting(searchData);
				logger.info("sessionData " + sessionData.toString());
				logger.info("userSetting " + userSetting.toString());

				// 릴리즈 전에 주석 제거
				// if (Validator.isValidScan(sessionData,
				// userSetting)) {
				logger.info("Valid Scan Request");
				SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
				PNPController pnpController = new PNPController(socketIOCallback, sessionHelper);
				pnpController.getBaseStatus(searchData);
				pnpController.go();
				// } else {
				// logger.info("Invalid Scan Request");
				// io.sockets().emit("invalidScanRequest",
				// "Invalid Scan Request");
				// }
			    }
			});

		    } // callback
		});

		// on click subtree
		socket.on("getDetailInfo", new Handler<JsonObject>() {

		    @Override
		    public void handle(JsonObject detailData) {
			logger.info("i received : " + detailData);

			SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
			PNPController pnpController = new PNPController(socketIOCallback, sessionHelper);
			pnpController.getDetailInfo(detailData);
		    }
		});

		// on search submit in community
		socket.on("community_search", new Handler<JsonObject>() {

		    @Override
		    public void handle(JsonObject searchInputData) {
			logger.info("i received : " + searchInputData);

			SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
			CommunityController communityController = new CommunityController(socketIOCallback, searchInputData);
			communityController.search();
		    }
		});

		// Community basic call function
		socket.on("getCategories", new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject searchData) {
			SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
			CommunityController communityController = new CommunityController(socketIOCallback);
			communityController.getCategories();

		    } // callback
		});

		// Community basic call function
		socket.on("getPerformRank", new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject searchData) {
			SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
			CommunityController communityController = new CommunityController(socketIOCallback);
			communityController.getPerformRank();

		    } // callback
		});

		// Community basic call function
		socket.on("setPerformUserRank", new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject searchData) {
			System.out.println(searchData);
			SocketIOCallback socketIOCallback = new PNPControllerSocketIOCallback(io);
			CommunityController communityController = new CommunityController(socketIOCallback);
			communityController.setPerformUserRank(searchData);

		    } // callback
		});

		// finished
		socket.on("search_finished", new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject searchData) {
			Date date = new Date();
			io.sockets().emit("finished_time", date.toString());
		    } // callback
		});

		// get history data
		socket.on("getHistoryData", new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject data) {
			//
			HistoryDAO historyDAO = new HistoryDAO();

			io.sockets().emit(
				"setHistoryData",
				PNPConverter.historyData(
					historyDAO.getPerformHistoryData(data.getString("user_email"), data.getString("target_url")),
					historyDAO.getProtectHistoryData(data.getString("user_email"), data.getString("target_url")),
					historyDAO.getResourceData(data.getString("user_email"), data.getString("target_url"))));

		    } // callback
		});

		socket.onDisconnect(new Handler<JsonObject>() {
		    @Override
		    public void handle(JsonObject event) {
			logger.info("disconnect");
			io.sockets().emit("user disconnected");
		    }
		});
	    }
	});

	server.listen(port);
    }
}
package com.pnp.socketIO;

import org.vertx.java.core.json.JsonObject;

public interface SocketIOCallback {
    public void emit(String eventName);
    public void emit(String eventName, JsonObject obj);
}

package com.pnp.module;

import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import com.pnp.module.protect.Protect;

public class JsonObjectGenerator {

    private String targetURL = "http://www.naver.com";
    private JsonObject jsonObject;

    private JsonObject setting;
    private JsonObject testRules;
    private JsonArray protect;
    private JsonArray perform;

    public JsonObjectGenerator() {
	initialize();
    }

    public JsonObjectGenerator(String targetULR) {
	this.targetURL = targetULR;

	initialize();
    }

    private void initialize() {
	jsonObject = new JsonObject();
	jsonObject.putString("target_url", targetURL);

	setting = new JsonObject();
	testRules = new JsonObject();
	protect = new JsonArray();
	perform = new JsonArray();
    }

    private void closeJsonObject() {
	testRules.putArray("protect", protect);
	testRules.putArray("perform", perform);
	setting.putObject("testRules", testRules);
	jsonObject.putObject("setting", setting);
    }

    private void fillProtectWithTenRule() {
	for (int i = 0; i < 10; i++) {
	    protect.addString("1");
	}
    }

    private void fillPerformWithTenRule() {
	for (int i = 0; i < 23; i++) {
	    perform.addString("1");
	}
    }

    public JsonObject createWithAllTestCaseInProtectAndPerform() {
	fillProtectWithTenRule();
	fillPerformWithTenRule();
	closeJsonObject();

	return jsonObject;
    }

    public JsonObject createWithAllTestCaseInProtect() {
	closeJsonObject();

	return jsonObject;
    }

    public JsonObject createWithAllTestCaseInPerform() {
	closeJsonObject();

	return jsonObject;
    }

    public JsonObject createWithZeroTestCaseInProtect() {
	closeJsonObject();

	return jsonObject;
    }

    public JsonObject createWithZeroTestCaseInPerform() {
	closeJsonObject();

	return jsonObject;
    }

    public JsonObject createProtectRule(Protect.Rule rule) {
	String value;
	for (int i = 0; i < 10; i++) {
	    value = (i == rule.getRuleIndex()) ? "1" : "0";
	    protect.addString(value);
	}

	closeJsonObject();

	return jsonObject;
    }

}
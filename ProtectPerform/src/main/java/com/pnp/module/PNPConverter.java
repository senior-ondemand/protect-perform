package com.pnp.module;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;

import com.pnp.data.BaseStatus;
import com.pnp.data.DetailInfo;
import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.ProtectDetailInfo;
import com.pnp.data.Rule;
import com.pnp.data.SearchInputData;
import com.pnp.data.SearchResultData;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;
import com.pnp.service.InspectionType;

public class PNPConverter {

	public void protectResultToJson(String analyze) {
	}

	public static UserSetting JsonToUserSetting(JsonObject jsonObject) {
		UserSetting userSetting = new UserSetting();
		userSetting.setTargetURL(jsonObject.getString("target_url"));
		userSetting.setScanType(jsonObject.getString("scan_type"));
		userSetting.setUserEmail(jsonObject.getString("user_id"));
		userSetting.setProtectList(JsonArrayToArrayList(jsonObject.getObject("setting").getObject("testRules").getArray("protect")));
		userSetting.setPerformList(JsonArrayToArrayList(jsonObject.getObject("setting").getObject("testRules").getArray("perform")));

		return userSetting;
	}

	public static ArrayList<Boolean> JsonArrayToArrayList(JsonArray jsonArray) {
		ArrayList<Boolean> arrayList = new ArrayList<Boolean>();
		if (jsonArray != null) {
			int len = jsonArray.size();
			for (int i = 0; i < len; i++) {
				if (jsonArray.get(i).toString().equals("1"))
					arrayList.add(true);
				else
					arrayList.add(false);
			}
		}

		return arrayList;
	}

	public static JsonObject protectResultToJson(boolean isVerification, String ruleTitle, TestCase testCase, URI targetURI) {
		JsonObject jsonObject = new JsonObject();

		jsonObject.putString("title", testCase.getTestCaseName());
		String[] key = testCase.getTestCaseCategory().split(":");
		jsonObject.putString("category", testCase.getTestCaseCategory());
		jsonObject.putString("ruleTitle", ruleTitle);
		jsonObject.putString("ruleName", key[1]);
		jsonObject.putNumber("testCaseNum", Integer.parseInt(key[3]));
		if (targetURI == null) {
			jsonObject.putString("url", "N/A");
		} else {
			jsonObject.putString("url", targetURI.toString());
		}
		jsonObject.putString("level", testCase.getTestCaseAttack().get("level"));
		jsonObject.putBoolean("isVerification", isVerification);

		return jsonObject;
	}

	public static JsonObject performResultToJson(Perform perform) {

		JsonObject jsonObject = new JsonObject();

		jsonObject.putString("title", perform.getTitle());
		jsonObject.putString("category", perform.getCategory());
		jsonObject.putString("result", perform.getResult());
		jsonObject.putString("solution", perform.getSolution());
		jsonObject.putString("grade", Integer.toString(perform.getResultGrade()));

		return jsonObject;
	}

	public static JsonObject detailInfoToJson(DetailInfo detailInfo) {
		JsonObject jsonObject = new JsonObject();

		JsonArray referenceNameJsonArray = new JsonArray(detailInfo.getReferenceNameList().toArray());
		JsonArray referenceValueJsonArray = new JsonArray(detailInfo.getReferenceValueList().toArray());

		jsonObject.putString("title", detailInfo.getTitle());
		jsonObject.putString("category", detailInfo.getCategory());
		jsonObject.putString("description", detailInfo.getDescription());
		jsonObject.putString("remedy", detailInfo.getRemedy());

		jsonObject.putArray("referenceNameArray", referenceNameJsonArray);
		jsonObject.putArray("referenceValueArray", referenceValueJsonArray);

		if (detailInfo.getType().equals("protect")) {
			JsonArray classficationNameJsonArray = new JsonArray(detailInfo.getClassificationNameList().toArray());
			JsonArray classficationValueJsonArray = new JsonArray(detailInfo.getClassificationValueList().toArray());

			jsonObject.putArray("classificationNameArray", classficationNameJsonArray);
			jsonObject.putArray("classificationValueArray", classficationValueJsonArray);

			ProtectDetailInfo protectDetailInfo = (ProtectDetailInfo) detailInfo;
			jsonObject.putString("testCaseTitle", protectDetailInfo.getTestCaseTitle());
			jsonObject.putString("attackPattern", protectDetailInfo.getAttackPattern());
			jsonObject.putString("verificationPattern", protectDetailInfo.getVerificationPattern());
			jsonObject.putString("level", protectDetailInfo.getLevel());
		} else if (detailInfo.getType().equals("perform")) {

		}

		return jsonObject;
	}

	public static String JsonToCategory(JsonObject data) {
		return data.getString("category");
	}

	public static InspectionType JsonToType(JsonObject data) {
		String type = data.getString("type");
		switch (type) {

		case "PROTECT":
			return InspectionType.PROTECT;
		case "PERFORM":
			return InspectionType.PERFORM;

		default:
			throw new IllegalArgumentException("�녿뒗 移댄뀒怨좊━");
		}
	}

	public static SearchInputData JsonToSearchInputData(JsonObject jsonData) {

		SearchInputData searchInputData = new SearchInputData();
		searchInputData.setSearchType(jsonData.getString("searchType"));
		searchInputData.setKeyword(jsonData.getString("keyword"));

		return searchInputData;
	}

	public static JsonObject protectCategoriesToJson(Map<String, Rule> protectCategories) {

		if (protectCategories == null)
			return null;

		JsonObject jsonObject = new JsonObject();
		JsonArray jsonArray = new JsonArray();

		for (String key : protectCategories.keySet()) {
			Rule rule = protectCategories.get(key);
			ArrayList<TestCase> testCaseList = rule.getTestCases();

			JsonObject jsonObjectCategory = new JsonObject();
			JsonArray jsonArrayTestCase = new JsonArray();

			jsonObjectCategory.putString("ruleName", rule.getTitle());

			for (TestCase testCase : testCaseList) {
				jsonArrayTestCase.addString(testCase.getTestCaseName());
			}
			jsonObjectCategory.putArray("testCaseNameArray", jsonArrayTestCase);
			jsonArray.addObject(jsonObjectCategory);
		}

		jsonObject.putArray("protectCategories", jsonArray);

		return jsonObject;
	}

	public static JsonObject performCategoriesToJson(Map<String, Rule> performCategories) {

		if (performCategories == null)
			return null;

		JsonObject jsonObject = new JsonObject();
		JsonArray jsonArray = new JsonArray();

		for (String key : performCategories.keySet()) {
			Rule rule = performCategories.get(key);

			JsonObject jsonObjectCategory = new JsonObject();

			jsonObjectCategory.putString("ruleName", rule.getTitle());
			jsonObjectCategory.putString("category", rule.getCategory());
			jsonArray.addObject(jsonObjectCategory);
		}

		jsonObject.putArray("performCategories", jsonArray);

		return jsonObject;
	}

	public static JsonObject searchResultDataListToJson(ArrayList<SearchResultData> searchResultDataList) {

		JsonObject jsonObject = new JsonObject();

		JsonArray jsonArrayProtect = new JsonArray();
		JsonArray jsonArrayPerform = new JsonArray();

		if (searchResultDataList != null) {
			for (SearchResultData searchResultData : searchResultDataList) {
				JsonObject jsonObjectData = new JsonObject();

				jsonObjectData.putString("title", searchResultData.getTitle());
				jsonObjectData.putString("category", searchResultData.getCategory());
				jsonObjectData.putString("description", searchResultData.getDescription());

				if (searchResultData.getType().equalsIgnoreCase("protect")) {
					jsonArrayProtect.addObject(jsonObjectData);
				} else if (searchResultData.getType().equalsIgnoreCase("perform")) {
					jsonArrayPerform.addObject(jsonObjectData);
				}
			}
		}

		jsonObject.putArray("protectResultDataList", jsonArrayProtect);
		jsonObject.putArray("performResultDataList", jsonArrayPerform);

		return jsonObject;
	}

	public static JsonObject baseInfoToJson(BaseStatus baseStatus) {
		JsonObject jsonObject = new JsonObject();

		jsonObject.putString("serverinfo", baseStatus.getServerInfo());
		jsonObject.putString("start", baseStatus.getStart());
		jsonObject.putString("performNumTestCases", Integer.toString(baseStatus.getPerformNumTestCases()));
		jsonObject.putString("protectNumTestCases", Integer.toString(baseStatus.getProtectNumTestCases()));

		return jsonObject;

	}

	public static JsonObject contentsListToJson(PerformPreParsedResponse parsedResponse) {
		JsonObject jsonObject = new JsonObject();

		JsonArray jsonArrayJS = new JsonArray();
		JsonArray jsonArrayCSS = new JsonArray();
		JsonArray jsonArrayImg = new JsonArray();

		if (parsedResponse != null) {

			// css
			for (ExtResource resource : parsedResponse.getExtCssResourceList()) {
				JsonObject jsonObjectData = new JsonObject();

				jsonObjectData.putString("absUrl", resource.getAbsUrl());
				jsonObjectData.putString("domain", resource.getDomain());
				jsonObjectData.putString("fileName", resource.getFileName());
				jsonObjectData.putString("linkLine", resource.getLinkLine());
				jsonObjectData.putString("responseBody", resource.getResponseBody());
				jsonObjectData.putString("url", resource.getUrl());
				jsonObjectData.putString("size", Integer.toString(resource.getSize()));
				jsonObjectData.putString("statusCode", Integer.toString(resource.getStatusCode()));

				jsonArrayCSS.addObject(jsonObjectData);
			}

			// js
			for (ExtResource resource : parsedResponse.getExtJsResourceList()) {
				JsonObject jsonObjectData = new JsonObject();

				jsonObjectData.putString("absUrl", resource.getAbsUrl());
				jsonObjectData.putString("domain", resource.getDomain());
				jsonObjectData.putString("fileName", resource.getFileName());
				jsonObjectData.putString("linkLine", resource.getLinkLine());
				jsonObjectData.putString("responseBody", resource.getResponseBody());
				jsonObjectData.putString("url", resource.getUrl());
				jsonObjectData.putString("size", Integer.toString(resource.getSize()));
				jsonObjectData.putString("statusCode", Integer.toString(resource.getStatusCode()));

				jsonArrayJS.addObject(jsonObjectData);
			}

			// img
			for (ImgTag imgTag : parsedResponse.getImgTagList()) {
				JsonObject jsonObjectData = new JsonObject();

				jsonObjectData.putString("linkLine", imgTag.getLinkLine());
				jsonObjectData.putString("url", imgTag.getUrl());
				jsonObjectData.putString("urlDomain", imgTag.getUrlDomain());
				jsonObjectData.putString("height", Integer.toString(imgTag.getHeight()));
				jsonObjectData.putString("width", Integer.toString(imgTag.getWidth()));
				jsonObjectData.putString("size", Integer.toString(imgTag.getImgInfo().getSize()));

				jsonArrayImg.addObject(jsonObjectData);
			}

		}

		jsonObject.putArray("jsList", jsonArrayJS);
		jsonObject.putArray("cssList", jsonArrayCSS);
		jsonObject.putArray("imgList", jsonArrayImg);

		return jsonObject;
	}

	public static JsonObject performRankToJson(Map<String, Integer> performRank) {
		JsonArray rankJsonArray = new JsonArray(performRank.entrySet().toArray());
		JsonObject jsonObject = new JsonObject();
		jsonObject.putArray("performRank", rankJsonArray);
		return jsonObject;
	}

	public static JsonObject historyData(TreeMap<String, String> performdata, TreeMap<String, String> protectdata, Map<String, TreeMap<String, String>> resources) {
		JsonObject jsonObject = new JsonObject();

		JsonArray perform = new JsonArray();
		JsonArray protect = new JsonArray();

		JsonArray jsNum = new JsonArray();
		JsonArray jsSize = new JsonArray();
		JsonArray cssNum = new JsonArray();
		JsonArray cssSize = new JsonArray();
		JsonArray imgNum = new JsonArray();
		JsonArray imgSize = new JsonArray();

		Iterator<String> it = performdata.keySet().iterator();

		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, performdata.get(key));
			perform.addObject(jsonObjectData);
			System.out.println(perform);
		}

		it = protectdata.keySet().iterator();

		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, protectdata.get(key));
			protect.addObject(jsonObjectData);
			System.out.println(protect);
		}
		
		/////
		
		it = resources.get("js_num").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("js_num").get(key));
			jsNum.addObject(jsonObjectData);
			System.out.println(jsNum);
		}
		
		it = resources.get("js_size").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("js_size").get(key));
			jsSize.addObject(jsonObjectData);
			System.out.println(jsSize);
		}
		
		it = resources.get("css_num").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("css_num").get(key));
			cssNum.addObject(jsonObjectData);
			System.out.println(cssNum);
		}
		
		it = resources.get("css_size").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("css_size").get(key));
			cssSize.addObject(jsonObjectData);
			System.out.println(cssSize);
		}
		
		it = resources.get("img_num").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("img_num").get(key));
			imgNum.addObject(jsonObjectData);
			System.out.println(imgNum);
		}
		
		it = resources.get("img_size").keySet().iterator();
		
		while (it.hasNext()) {
			JsonObject jsonObjectData = new JsonObject();
			String key = (String) it.next();
			jsonObjectData.putString(key, resources.get("img_size").get(key));
			imgSize.addObject(jsonObjectData);
			System.out.println(imgSize);
		}

		jsonObject.putArray("protect", protect);
		jsonObject.putArray("perform", perform);
		
		jsonObject.putArray("js_num", jsNum);
		jsonObject.putArray("js_size", jsSize);
		jsonObject.putArray("css_num", cssNum);
		jsonObject.putArray("css_size", cssSize);
		jsonObject.putArray("img_num", imgNum);
		jsonObject.putArray("img_size", imgSize);
		

		return jsonObject;
	}

	public static String urlToHost(String url) {

		URL targetURLFormat = null;
		try {
			targetURLFormat = new URL(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return targetURLFormat.getHost();
	}
}
package com.pnp.module.parser;

import java.util.Map;

import org.apache.http.HttpResponse;

import com.pnp.data.ParsedPostResponse;

public interface PostParser {
    public ParsedPostResponse parseResponse(HttpResponse httpResponse, Map<String, String> testCaseVerificationMap);

    public abstract ParsedPostResponse getProtectPostParsedResponse();
}
package com.pnp.module.parser;

import com.pnp.service.InspectionType;

public class PostParserFactory {

    public static PostParser create(InspectionType inspectionType) {
	switch (inspectionType) {
	case PROTECT:
	    return ProtectPostParser.create();
	case PERFORM:
	    return PerformPostParser.create();

	default:
	    throw new IllegalArgumentException("없는 카테고리");
	}
    }

}
package com.pnp.module.parser.perform;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pnp.common.Common;
import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.html.ExtResource;
import com.pnp.module.StrLib;
import com.pnp.module.UrlLib;

public class ExtResourceManager {

    private static HashMap<FileType, String> attributeMap;
    private static HashMap<FileType, String> extensionMap;
    private static HashMap<FileType, String> selectorMap;

    private final PerformPreParsedResponse preParsedResponse;

    private final ArrayList<String> status404UrlList;

    private HttpResponse response;
    private int extHeadCount;

    static {
	attributeMap = new HashMap<>();
	attributeMap.put(FileType.JS, "src");
	attributeMap.put(FileType.CSS, "href");

	extensionMap = new HashMap<>();
	extensionMap.put(FileType.JS, ".js");
	extensionMap.put(FileType.CSS, ".css");

	selectorMap = new HashMap<>();
	selectorMap.put(FileType.JS, "script[src]");
	selectorMap.put(FileType.CSS, "link[href]");
    }

    public ExtResourceManager(PerformPreParsedResponse preParsedResponse) {
	this.preParsedResponse = preParsedResponse;
	status404UrlList = new ArrayList<String>();
    }

    public ArrayList<ExtResource> makeExtJSList() {
	return makeExtList(FileType.JS);
    }

    public ArrayList<ExtResource> makeExtCSSList() {
	return makeExtList(FileType.CSS);
    }

    private ArrayList<ExtResource> makeExtList(FileType fileType) {
	Elements elements = preParsedResponse.getDocument().select(selectorMap.get(fileType));
	ArrayList<ExtResource> extResourceList = new ArrayList<>();

	for (Element element : elements) {
	    if (isValidExtResource(fileType, preParsedResponse.getDocument().baseUri(), element)) {
		String responseBody = Common.httpResponseToResponseBody(response);
		ExtResource extResource = new ExtResource();
		String url = element.attr(attributeMap.get(fileType));
		String absUrl = UrlLib.absUrl(element.baseUri(), url);
		Integer size = 0;

		if (responseBody != null)
		    size = responseBody.length();

		extResource.setHeaders(response.getAllHeaders());
		extResource.setAbsUrl(absUrl);
		extResource.setDomain(UrlLib.getHost(absUrl));
		extResource.setFileName(extractFileName(url));
		extResource.setLinkLine(StrLib.removeNewLine(element.toString()));
		responseBody = (fileType == FileType.JS) ? StrLib.removeJsComment(responseBody) : StrLib.removeCssComment(responseBody);
		extResource.setResponseBody(responseBody);
		extResource.setUrl(url);
		extResource.setSize(size);

		extResourceList.add(extResource);
	    }
	}
	getFromHeadCount(fileType);

	return extResourceList;
    }

    private boolean isValidExtResource(FileType fileType, String baseUri, Element element) {
	String linkUrl = element.attr(attributeMap.get(fileType));

	if (isValidExtension(linkUrl, fileType) == false) {
	    return false;
	}

	if ((linkUrl = UrlLib.absUrl(baseUri, linkUrl)).isEmpty()) {
	    return false;
	}

	if ((response = getResponse(linkUrl)) == null) {
	    return false;
	}

	return true;
    }

    private HttpResponse getResponse(String targetURL) {
	HttpResponse response = Common.exePNPRequest(targetURL);
	int status = response.getStatusLine().getStatusCode();

	if (status >= 200 && status < 300) {
	    HttpEntity entity = response.getEntity();
	    response = (entity != null) ? response : null;
	} else {
	    response = null;
	}

	if (status == 404) {
	    status404UrlList.add(targetURL);
	}

	return response;
    }

    private boolean isValidExtension(String linkUrl, FileType fileType) {
	String urlWithOutParameter = urlWithOutParameter(linkUrl);
	return urlWithOutParameter.endsWith(extensionMap.get(fileType));
    }

    private String urlWithOutParameter(String linkUrl) {
	int lastQuestionMarkIndex = (linkUrl.lastIndexOf("?") > -1) ? linkUrl.lastIndexOf("?") : linkUrl.length();
	String linkUrlWithoutParameter = linkUrl.substring(0, lastQuestionMarkIndex);
	return linkUrlWithoutParameter;
    }

    private String extractFileName(String url) {
	String fileName = urlWithOutParameter(url);
	int indexOfLastSlash = fileName.lastIndexOf("/");

	return fileName.substring(indexOfLastSlash + 1);
    }

    private void getFromHeadCount(FileType fileType) {
	Elements headTags = preParsedResponse.getDocument().select("head");
	extHeadCount = 0;
	switch (fileType) {
	case JS:
	    Elements scriptTags = headTags.get(0).select(selectorMap.get(fileType));
	    for (Element linkTag : scriptTags) {
		if ((linkTag != null) && (!linkTag.equals(""))) {
		    extHeadCount++;
		}
	    }
	    break;
	case CSS:
	    Elements linkTags = headTags.get(0).select("link");
	    extHeadCount = 0;
	    for (Element linkTag : linkTags) {
		if (linkTag.attr("rel").equalsIgnoreCase("stylesheet")) {
		    extHeadCount++;
		}
	    }
	    break;
	}
    }

    public int getExtHeadCount() {
	return extHeadCount;
    }

    public ArrayList<String> getStatus404UrlList() {
	return status404UrlList;
    }

}
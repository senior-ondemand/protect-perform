package com.pnp.module.parser.perform;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.html.IntResource;
import com.pnp.module.StrLib;

public class IntResourceManager {

    private static final int FIRST_NODE = 0;
    private static final int LINE_NO = 3;
    private static final HashMap<FileType, String> selectorMap;
    private final PerformPreParsedResponse preParsedResponse;

    static {
	selectorMap = new HashMap<>();
	selectorMap.put(FileType.JS, "script:not([src])");
	selectorMap.put(FileType.CSS, "style");
    }

    public IntResourceManager(PerformPreParsedResponse preParsedResponse) {
	this.preParsedResponse = preParsedResponse;
    }

    public ArrayList<IntResource> makeIntJsList() {
	return makeIntList(FileType.JS);
    }

    public ArrayList<IntResource> makeIntCssList() {
	return makeIntList(FileType.CSS);
    }

    private ArrayList<IntResource> makeIntList(FileType fileType) {
	Elements elements = preParsedResponse.getDocument().select(selectorMap.get(fileType));
	ArrayList<IntResource> intResourceList = new ArrayList<>();

	for (Element element : elements) {
	    if (isValidIntResource(element)) {
		IntResource intResource = new IntResource();
		intResource.setLinkLine(StrLib.subLine(element.toString(), LINE_NO));
		// inLine Js는 <script> 이후에 모든 내용이 childNode(0)에 저장
		intResource.setResponseBody(element.childNode(FIRST_NODE).toString());

		intResourceList.add(intResource);
	    }
	}

	return intResourceList;
    }

    private boolean isValidIntResource(Element element) {
	boolean result = true;

	if (element.childNodeSize() == 0) {
	    result = false;
	} else if (element.childNodeSize() > 1) {
	    throw new IllegalArgumentException("inLine Js 내용이 1개 존재하지 않음");
	}
	return result;
    }

    /**
     * 테스트 IntJs, IntCss 파일을 만드는 메소드
     *
     * @param element
     * @param i
     */
    private void writeToFile(FileType fileType, Element element, int i) {
	File file = new File("test/perform/y11/GmarketInt" + i + ".html");
	String encoding = "UTF8";
	i++;
	try {
	    FileUtils.writeStringToFile(file, element.childNode(FIRST_NODE).toString(), encoding);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
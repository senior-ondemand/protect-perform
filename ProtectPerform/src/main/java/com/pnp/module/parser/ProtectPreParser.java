package com.pnp.module.parser;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pnp.data.PreParsedResponse;
import com.pnp.data.ProtectPreParsedResponse;
import com.pnp.data.html.FormTag;
import com.pnp.data.html.InputTag;

public class ProtectPreParser implements PreParser {

    private String targetURL;
    private Document doc;

    private final ProtectPreParsedResponse preParsedResponse;

    private ProtectPreParser() {
	preParsedResponse = new ProtectPreParsedResponse();
    }

    public static PreParser create() {
	return new ProtectPreParser();
    }

    @Override
    public PreParsedResponse getParsedResponse() {
	return preParsedResponse;
    }

    @Override
    public void preParseResponse() {

	// parsing target url document
	try {

	    ArrayList<FormTag> formTagList;

	    doc = Jsoup.connect(targetURL).get();
	    formTagList = parseFormTagList();

	    preParsedResponse.setFormTagList(formTagList);

	} catch (Exception e) {
	    throw new IllegalArgumentException("잘못된 타켓 url");
	}

    }

    private ArrayList<FormTag> parseFormTagList() {

	ArrayList<FormTag> localFormTagList = new ArrayList<FormTag>();

	// parse form tags
	Elements forms = doc.select("form");

	// set form/input objects and insert to arraylist
	for (Element form : forms) {

	    if (checkHasInputTags(form)) {
		FormTag formTag = new FormTag();
		ArrayList<InputTag> inputTagList;

		inputTagList = parseInputTagList(form);
		formTag.setInputTags(inputTagList);

		formTag.setAction(form.attr("action"));
		formTag.setMethod(form.attr("method"));
		formTag.setName(form.attr("name"));
		formTag.setId(form.attr("id"));

		localFormTagList.add(formTag);
	    }
	}

	return localFormTagList;
    }

    private boolean checkHasInputTags(Element form) {

	Elements inputs = form.select("input");

	return !inputs.isEmpty();
    }

    private ArrayList<InputTag> parseInputTagList(Element form) {

	ArrayList<InputTag> inputTagList = new ArrayList<InputTag>();

	// set input objects
	Elements inputs = form.select("input");

	for (Element input : inputs) {

	    InputTag inputTag = new InputTag();

	    inputTag.setAutocomplete(input.attr("autocomplete"));
	    inputTag.setId(input.attr("id"));
	    inputTag.setName(input.attr("name"));
	    inputTag.setType(input.attr("type"));
	    inputTag.setValue(input.attr("value"));

	    // insert to input tag list
	    inputTagList.add(inputTag);
	}

	return inputTagList;
    }

    @Override
    public void setTargetURL(String targetURL) {
	this.targetURL = targetURL;
    }

}
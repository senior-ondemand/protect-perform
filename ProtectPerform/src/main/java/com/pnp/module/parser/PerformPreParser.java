package com.pnp.module.parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pnp.common.Common;
import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.PreParsedResponse;
import com.pnp.data.html.ImgInfo;
import com.pnp.data.html.ImgTag;
import com.pnp.module.StrLib;
import com.pnp.module.UrlLib;
import com.pnp.module.parser.perform.ExtResourceManager;
import com.pnp.module.parser.perform.IntResourceManager;

public class PerformPreParser implements PreParser {

    private final PerformPreParsedResponse preParsedResponse;
    private String targetURL;

    private PerformPreParser() {
	preParsedResponse = new PerformPreParsedResponse();
    }

    public static PerformPreParser create() {
	return new PerformPreParser();
    }

    @Override
    public void preParseResponse() {
	preParsedResponse.setTargetURL(targetURL);

	HttpResponse response = Common.exePNPRequest(targetURL);
	preParsedResponse.setResponseBody(Common.httpResponseToResponseBody(response));
	preParsedResponse.setTargetUrlResponse(response);

	try {
	    Document document = Jsoup.connect(targetURL).get();
	    String responseBody = StrLib.removeHtmlComment(document.toString());

	    preParsedResponse.setDocument(Jsoup.parse(responseBody));
	    preParsedResponse.getDocument().setBaseUri(targetURL);

	} catch (Exception e) {
	    throw new IllegalArgumentException("잘못된 타켓 url");
	}

	parseByJsoup();

	preParsedResponse.setFaviconInfo(getFavicon());
	preParsedResponse.setImgTagList(getImageTagList());
	preParsedResponse.setInlineCssSize(getInLineCssSize());
	preParsedResponse.setInlineJsSize(getInLineJsSize());
    }

    private void parseByJsoup() {
	ExtResourceManager extResourceManager = new ExtResourceManager(preParsedResponse);
	IntResourceManager intResourceManager = new IntResourceManager(preParsedResponse);

	preParsedResponse.setExtJSResourceList(extResourceManager.makeExtJSList());
	preParsedResponse.setExtJsAllCount(preParsedResponse.getExtJsResourceList().size());
	preParsedResponse.setExtJsHeadCount(extResourceManager.getExtHeadCount());
	preParsedResponse.setExtCSSResourceList(extResourceManager.makeExtCSSList());
	preParsedResponse.setExtCssAllCount(preParsedResponse.getExtCssResourceList().size());
	preParsedResponse.setExtCssHeadCount(extResourceManager.getExtHeadCount());
	
	preParsedResponse.setStatus404UrlList(extResourceManager.getStatus404UrlList());

	preParsedResponse.setIntJsResourceList(intResourceManager.makeIntJsList());
	preParsedResponse.setIntCssResourceList(intResourceManager.makeIntCssList());
    }

    @Override
    public PreParsedResponse getParsedResponse() {
	return preParsedResponse;
    }

    @Override
    public void setTargetURL(String targetUrl) {
	this.targetURL = targetUrl;
    }

    private ArrayList<ImgTag> getImageTagList() {

	// Get src domain in src tag
	Elements imgs = preParsedResponse.getDocument().select("img");
	ArrayList<ImgTag> imgTagList = new ArrayList<ImgTag>();
	for (Element img : imgs) {
	    if ((!img.attr("src").isEmpty()) && (!img.attr("src").equals(""))) {
		ImgTag imgTag = new ImgTag();
		String imgSrc = UrlLib.absUrl(targetURL, img.attr("src"));
		// Get hsot in url         
		URL imageURLFormat = null;
		try {
		    imageURLFormat = new URL(imgSrc);
		} catch (MalformedURLException e) {
		    e.printStackTrace();
		}

		imgTag.setUrlDomain(imageURLFormat.getHost());
		imgTag.setUrl(imgSrc);
		imgTag.setImgInfo(getImageInformation(imgSrc));

		if (!img.attr("width").isEmpty())
		    imgTag.setWidth(stringToInt(img.attr("width")));
		if (!img.attr("height").isEmpty())
		    imgTag.setHeight(stringToInt(img.attr("height")));

		imgTagList.add(imgTag);
	    }
	}

	return imgTagList;
    }

    private int stringToInt(String string) {
	if(string.equalsIgnoreCase("auto"))
	    return 0;
	int length = string.length();
	int i = 0;
	for (i = 0; i < length; i++) {
	    if (!Character.isDigit(string.charAt(i))) {
		break;
	    }
	}
	return Integer.parseInt(string.substring(0, i));
    }

    private ImgInfo getImageInformation(String url) {
	ImgInfo imgInfo = new ImgInfo();
	HttpResponse response = Common.exePNPRequest(url);
	HttpEntity responseEntity = response.getEntity();
	if (responseEntity != null) {
	    try {
		byte[] iconBytes = EntityUtils.toByteArray(responseEntity);
		ImageIcon ii = new ImageIcon(iconBytes);
		imgInfo.setWidth(ii.getIconWidth());
		imgInfo.setHeight(ii.getIconHeight());
		imgInfo.setSize(iconBytes.length);
		imgInfo.setMimeType(EntityUtils.getContentMimeType(responseEntity));
		imgInfo.setResponseHeader(response.getAllHeaders());
		imgInfo.setStatusCode(response.getStatusLine().getStatusCode());
	    } catch (ParseException e) {
		e.printStackTrace();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	return imgInfo;
    }

    private ImgInfo getFavicon() {
	ImgInfo faviconInfo = new ImgInfo();
	URL imageURLFormat = null;
	try {
	    imageURLFormat = new URL(targetURL);
	} catch (MalformedURLException e) {
	    e.printStackTrace();
	}

	HttpResponse response = Common.exePNPRequest(imageURLFormat.getProtocol() + "://" + imageURLFormat.getHost() + "/favicon.ico");

	if (!response.getStatusLine().toString().contains("404")) {

	    HttpEntity responseEntity = response.getEntity();
	    if (responseEntity != null) {
		try {
		    // http://en.wikipedia.org/wiki/ICO_(file_format)

		    byte[] iconBytes = EntityUtils.toByteArray(responseEntity);
		    faviconInfo.setWidth(iconBytes[6] & 0xFF);
		    faviconInfo.setHeight(iconBytes[7] & 0xFF);
		    faviconInfo.setMimeType(EntityUtils.getContentMimeType(responseEntity));
		    faviconInfo.setSize(iconBytes.length);
		    faviconInfo.setResponseHeader(response.getAllHeaders());
		} catch (ParseException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	} else {
	    faviconInfo = null;
	}

	return faviconInfo;
    }

    private int getInLineCssSize() {
	return preParsedResponse.getDocument().select("style").toString().length();
    }

    private int getInLineJsSize() {
	int inlineJsSize = 0;

	Elements scriptTags = preParsedResponse.getDocument().select("script");
	for (Element scriptTag : scriptTags) {
	    if (scriptTag.attr("src").isEmpty()) {
		inlineJsSize += scriptTag.toString().length();
	    }
	}

	return inlineJsSize;
    }

}
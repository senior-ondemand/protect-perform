package com.pnp.module.parser;

import com.pnp.data.PreParsedResponse;

public interface PreParser {
    public void preParseResponse();

    public void setTargetURL(String targetURL);

    public PreParsedResponse getParsedResponse();

    // 1. 매칭되는 String 리턴
    // 리턴형태? 단순 해당 String이 포함된 DOM? 단순 스트링이 있는지 없는지?

    // 2. 매칭되는 tag(form) 모두 리턴(children 포함)
    // child 형태로 제공

    // 3.

}
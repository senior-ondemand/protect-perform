package com.pnp.module.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;

import com.pnp.data.ParsedPostResponse;
import com.pnp.data.ProtectPostParsedResponse;

public class ProtectPostParser implements PostParser {

    private final int indexInterval = 20;
    private final ParsedPostResponse protectPostParsedResponse;

    private ProtectPostParser() {
	protectPostParsedResponse = new ProtectPostParsedResponse();
    }

    public static PostParser create() {
	return new ProtectPostParser();
    }

    @Override
    public ParsedPostResponse parseResponse(HttpResponse httpResponse, Map<String, String> verifications) {
	Header[] headers = httpResponse.getAllHeaders();
	ResponseHandler<String> handler = new BasicResponseHandler();
	String responseBody = null;

	try {
	    int status = httpResponse.getStatusLine().getStatusCode();
	    if (status >= 200 && status < 300)
		responseBody = handler.handleResponse(httpResponse);
	} catch (IOException e) {
	    e.printStackTrace();
	}

	Set<String> keySet = verifications.keySet();
	List<String> keys = new ArrayList<String>(keySet);

	for (String currentKey : keys) {
	    String currentValue = verifications.get(currentKey);
	    if (currentValue != null) {
		switch (currentKey) {

		case "header_param0":
		    ArrayList<String> headerResultList = new ArrayList<String>();

		    for (Header currentHeader : headers) {
			if (currentHeader.getName().equals(currentValue)) {
			    String currentKeyValue = verifications.get(currentKey);
			    if (currentKeyValue != null)
				if (currentKeyValue.contains(currentKeyValue)) {

				    String currentResult = verifications.get(currentValue) + ": " + currentKeyValue;
				    headerResultList.add(currentResult);
				}
			}
		    }

		    protectPostParsedResponse.setHeaderResultList(headerResultList);
		    break;

		case "status_code":
		    protectPostParsedResponse.setStatusCode(currentValue);
		    break;

		case "body":
		    ArrayList<String> bodyResultList = new ArrayList<String>();
		    String result;
		    Integer indexOfMatchedString;

		    if ((indexOfMatchedString = responseBody.indexOf(currentValue)) != -1) {
			Integer beginIndex = indexOfMatchedString - indexInterval;
			Integer endIndex = indexOfMatchedString + indexInterval;

			if (beginIndex < 0)
			    beginIndex = 0;
			if (endIndex > responseBody.length())
			    endIndex = responseBody.length();

			result = responseBody.substring(beginIndex, endIndex);
			bodyResultList.add(result);
		    }

		    protectPostParsedResponse.setBodyResultList(bodyResultList);
		    break;
		}
	    }
	}

	return protectPostParsedResponse;
    }

    @Override
    public ParsedPostResponse getProtectPostParsedResponse() {
	return protectPostParsedResponse;
    }

}
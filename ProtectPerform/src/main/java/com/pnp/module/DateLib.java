package com.pnp.module;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateLib {

    private static final String STANDARD_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String currentDateString() {
	DateTimeFormatter formatter = DateTimeFormat.forPattern(STANDARD_DATETIME_FORMAT);
	DateTime currentDate = new DateTime();

	return currentDate.toString(formatter);
    }

}

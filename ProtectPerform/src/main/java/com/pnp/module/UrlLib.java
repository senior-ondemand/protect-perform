package com.pnp.module;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.whois.WhoisClient;
import org.apache.commons.validator.routines.UrlValidator;

public class UrlLib {

    private static final String[] allowedProtocols = { "http", "https" };
    private static UrlValidator urlValidator;

    static {
	urlValidator = new UrlValidator(allowedProtocols);
    }

    public static boolean isValidUrl(String url) {
	return urlValidator.isValid(url);
    }

    public static String absUrl(String baseUri, String targetUrl) {
	baseUri = StringUtils.removeEnd(baseUri, "/");

	if (urlValidator.isValid(removeDoubleSlashs(targetUrl)) == false) {
	    if (startsWithDoubleSlashes(targetUrl)) {
		targetUrl = getProtocol(baseUri) + ":" + targetUrl;
	    } else if (startsWithOneSlash(targetUrl)) {
		targetUrl = getProtocol(baseUri) + "://" + getHost(baseUri) + targetUrl;
	    } else if (startsWithAlpha(targetUrl)) {
		targetUrl = relativeToAbsUrl(baseUri, targetUrl);
	    }

	    if (urlValidator.isValid(targetUrl) == false) {
		return StringUtils.EMPTY;
	    }
	}

	return targetUrl;
    }

    private static String relativeToAbsUrl(String domain, String targetUrl) {
	String absUrl = StringUtils.EMPTY;
	String host = StringUtils.EMPTY;

	URL url = null;
	try {
	    url = new URL(domain);
	    host = url.getProtocol() + "://" + url.getHost();

	    if (host != null && host.equals(domain)) {
		absUrl = host + "/" + targetUrl;
	    } else {
		absUrl = getParent(domain) + targetUrl;
	    }
	} catch (MalformedURLException e) {
	    e.printStackTrace();
	}

	return absUrl;
    }

    private static String removeDoubleSlashs(String targetUrl) {
	if (targetUrl.contains("http://"))
	    return "http://" + targetUrl.substring(7).replaceAll("//+", "/");
	else if (targetUrl.contains("https://"))
	    return "http://" + targetUrl.substring(8).replaceAll("//+", "/");
	else
	    return targetUrl.substring(0, 1) + targetUrl.substring(1).replace("//", "/");
    }

    private static String getProtocol(String domain) {
	String protocol = "";
	try {
	    URL url = new URL(domain);
	    protocol = url.getProtocol();
	} catch (MalformedURLException e) {
	    e.printStackTrace();
	}

	return protocol;
    }

    private static boolean startsWithAlpha(String targetUrl) {
	return (!startsWithOneSlash(targetUrl) && !startsWithDoubleSlashes(targetUrl));
    }

    private static boolean startsWithOneSlash(String targetUrl) {
	return targetUrl.charAt(0) == '/' && targetUrl.charAt(1) != '/';
    }

    private static boolean startsWithDoubleSlashes(String targetUrl) {
	return targetUrl.startsWith("//") == true;
    }

    public static String getHost(String absUrl) {
	URL url = null;
	try {
	    url = new URL(absUrl);
	} catch (MalformedURLException e) {
	    e.printStackTrace();
	}
	return url.getHost();
    }

    public static String getParent(String path) {
	URL url = null;

	try {
	    url = new URL(path);
	    String file = url.getFile();
	    int len = file.length();
	    if (len == 0 || len == 1 && file.charAt(0) == '/')
		return null;
	    int lastSlashIndex = -1;
	    for (int i = len - 2; lastSlashIndex == -1 && i >= 0; --i) {
		if (file.charAt(i) == '/')
		    lastSlashIndex = i;
	    }
	    if (lastSlashIndex == -1)
		file = StringUtils.EMPTY;
	    else
		file = file.substring(0, lastSlashIndex + 1);
	    url = new URL(url.getProtocol(), url.getHost(), url.getPort(), file);
	} catch (MalformedURLException e1) {
	    e1.printStackTrace();
	}

	return url.toString();
    }

    public static String getWhois(String domain) {
	StringBuilder result = new StringBuilder();
	WhoisClient whois = new WhoisClient();

	try {
	    whois.connect(WhoisClient.DEFAULT_HOST);

	    // whois =google.com
	    String whoisData1 = whois.query("=" + domain);

	    // append first result
	    result.append(whoisData1);
	    whois.disconnect();

	    // get the google.com whois server - whois.markmonitor.com
	    String whoisServerUrl = getWhoisServer(whoisData1);
	    if (!whoisServerUrl.equals("")) {

		// whois -h whois.markmonitor.com google.com
		String whoisData2 = queryWithWhoisServer(domain, whoisServerUrl);

		// append 2nd result
		result.append(whoisData2);
	    }

	} catch (SocketException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return result.toString();
    }

    private static String queryWithWhoisServer(String domain, String whoisServer) {
	String result = StringUtils.EMPTY;
	WhoisClient whois = new WhoisClient();

	try {
	    whois.connect(whoisServer);
	    result = whois.query(domain);
	    whois.disconnect();
	} catch (SocketException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return result;
    }

    private static String getWhoisServer(String whois) {
	String result = StringUtils.EMPTY;
	String WHOIS_SERVER_PATTERN = "Whois Server:\\s(.*)";
	Pattern pattern = Pattern.compile(WHOIS_SERVER_PATTERN);
	Matcher matcher = pattern.matcher(whois);

	// get last whois server
	while (matcher.find()) {
	    result = matcher.group(1);
	}

	return result;
    }

    /**
     *
     * 출처 http://www.firatatagun.com/java-email-extractor-extract-email-address-
     * from-string-text-file
     *
     * @param str
     * @return
     */
    public static ArrayList<String> getEmailList(String str) {
	String RE_MAIL = "([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})";
	Pattern p = Pattern.compile(RE_MAIL);
	Matcher m = p.matcher(str);
	ArrayList<String> emailList = new ArrayList<String>();

	while (m.find()) {
	    if (!emailList.contains(m.group(1))) {
		emailList.add(m.group(1));
	    }
	}

	return emailList;
    }
}

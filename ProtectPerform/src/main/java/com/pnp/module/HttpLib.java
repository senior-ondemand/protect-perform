package com.pnp.module;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpLib {

    public static CloseableHttpResponse requestGet(String url) {
	HttpRequestBase httpGet = new HttpGet(url);
	return requestUrl(httpGet);
    }

    public static CloseableHttpResponse requestPost(String url) {
	HttpRequestBase httpPost = new HttpPost(url);
	return requestUrl(httpPost);
    }

    private static CloseableHttpResponse requestUrl(HttpRequestBase httpRequestBase) {
	CloseableHttpClient httpclient = HttpClients.createDefault();
	CloseableHttpResponse response = null;
	HttpEntity entity = null;
	try {
	    response = httpclient.execute(httpRequestBase);
	    entity = response.getEntity();
	    EntityUtils.consume(entity);
	    response.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return response;
    }

}

package com.pnp.module;

import java.util.ArrayList;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.TestCase;
import com.pnp.module.protect.Protect;
import com.pnp.module.protect.VulnAnalyzer;
import com.pnp.service.InspectionType;

public class ProtectInspection extends Inspection {

    private static Logger logger = LoggerFactory.getLogger(ProtectInspection.class);

    private ProtectInspection() {
	super(InspectionType.PROTECT);
    }

    public static ProtectInspection create() {
	return new ProtectInspection();
    }

    @Override
    public void exe() {
	logger.info("ProtectInsepction Run");
	VulnAnalyzer vulnAnalyzer = new VulnAnalyzer();
	ArrayList<TestCase> testCases = rule.getTestCases();
	Protect protect = null;

	for (TestCase testCase : testCases) {
	    protect = new Protect(targetURL, testCase, preParsedResponse);
	    protect.exe();
	    boolean isVerification = vulnAnalyzer.analyze(protect.getProtectResult(), testCase.getTestCaseVerification());
	    Integer isVerification_result = isVerification ? 1 : 0;

	    if (socketIOCallback != null) {
		socketIOCallback.emit(inspectionType.toString(),
			PNPConverter.protectResultToJson(isVerification, rule.getTitle(), testCase, protect.getUri()));
	    }

	    scanResultDAO.set(index, testCase.getTestCaseCategory(), isVerification_result, userSetting);
	}
    }

}
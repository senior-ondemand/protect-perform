package com.pnp.module;

import org.apache.commons.validator.routines.EmailValidator;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;
import org.mindrot.jbcrypt.BCrypt;

import org.vertx.java.core.json.JsonObject;

import com.pnp.controller.IllegalUserDataException;
import com.pnp.controller.IllegalUserDataException.UserDataField;
import com.pnp.data.User;
import com.pnp.data.UserSetting;

public class Validator {

    private static Logger logger = LoggerFactory.getLogger(Validator.class);

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final int NAME_MIN_LENGTH = 4;
    private static final int NAME_MAX_LENGTH = 50;

    public static boolean validateSignUpInput(User user) throws IllegalUserDataException {
	if (isValidUserEmail(user) && isValidUserPassword(user) && equalsToRePassword(user) && isValidUserName(user) && isValidUserUrl(user)) {
	    return true;
	}

	return false;
    }

    public static boolean validateSignInInput(User user) throws IllegalUserDataException {
	if (isValidUserEmail(user) && isValidUserPassword(user)) {
	    return true;
	}

	return false;
    }

    private static boolean equalsToRePassword(User user) throws IllegalUserDataException {
	if (user.getPassword().equals(user.getRePassword()) == false) {
	    throw new IllegalUserDataException(UserDataField.UserPassword, "Password Does Not Match with Confirm Password");
	}

	return true;
    }

    public static boolean isValidUserEmail(User user) throws IllegalUserDataException {
	if (EmailValidator.getInstance().isValid(user.getEmail()) == false) {
	    throw new IllegalUserDataException(UserDataField.UserEmail, "Invalid Email Format");
	}

	return true;
    }

    public static boolean isValidUserPassword(User user) throws IllegalUserDataException {
	if (user.getPassword().isEmpty()) {
	    throw new IllegalUserDataException(UserDataField.UserPassword, "Password is Empty");
	} else if (user.getPassword().length() < PASSWORD_MIN_LENGTH) {
	    throw new IllegalUserDataException(UserDataField.UserPassword, "Length of Password is Shorter than " + PASSWORD_MIN_LENGTH);
	}

	return true;
    }

    public static boolean isValidUserName(User user) throws IllegalUserDataException {
	if (user.getName().isEmpty()) {
	    throw new IllegalUserDataException(UserDataField.UserName, "User Name is Empty");
	} else if (user.getName().length() < NAME_MIN_LENGTH) {
	    throw new IllegalUserDataException(UserDataField.UserName, "Length of User Name is Shorter than " + NAME_MIN_LENGTH);
	} else if (user.getName().length() > NAME_MAX_LENGTH) {
	    throw new IllegalUserDataException(UserDataField.UserName, "Length of User Name is Longer than " + NAME_MAX_LENGTH);
	}

	return true;
    }

    private static boolean isValidUserUrl(User user) throws IllegalUserDataException {
	if (UrlLib.isValidUrl(user.getUrl()) == false) {
	    throw new IllegalUserDataException(UserDataField.UserUrl, "User Url is Invalid");
	}

	return true;
    }

    public static boolean validateWithStoredPassword(User user, User userFromRedis) throws IllegalUserDataException {
	if (BCrypt.checkpw(user.getPassword(), userFromRedis.getPassword()) == false) {
	    throw new IllegalUserDataException(UserDataField.UserPassword, "Your Information is not Valid");
	}

	return true;
    }

    public static boolean isValidScan(JsonObject event, UserSetting userSetting) {
	return (signInAndPerformScan(event, userSetting) || signInAndProtectScanAndUserUrl(event, userSetting) || signOffAndPerformScan(event,
		userSetting));
    }

    private static boolean signOffAndPerformScan(JsonObject event, UserSetting userSetting) {
	return event.getString("status").equals("error") && userSetting.getScanType().equals("Perform");
    }

    private static boolean signInAndPerformScan(JsonObject event, UserSetting userSetting) {
	return event.getString("status").equals("ok") && userSetting.getScanType().equals("Perform");
    }

    private static boolean signInAndProtectScanAndUserUrl(JsonObject event, UserSetting userSetting) {
	return event.getString("status").equals("ok") && (userSetting.getScanType().equals("Protect") || userSetting.getScanType().equals("PNP"))
		&& UrlLib.getHost(userSetting.getTargetURL()).equals(UrlLib.getHost(event.getObject("data").getObject("data").getString("userUrl")));
    }
}

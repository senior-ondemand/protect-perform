package com.pnp.module;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.lang3.StringUtils;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

public class SSlLib {

    public enum Version {
	SSL_2("SSLv2"), SSL_3("SSLv3"), TLS1("TLSv1");

	private final String sslVersion;

	private Version(String sslVersion) {
	    this.sslVersion = sslVersion;
	}

	public String getSslVersion() {
	    return sslVersion;
	}
    }

    private static Logger logger = LoggerFactory.getLogger(SSlLib.class);

    public static String getSslVersion(String host) {
	String sslVersion = StringUtils.EMPTY;

	SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

	SSLSocket sslsock = null;
	SSLSession session = null;
	try {
	    sslsock = (SSLSocket) factory.createSocket(host, 443);
	    session = sslsock.getSession();
	    sslsock.close();
	    sslVersion = session.getProtocol();
	} catch (Exception e) {
	    logger.info(host + " does not support SSL");
	}
	return sslVersion;
    }

    public static boolean isSecureVersion(String sslVersion) {
	return sslVersion.startsWith(Version.SSL_3.getSslVersion()) || sslVersion.startsWith(Version.TLS1.getSslVersion());
    }
}

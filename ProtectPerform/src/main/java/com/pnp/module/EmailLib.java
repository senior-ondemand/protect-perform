package com.pnp.module;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

public class EmailLib implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(EmailLib.class);

    private final String to;
    private final String text;

    private EmailLib(String to, String text) {
	this.to = to;
	this.text = text;
    }

    public static boolean sendEmailByGmail(String to, String text) {
	EmailLib emailLib = new EmailLib(to, text);
	new Thread(emailLib).start();

	return true;
    }

    private void send(String to, String text) {
	final String username = "admin@protectnperform.com";
	final String password = "tkgkrsus";

	Properties props = new Properties();
	props.put("mail.smtp.auth", true);
	props.put("mail.smtp.starttls.enable", true);
	props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "587");

	Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	    @Override
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	    }
	});

	try {
	    Message message = new MimeMessage(session);
	    message.setFrom(new InternetAddress("admin@protectnperform.com"));
	    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	    message.setSubject("Protect & Perform Account Confirmation");
	    message.setText(text);

	    Transport.send(message);
	    logger.info("Email to \"" + to + "\" has been sents");
	} catch (MessagingException e) {
	    throw new RuntimeException(e);
	}
    }

    @Override
    public void run() {
	send(to, text);
    }

}

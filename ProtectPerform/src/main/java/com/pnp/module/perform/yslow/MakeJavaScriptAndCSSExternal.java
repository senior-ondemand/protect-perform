package com.pnp.module.perform.yslow;

import com.pnp.module.perform.Perform;

public class MakeJavaScriptAndCSSExternal extends Perform {

    private static final double MAXIMUM_THRESHOLD = 1; // (%)
    private static final int MAXIMUM_GRADE = 100;

    // count
    private int inlineJsSize;
    private int inlineCssSize;
    
    public static Perform create() {
	return new MakeJavaScriptAndCSSExternal();
    }

    @Override
    public void exe() {
	int resultGrade;

	inlineCssSize = preParsedResponse.getInlineCssSize();
	inlineJsSize = preParsedResponse.getInlineJsSize();
	int bodySize = preParsedResponse.getDocument().toString().length();
	final int maximumSize = (int) ((float) bodySize / 100.0 * MAXIMUM_THRESHOLD);

	resultBuilder.append("There is a total of ").append(inlineCssSize).append(" inline CSS size<br>");
	resultBuilder.append("There is a total of ").append(inlineJsSize).append(" inline JavaScript size<br>");

	solutionBuilder.append("Internal CSS and Javascript to an external file Write in.");

	double resultRate = ((float) (inlineCssSize + inlineJsSize) / (float) bodySize) * 100.0;
	resultGrade = MAXIMUM_GRADE - (int) (((double) (inlineCssSize + inlineJsSize) / (double) maximumSize) * 100.0);

	if (resultGrade < 0)
	    resultGrade = 0;

	resultBuilder.append("CSS and JavaScript : #").append(String.format("%.2f", resultRate)).append("% 를 차지합니다.");

	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }

    public Integer getInlineJsSize() {
	return inlineJsSize;
    }
    
    public Integer getInlineCssSize() {
	return inlineCssSize;
    }
}

package com.pnp.module.perform.yslow;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.pnp.data.html.ExtResource;
import com.pnp.module.perform.Perform;

public class AvoidCSSExpressions extends Perform {

    private static final String CSS_EXPRESSION_REGEX = "expression\\s*\\((.*)\\)\\s*";
    private static final String STYLE_SELECTOR = "style";
    private static final String CSS_EXPRESSION_SELECTOR = "*[style~=expression]";

    // count
    private int cssExpFromExtCssCnt;
    private int cssExpFromBodyCnt;

    private int grade;

    private AvoidCSSExpressions() {
	grade = 100;
    }

    public static Perform create() {
	return new AvoidCSSExpressions();
    }

    @Override
    public void exe() {
	countFromExtCss();
	countFromBody();

	if (cssExpFromExtCssCnt > 0 || cssExpFromBodyCnt > 0) {
	    solutionBuilder.append("Consider Using One \"Time Expressions\" or \"Event Handlers\"\n");

	    grade = 0;
	}

	summary();
    }

    private void countFromExtCss() {
	StringBuilder cssExpressionFromCssBuilder = new StringBuilder();
	cssExpFromExtCssCnt = 0;

	for (ExtResource extResource : preParsedResponse.getExtCssResourceList()) {
	    Pattern pattern = Pattern.compile(CSS_EXPRESSION_REGEX);
	    Matcher matcher = pattern.matcher(extResource.getResponseBody());
	    int cssExpressionCnt = 0;

	    while (matcher.find()) {
		cssExpressionFromCssBuilder.append(matcher.group()).append(BR);
		cssExpressionCnt++;
	    }

	    cssExpFromExtCssCnt += cssExpressionCnt;

	    if (cssExpressionCnt > 0) {
		resultBuilder.append("The Number of CSS Expression From \"");
		resultBuilder.append(extResource.getUrl()).append("\": ");
		resultBuilder.append(cssExpressionCnt).append(BR);
		resultBuilder.append(cssExpressionFromCssBuilder);
	    }
	}
    }

    private void countFromBody() {
	StringBuilder cssExpressionsFromBodyBuilder = new StringBuilder();
	cssExpFromBodyCnt = 0;

	// <style>
	Pattern pattern = Pattern.compile(CSS_EXPRESSION_REGEX);
	Matcher matcher = pattern.matcher(preParsedResponse.getDocument().select(STYLE_SELECTOR).toString());

	while (matcher.find()) {
	    cssExpressionsFromBodyBuilder.append(matcher.group()).append(BR);
	    cssExpFromBodyCnt++;
	}

	// <... style=... />
	cssExpFromBodyCnt += preParsedResponse.getDocument().select(CSS_EXPRESSION_SELECTOR).size();

	if (cssExpFromBodyCnt > 0) {
	    resultBuilder.append("The Number of CSS Expression From Body: ");
	    resultBuilder.append(cssExpFromBodyCnt).append(BR);
	    resultBuilder.append(cssExpressionsFromBodyBuilder);
	}
    }

    private void summary() {
	resultBuilder.append(BR);
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setGrade(grade);
    }

    public int getCssExpFromExtCssCnt() {
	return cssExpFromExtCssCnt;
    }

    public int getCssExpFromBodyCnt() {
	return cssExpFromBodyCnt;
    }

}
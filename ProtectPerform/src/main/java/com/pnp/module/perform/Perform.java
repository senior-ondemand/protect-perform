package com.pnp.module.perform;

import org.apache.commons.lang3.StringUtils;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.PreParsedResponse;

public abstract class Perform {

    protected static final int MAXIMUM_GRADE = 100;
    protected String BR = "<br>";

    private String title;
    private PerformRuleType typeCode;
    protected PerformPreParsedResponse preParsedResponse;
    protected String targetURL = StringUtils.EMPTY;
    private String result = StringUtils.EMPTY;
    private String solution = StringUtils.EMPTY;
    private int grade = 100;

    protected StringBuilder resultBuilder;
    protected StringBuilder solutionBuilder;

    public abstract void exe();

    public void initialize(PerformRuleType typeCode, String targetUrl, PreParsedResponse preParsedResponse) {
	this.typeCode = typeCode;
	this.targetURL = targetUrl;
	this.preParsedResponse = (PerformPreParsedResponse) preParsedResponse;

	resultBuilder = new StringBuilder();
	solutionBuilder = new StringBuilder();
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public PerformRuleType getTypeCode() {
	return typeCode;
    }

    public void setTypeCode(PerformRuleType typeCode) {
	this.typeCode = typeCode;
    }

    public String getCategory() {
	return "PER:" + typeCode;
    }

    public PreParsedResponse getPreParsedResponse() {
	return preParsedResponse;
    }

    public void setPreParsedResponse(PreParsedResponse preParsedResponse) {
	this.preParsedResponse = (PerformPreParsedResponse) preParsedResponse;
    }

    public String getTargetURL() {
	return targetURL;
    }

    public void setTargetURL(String targetURL) {
	this.targetURL = targetURL;
    }

    public String getResult() {
	return result;
    }

    public void setResult(String performResult) {
	this.result = performResult;
    }

    public String getSolution() {
	return solution;
    }

    public void setSolution(String solution) {
	this.solution = solution;
    }

    public int getResultGrade() {
	return getGrade();
    }

    public void setResultGrade(int resultGrade) {
	this.setGrade(resultGrade);
    }

    public int getGrade() {
	return grade;
    }

    public void setGrade(int grade) {
	this.grade = grade;
    }

}
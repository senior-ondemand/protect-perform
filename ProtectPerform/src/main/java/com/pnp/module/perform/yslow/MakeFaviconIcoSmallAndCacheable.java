package com.pnp.module.perform.yslow;

import org.apache.http.Header;

import com.pnp.data.html.ImgInfo;
import com.pnp.module.perform.Perform;

public class MakeFaviconIcoSmallAndCacheable extends Perform {

    private static final int MAXIMUM_SIZE = 2000;
    
    private int faviconSize;
    private boolean isFaviconCacheable;

    public static Perform create() {
	return new MakeFaviconIcoSmallAndCacheable();
    }

    @Override
    public void exe() {
	int resultGrade = 0;

	ImgInfo faviconInfo = preParsedResponse.getFaviconInfo();
	
	faviconSize = faviconInfo.getSize();
	int faviconWidth = faviconInfo.getWidth();
	int faviconHeight = faviconInfo.getHeight();
	
	if (faviconSize == 0) {
	    resultBuilder.append("favicon.ico is not found<br>");
	} else {
	    if (faviconSize > MAXIMUM_SIZE) {
		resultBuilder.append("Size of favicon.ico is very big(more than 2000 bytes)<br>");
		solutionBuilder.append("Reduce size of favicon.ico less than 2000 bytes<br>");
	    } else {
		resultBuilder.append("Size of favicon.ico is suitable(less than 2000 bytes)<br>");
	    }

	    resultBuilder.append("favicon.ico infomation : <br>");
	    resultBuilder.append("\t width  : ").append(faviconWidth).append("px<br>");
	    resultBuilder.append("\t height : ").append(faviconHeight).append("px<br>");
	    resultBuilder.append("\t size   : ").append(faviconSize).append("bytes<br>");
	    resultBuilder.append("<br>");

	    resultGrade = (int) (((float) faviconSize - (float) MAXIMUM_SIZE) / 1000.0);

	    if (resultGrade < 0) {
		resultGrade = 50;
	    } else {
		resultGrade = 50 - resultGrade;
		if (resultGrade < 0) {
		    resultGrade = 0;
		}
	    }
	}

	isFaviconCacheable = checkExpiresOrCacheControlHeader(faviconInfo.getResponseHeader());
	if (isFaviconCacheable) {
	    resultGrade += 50;
	} else {
	    resultBuilder.append("favicon.ico is not cacheable<br>");
	    solutionBuilder.append("Make favicon.ico enable the cache.<br>");
	}

	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }

    private boolean checkExpiresOrCacheControlHeader(Header[] headers) {
	for (Header header : headers) {

	    // 1. Cache-control : no-cache, no-store, must-revalidate
	    if (header.getName().equalsIgnoreCase("cache-control")) {

		String value = header.getValue();

		if (value.contains("no-cache") || value.contains("no-store") || value.contains("must-revalidate")) {
		    return false;
		} else if (value.contains("max-age=0")) {
		    return false;
		}

	    } else if (header.getName().equalsIgnoreCase("expires")) {
		return true;
	    }
	}
	return false;
    }
    
    public int getFaviconSize() {
	return faviconSize;
    }
    
    public int getIsFaviconCacheable() {
	if (!isFaviconCacheable)
	    return 1;
	return 0;
    }
}

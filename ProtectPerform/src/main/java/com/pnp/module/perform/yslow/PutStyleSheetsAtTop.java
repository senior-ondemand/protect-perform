package com.pnp.module.perform.yslow;

import com.pnp.module.perform.Perform;

public class PutStyleSheetsAtTop extends Perform {

    private static final int MAXIMUM_THRESHOLD = 10;
    private static final int MAXIMUM_GRADE = 100;

 // count
    private int extCssOtherCount;
    
    public static Perform create() {
	return new PutStyleSheetsAtTop();
    }

    @Override
    public void exe() {
	int resultGrade;

	int extCssAllCount = preParsedResponse.getExtCssAllCount();
	int extCssHeadCount = preParsedResponse.getExtCssHeadCount();
	extCssOtherCount = extCssAllCount - extCssHeadCount;

	resultBuilder
		.append("Moving style sheets to the document HEAD element <br>helps pages appear to load quicker since this allows pages to render progressively.<br><br>");
	resultBuilder.append("number of external CSS in head tag  : ").append(extCssHeadCount).append("<br>");
	resultBuilder.append("number of external CSS in other tag : ").append(extCssOtherCount).append("<br>");

	solutionBuilder.append("Write CSS in the head tag.");

	resultGrade = MAXIMUM_GRADE - (int) (((float) extCssOtherCount / (float) MAXIMUM_THRESHOLD) * 100.0);

	if ( resultGrade < 0)
	    resultGrade = 0;
	
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }
    
    public int getExtCssOtherCount() {
	return extCssOtherCount;
    }
}

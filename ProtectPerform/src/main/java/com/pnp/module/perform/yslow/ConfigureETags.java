package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.http.Header;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class ConfigureETags extends Perform {

    private int totalCount;
    private int problemCount;
    private int grade;
    private StringBuffer resultString = new StringBuffer();

    private ConfigureETags() {

    }

    public static Perform create() {
	return new ConfigureETags();
    }

    @Override
    public void exe() {

	Header currentHeader;

	// check targetURL http response
	ArrayList<ExtResource> jsResourceList = preParsedResponse.getExtJsResourceList();
	ArrayList<ExtResource> cssResourceList = preParsedResponse.getExtCssResourceList();
	ArrayList<ImgTag> imgTagList = preParsedResponse.getImgTagList();

	// js list
	for (ExtResource currentJsResource : jsResourceList) {
	    totalCount++;
	    currentHeader = test(currentJsResource);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentJsResource.getFileName() + "<br>");
		appendResultString("problem : " + currentHeader.toString() + "<br><br>");
	    }
	}

	// css list
	for (ExtResource currentCssResource : cssResourceList) {
	    totalCount++;
	    currentHeader = test(currentCssResource);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentCssResource.getFileName() + "<br>");
		appendResultString("problem : " + currentHeader.toString() + "<br><br>");
	    }
	}

	// img list
	for (ImgTag currentImg : imgTagList) {
	    totalCount++;
	    currentHeader = test(currentImg);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentImg.getUrl() + "<br>");
	    }
	}

	grade = (int) ((double) (totalCount - problemCount) / (double) totalCount * (double) MAXIMUM_GRADE);

	setResult(resultString.toString());
	setSolution("blah blah");
	setResultGrade(grade);
    }

    private Header test(ExtResource currentResource) {
	Header[] headers = null;

	headers = currentResource.getHeaders();

	return checkEtagHeader(headers);
    }

    private Header test(ImgTag currentResource) {
	Header[] headers = null;

	headers = currentResource.getImgInfo().getResponseHeader();

	return checkEtagHeader(headers);
    }

    private void appendResultString(String result) {
	resultString.append(result);
	resultString.append("\n");
    }

    private Header checkEtagHeader(Header[] headers) {
	if (headers != null)
	    for (Header header : headers) {
		// if header is "content-encoding" header
		if (header.getName().equalsIgnoreCase("etag")) {
		    return header;
		}
	    }

	return null;
    }

    public int getProblemCount() {
	return problemCount;
    }

    public int getTotalCount() {
	return totalCount;
    }

}

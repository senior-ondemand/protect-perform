package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pnp.data.html.ExtResource;
import com.pnp.module.perform.Perform;

public class AvoidEmptySrcOrHref extends Perform {

    private static final String SCRIPT_SELECTOR = "script";
    private static ArrayList<String> emptySelectorList;
    private static ArrayList<String> emptyHtmlTagList;

    // count
    private int emptyLinkFromJsCnt;
    private int emptyLinkFromBodyCnt;

    private int grade;

    static {
	emptySelectorList = new ArrayList<>();
	emptySelectorList.add("*[href~=^$]");
	emptySelectorList.add("*[href=null]");
	emptySelectorList.add("img[src~=^$]");
	emptySelectorList.add("img[src=null]");

	emptyHtmlTagList = new ArrayList<>();
	emptyHtmlTagList.add("img.src=''");
	emptyHtmlTagList.add("img.src=\"\"");
	emptyHtmlTagList.add("img.src=null");
	emptyHtmlTagList.add("img.src='null'");
	emptyHtmlTagList.add("img.src=\"null\"");
    }

    private AvoidEmptySrcOrHref() {
	grade = 100;
    }

    public static Perform create() {
	return new AvoidEmptySrcOrHref();
    }

    @Override
    public void exe() {
	int emptyLinkfromJsCnt = countFromJs();
	int emptyLinkfromBodyCnt = countFromBody();

	if (emptyLinkfromBodyCnt > 0 || emptyLinkfromJsCnt > 0) {
	    solutionBuilder.append("Remove Emtpy src or href\n");

	    grade = 0;
	}

	summary();
    }

    private int countFromJs() {
	StringBuilder emptyListFromJsBuilder = new StringBuilder();
	emptyLinkFromJsCnt = 0;

	for (String emptyHtmlTag : emptyHtmlTagList) {
	    // External Js
	    for (ExtResource extResource : preParsedResponse.getExtJsResourceList()) {
		emptyLinkFromJsCnt += StringUtils.countMatches(extResource.getResponseBody(), emptyHtmlTag);
		emptyListFromJsBuilder.append(printEmtpySrcScript(emptyHtmlTag, extResource.getResponseBody()));
	    }

	    // Internal Js
	    emptyLinkFromJsCnt += StringUtils.countMatches(preParsedResponse.getDocument().select(SCRIPT_SELECTOR).toString(), emptyHtmlTag);
	    emptyListFromJsBuilder.append(printEmtpySrcScript(emptyHtmlTag, preParsedResponse.getDocument().select(SCRIPT_SELECTOR).toString()));
	}

	resultBuilder.append("The Number of Emtpy src or href: ");
	resultBuilder.append(emptyLinkFromJsCnt).append(BR);
	resultBuilder.append(emptyListFromJsBuilder).append(BR);

	return emptyLinkFromJsCnt;
    }

    private StringBuilder printEmtpySrcScript(String emptyHtmlTag, String str) {
	StringBuilder emptyListFromJsBuilder = new StringBuilder();
	int fromIndex = 0;
	int beginIndex = 0;

	while ((beginIndex = str.indexOf(emptyHtmlTag, fromIndex)) != StringUtils.INDEX_NOT_FOUND) {
	    int returnIndex = indexOfFirstReturn(beginIndex, str);

	    emptyListFromJsBuilder.append(str.substring(beginIndex, returnIndex)).append(BR);
	    fromIndex = beginIndex + 1;
	}

	return emptyListFromJsBuilder;
    }

    private int indexOfFirstReturn(int beginIndex, String str) {
	int linuxReturnIndex = str.indexOf("\n", beginIndex);
	int winReturnIndex = str.indexOf("\r\n", beginIndex);
	int returnIndex = 0;

	if (linuxReturnIndex > 0 && winReturnIndex > 0) {
	    returnIndex = Math.min(linuxReturnIndex, winReturnIndex);
	} else if (linuxReturnIndex == StringUtils.INDEX_NOT_FOUND && winReturnIndex == StringUtils.INDEX_NOT_FOUND) {
	    returnIndex = str.length() - 1;
	} else if (linuxReturnIndex == StringUtils.INDEX_NOT_FOUND || winReturnIndex == StringUtils.INDEX_NOT_FOUND) {
	    returnIndex = Math.max(linuxReturnIndex, winReturnIndex);
	} else {
	    throw new IllegalArgumentException("문자열에 리턴이 없음");
	}

	return returnIndex;
    }

    private int countFromBody() {
	StringBuilder emptyListFromBodyBuilder = new StringBuilder();
	emptyLinkFromBodyCnt = 0;

	for (String emptyLinkSelector : emptySelectorList) {
	    emptyLinkFromBodyCnt += preParsedResponse.getDocument().select(emptyLinkSelector).size();

	    Elements elements = preParsedResponse.getDocument().select(emptyLinkSelector);
	    for (Element element : elements) {
		emptyListFromBodyBuilder.append("<xmp>").append(element.toString()).append("</xmp>");
	    }
	}

	resultBuilder.append("The Number of Empty src or href From Body: ");
	resultBuilder.append(emptyLinkFromBodyCnt).append(BR);
	resultBuilder.append(emptyListFromBodyBuilder).append(BR);

	return emptyLinkFromBodyCnt;
    }

    private void summary() {
	resultBuilder.append(BR);
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setGrade(grade);
    }

    public int getEmptyLinkFromJsCnt() {
	return emptyLinkFromJsCnt;
    }

    public int getEmptyLinkFromBodyCnt() {
	return emptyLinkFromBodyCnt;
    }

}
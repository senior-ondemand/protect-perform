package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.http.Header;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class ReduceCookieSize extends Perform {

    private static final int checkByteSize = 100;
    private int totalCount;
    private int problemCount;
    private int grade;
    private final StringBuffer resultString = new StringBuffer();

    private ReduceCookieSize() {

    }

    public static Perform create() {
	return new ReduceCookieSize();
    }

    @Override
    public void exe() {

	Header currentHeader;

	ArrayList<ExtResource> jsResourceList = preParsedResponse.getExtJsResourceList();
	ArrayList<ExtResource> cssResourceList = preParsedResponse.getExtCssResourceList();
	ArrayList<ImgTag> imgTagList = preParsedResponse.getImgTagList();

	// js list
	for (ExtResource currentJsResource : jsResourceList) {
	    totalCount++;
	    currentHeader = test(currentJsResource);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentJsResource.getFileName() + "<br>");
		appendResultString("problem : " + currentHeader.toString() + "<br><br>");
	    }
	}

	// css list
	for (ExtResource currentCssResource : cssResourceList) {
	    totalCount++;
	    currentHeader = test(currentCssResource);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentCssResource.getFileName() + "<br>");
		appendResultString("problem : " + currentHeader.toString() + "<br><br>");
	    }
	}

	// img list
	for (ImgTag currentImg : imgTagList) {
	    totalCount++;
	    currentHeader = test(currentImg);

	    if (currentHeader != null) {
		problemCount++;
		appendResultString("resource : " + currentImg.getUrl() + "<br>");
		appendResultString("problem : " + currentHeader.toString() + "<br><br>");
	    }
	}

	grade = (int) ((double) (totalCount - problemCount) / (double) totalCount * (double) MAXIMUM_GRADE);

	setResult(resultString.toString());
	setSolution("blah blah");
	setResultGrade(grade);
    }

    private Header test(ExtResource currentResource) {
	Header[] headers = null;

	headers = currentResource.getHeaders();

	return checkCookieSize(headers);
    }

    private Header test(ImgTag currentResource) {
	Header[] headers = null;

	headers = currentResource.getImgInfo().getResponseHeader();

	return checkCookieSize(headers);
    }

    private void appendResultString(String result) {
	resultString.append(result);
	resultString.append("\n");
    }

    private Header checkCookieSize(Header[] headers) {
	if (headers != null)
	    for (Header header : headers) {

		// set-cookie
		if (header.getName().equalsIgnoreCase("set-cookie")) {

		    String value = header.getValue();
		    if (value.length() > checkByteSize) {
			return header;
		    }
		}
	    }

	return null;
    }

    public int getProblemCount() {
	return problemCount;
    }

    public int getTotalCount() {
	return totalCount;
    }

}

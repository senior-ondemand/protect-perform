package com.pnp.module.perform.yslow.y11;

public class YuiOption {

    public String charset = "UTF-8";
    public int lineBreakPos = -1;
    public boolean munge = true;
    public boolean verbose = false;
    public boolean preserveAllSemiColons = false;
    public boolean disableOptimizations = false;
}

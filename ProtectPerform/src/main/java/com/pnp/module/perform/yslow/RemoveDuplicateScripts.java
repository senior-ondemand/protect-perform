package com.pnp.module.perform.yslow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.pnp.data.html.ExtResource;
import com.pnp.module.perform.Perform;

public class RemoveDuplicateScripts extends Perform {

    private enum ExtType {
	JS, CSS
    }

    private static final int DEFUALT_GRADE = 50;
    private static final int DUPLICATE_GRADE = 0;

    private final HashMap<String, ArrayList<ExtResource>> extJsMap;
    private final HashMap<String, ArrayList<ExtResource>> extCssMap;
    private final HashMap<ExtType, Integer> gradeMap;

    private RemoveDuplicateScripts() {
	extJsMap = new HashMap<>();
	extCssMap = new HashMap<>();
	gradeMap = new HashMap<>();

	gradeMap.put(ExtType.JS, DEFUALT_GRADE);
	gradeMap.put(ExtType.CSS, DEFUALT_GRADE);
    }

    public static Perform create() {
	return new RemoveDuplicateScripts();
    }

    @Override
    public void exe() {
	countDuplicateList(ExtType.JS, preParsedResponse.getExtJsResourceList(), extJsMap);
	countDuplicateList(ExtType.CSS, preParsedResponse.getExtCssResourceList(), extCssMap);
	summary();
    }

    private void countDuplicateList(ExtType extType, ArrayList<ExtResource> extResourceList, HashMap<String, ArrayList<ExtResource>> extResourceMap) {
	HashSet<String> uniqueUrlSet = countUniqueUrl(extResourceList);

	for (String uniqueUrl : uniqueUrlSet) {
	    ArrayList<ExtResource> duplicateExtResourceList = new ArrayList<>();

	    for (ExtResource extResource : extResourceList) {
		if (uniqueUrl.equals(extResource.getAbsUrl())) {
		    duplicateExtResourceList.add(extResource);
		}
	    }

	    gradeMap.put(extType, (duplicateExtResourceList.size() > 1) ? DUPLICATE_GRADE : DEFUALT_GRADE);
	    extResourceMap.put(uniqueUrl, duplicateExtResourceList);
	}

	printDuplicateList(extType, extResourceMap);
    }

    private HashSet<String> countUniqueUrl(ArrayList<ExtResource> extResourceList) {
	HashSet<String> uniqueUrlSet = new HashSet<>();

	for (ExtResource extResource : extResourceList) {
	    if (uniqueUrlSet.contains(extResource.getAbsUrl()) == false) {
		uniqueUrlSet.add(extResource.getAbsUrl());
	    }
	}

	return uniqueUrlSet;
    }

    private void printDuplicateList(ExtType extType, HashMap<String, ArrayList<ExtResource>> duplicateList) {
	boolean found = false;

	for (String url : duplicateList.keySet()) {
	    if (duplicateList.get(url).size() > 1) {
		resultBuilder.append("\"").append(url).append("\"");
		resultBuilder.append(" Was Included ");
		resultBuilder.append(duplicateList.get(url).size());
		resultBuilder.append(" Times").append(BR);

		printIncludeLine(duplicateList.get(url));

		solutionBuilder.append("Include \"").append(url).append("\"");
		solutionBuilder.append(" Only Once").append(BR);

		found = true;
	    }
	}

	if (!found) {
	    resultBuilder.append("No ").append(extType.toString()).append(" Duplicate.").append(BR);
	}
    }

    private void printIncludeLine(ArrayList<ExtResource> extResources) {
	for (ExtResource extResource : extResources) {
	    resultBuilder.append(extResource.getFileName()).append(BR);
	}

	resultBuilder.append(BR);
    }

    private void summary() {
	resultBuilder.append(BR);
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setGrade(gradeMap.get(ExtType.JS) + gradeMap.get(ExtType.CSS));
    }

    public HashMap<String, ArrayList<ExtResource>> getExtJsMap() {
	return extJsMap;
    }

    public HashMap<String, ArrayList<ExtResource>> getExtCssMap() {
	return extCssMap;
    }

}
package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class UseContentDeliveryNetwork extends Perform {

    private static final ArrayList<String> CDNNameList;

    static {
	CDNNameList = new ArrayList<>();
	CDNNameList.add("cdn");
	CDNNameList.add(".akamai.net"); // Akamai
	CDNNameList.add(".akamaiedge.net"); // Akamai
	CDNNameList.add(".llnwd.net"); // Limelight
	CDNNameList.add(".instacontent.net"); // Mirror Image
	CDNNameList.add(".footprint.net"); // ???
	CDNNameList.add(".ay1.b.yahoo.com"); // Yahoo distribution
	CDNNameList.add(".google.com"); // Google distribution
	CDNNameList.add(".cloudfront.net"); // Amazon CloudFront
	CDNNameList.add(".cachefly.net"); // Cachefly (also anycast - support to
					  // be added)
	CDNNameList.add(".nyucd.net"); // Coral Cache
    }
    
    // count
    private int failCount;

    public static Perform create() {
	return new UseContentDeliveryNetwork();
    }

    @Override
    public void exe() {
	int resultGrade;
	int allCount = 0;
	int successCount = 0;

	ArrayList<ImgTag> imgTagList = preParsedResponse.getImgTagList();
	ArrayList<String> exDomainList = new ArrayList<String>();

	// Remove duplicate domain
	for (ImgTag imgTag : imgTagList) {
	    if (!exDomainList.contains(imgTag.getUrlDomain()))
		exDomainList.add(imgTag.getUrlDomain());
	}

	for (ExtResource extResource : preParsedResponse.getExtCssResourceList()) {
	    if (!exDomainList.contains(extResource.getDomain()))
		exDomainList.add(extResource.getDomain());
	}

	for (ExtResource extResource : preParsedResponse.getExtJsResourceList()) {
	    if (!exDomainList.contains(extResource.getDomain()))
		exDomainList.add(extResource.getDomain());
	}
	
	System.out.println(exDomainList);

	for (String domain : exDomainList) {

	    // Get cname of src domain
	    ArrayList<String> domainCnameList = new ArrayList<String>();
	    domainCnameList.add(domain);
	    String result = domain + " is not using the CDN.<br><br>";
	    dnsLookupCname(domain, domainCnameList);
	    for (String imageDomainCname : domainCnameList) {
		for (String CDNName : CDNNameList) {
		    if (imageDomainCname.contains(CDNName)) {
			result = domain + " is using the following CDN.<br>\t- " + imageDomainCname + "<br><br>";
			successCount++;
		    }
		}
	    }
	    allCount++;
	    resultBuilder.append(result);
	}

	solutionBuilder.append("Static data, such as Images, CSS, and Javascript use the CDN");
	resultGrade = (int) (((double) successCount / (double) allCount) * 100.0);
	
	failCount = allCount - successCount;

	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }

    public void dnsLookupCname(String url, ArrayList<String> cnames) {
	Record[] records = null;
	try {
	    records = new Lookup(url, Type.CNAME).run();
	} catch (TextParseException e) {
	    e.printStackTrace();
	}
	// Escape conditions in recursive function
	if (records == null)
	    return;

	for (int i = 0; i < records.length; i++) {

	    CNAMERecord cNameRecord = (CNAMERecord) records[i];
	    String cname = cNameRecord.getTarget().toString();

	    cnames.add(cname);
	    dnsLookupCname(cname, cnames);
	}
    }
    
    public int getFailCount() {
	return failCount;
    }
}

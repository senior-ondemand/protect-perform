package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.http.Header;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class GzipComponents extends Perform {

    private int totalCount;
    private int problemCount;
    private int grade;
    private final StringBuffer resultString = new StringBuffer();

    private GzipComponents() {

    }

    public static Perform create() {
	return new GzipComponents();
    }

    @Override
    public void exe() {

	Header currentHeader;

	// check targetURL http response
	ArrayList<ExtResource> jsResourceList = preParsedResponse.getExtJsResourceList();
	ArrayList<ExtResource> cssResourceList = preParsedResponse.getExtCssResourceList();
	ArrayList<ImgTag> imgTagList = preParsedResponse.getImgTagList();

	// js list
	for (ExtResource currentJsResource : jsResourceList) {
	    totalCount++;

	    if (!test(currentJsResource)) {
		problemCount++;
		appendResultString("resource : " + currentJsResource.getFileName() + "<br>");
	    }
	}

	// css list
	for (ExtResource currentCssResource : cssResourceList) {
	    totalCount++;

	    if (!test(currentCssResource)) {
		problemCount++;
		appendResultString("resource : " + currentCssResource.getFileName() + "<br>");
	    }
	}

	// img list
	for (ImgTag currentImg : imgTagList) {
	    totalCount++;

	    if (!test(currentImg)) {
		problemCount++;
		appendResultString("resource : " + currentImg.getUrl() + "<br>");
	    }
	}
	grade = (totalCount - problemCount) / totalCount * 100;

	setResult(resultString.toString());
	setSolution("blah blah");
	setResultGrade(grade);
    }

    private boolean test(ExtResource currentResource) {
	Header[] headers = null;

	headers = currentResource.getHeaders();

	return checkContentEncodingHeader(headers);
    }

    private boolean test(ImgTag currentResource) {
	Header[] headers = null;

	headers = currentResource.getImgInfo().getResponseHeader();

	return checkContentEncodingHeader(headers);
    }

    private void appendResultString(String result) {
	resultString.append(result);
	resultString.append("\n");
    }

    private boolean checkContentEncodingHeader(Header[] headers) {

	if (headers != null) {
	    for (Header header : headers) {
		// if header is "content-encoding" header
		if (header.getName().equalsIgnoreCase("content-encoding")) {

		    String value = header.getValue();

		    if (value.equals("gzip") || value.equals("deflate") || value.equals("sdch")) {
			return true;
		    } else
			return false;

		}
	    }
	}
	return false;

    }

    public int getProblemCount() {
	return problemCount;
    }

    public int getTotalCount() {
	return totalCount;
    }

}

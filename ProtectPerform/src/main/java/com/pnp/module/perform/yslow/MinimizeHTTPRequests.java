package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.pnp.data.html.ExtResource;
import com.pnp.module.perform.Perform;

public class MinimizeHTTPRequests extends Perform {

    // CSS Sprite
    private static final String STYLE_SELECTOR = "style";
    private static final ArrayList<String> cssSpriteSelectorList;
    private static final ArrayList<String> cssSpriteStyleList;

    // Image Maps
    private static final String IMAGE_MAP_SELECTOR = "area[coords~=$]";

    // InLine Image
    private static final String INLINE_IMAGE_STYLE = "data:image/";
    private static final String INLINE_IMAGE_SELECTOR = "*[style~=data:image/]";

    // Grade
    private static final int JS_INCLUDE_GRADE = 20;
    private static final int CSS_INCLUDE_GRADE = 20;
    private static final int CSS_SPRITE_GRADE = 20;
    private static final int IMAGE_MAP_GRADE = 20;
    private static final int INLINE_IMAGE_GRADE = 20;

    // count
    private int extJsLinkCnt;
    private int extCssLinkCnt;
    private int cssSpriteFromExtCnt;
    private int cssSpriteFromBodyCnt;
    private int imageMapCnt;
    private int inLineImageFromExtCnt;
    private int inLineImageFromBodyCnt;

    private int grade;

    static {
	cssSpriteStyleList = new ArrayList<>();
	cssSpriteStyleList.add("background-image:");
	cssSpriteStyleList.add("background-position:");

	cssSpriteSelectorList = new ArrayList<>();
	cssSpriteSelectorList.add("*[style~=background-image:]");
	cssSpriteSelectorList.add("*[style~=background-position:]");
    }

    private MinimizeHTTPRequests() {
	grade = 100;
    }

    public static Perform create() {
	return new MinimizeHTTPRequests();
    }

    @Override
    public void exe() {
	countExtJsLink();
	countExtCssLink();
	countCssSprite();
	countImageMap();
	countInLineImage();
	summary();
    }

    private void countExtJsLink() {
	extJsLinkCnt = 0;

	resultBuilder.append("The Number of External JS File: ");
	extJsLinkCnt = preParsedResponse.getExtJsResourceList().size();
	resultBuilder.append(extJsLinkCnt);
	resultBuilder.append(BR);

	if (extJsLinkCnt > 1) {
	    solutionBuilder.append("Combine All External JS File into One").append(BR);

	    grade -= JS_INCLUDE_GRADE;
	}
    }

    private void countExtCssLink() {
	extCssLinkCnt = 0;

	resultBuilder.append("The Number of External CSS File: ");
	extCssLinkCnt = preParsedResponse.getExtCssResourceList().size();
	resultBuilder.append(extCssLinkCnt);
	resultBuilder.append(BR).append(BR);

	if (extCssLinkCnt > 1) {
	    solutionBuilder.append("Combine All External CSS File into One").append(BR);

	    grade -= CSS_INCLUDE_GRADE;
	}
    }

    private void countCssSprite() {
	countCssSpriteFromExt();
	countCssSpriteFromBody();

	if (cssSpriteFromExtCnt == 0 && cssSpriteFromBodyCnt == 0) {
	    solutionBuilder.append("Consider Using CSS Prite").append(BR);

	    grade -= CSS_SPRITE_GRADE;
	}
    }

    private void countCssSpriteFromExt() {
	cssSpriteFromExtCnt = 0;

	for (ExtResource extResource : preParsedResponse.getExtCssResourceList()) {
	    for (String cssSpriteStyle : cssSpriteStyleList) {
		cssSpriteFromExtCnt += StringUtils.countMatches(extResource.getResponseBody(), cssSpriteStyle);
	    }

	    resultBuilder.append("The Number of CSS Prite From \"");
	    resultBuilder.append(extResource.getFileName());
	    resultBuilder.append("\": ");
	    resultBuilder.append(cssSpriteFromExtCnt);
	    resultBuilder.append(BR);
	}
    }

    private void countCssSpriteFromBody() {
	cssSpriteFromBodyCnt = 0;

	for (String cssSpriteStyle : cssSpriteStyleList) {
	    // <style>
	    cssSpriteFromBodyCnt += StringUtils.countMatches(preParsedResponse.getDocument().select(STYLE_SELECTOR).toString(), cssSpriteStyle);
	}
	for (String cssSpriteSelector : cssSpriteSelectorList) {
	    // <... style=... />
	    cssSpriteFromBodyCnt += preParsedResponse.getDocument().select(cssSpriteSelector).size();
	}

	resultBuilder.append("The Number of CSS Prite From Body: ");
	resultBuilder.append(cssSpriteFromBodyCnt);
	resultBuilder.append(BR).append(BR);
    }

    private void countImageMap() {
	imageMapCnt = 0;
	imageMapCnt = preParsedResponse.getDocument().select(IMAGE_MAP_SELECTOR).size();

	resultBuilder.append(BR).append("The Number of Image Map From Body: ");
	resultBuilder.append(imageMapCnt);
	resultBuilder.append(BR).append(BR);

	if (imageMapCnt == 0) {
	    solutionBuilder.append("Consider Using Image Map").append(BR);

	    grade -= IMAGE_MAP_GRADE;
	}
    }

    private void countInLineImage() {
	countInLineImageFromExt();
	countInLineImageFromBody();

	if (inLineImageFromExtCnt == 0 && inLineImageFromBodyCnt == 0) {
	    solutionBuilder.append("Consider Using Inline Image").append(BR);

	    grade -= INLINE_IMAGE_GRADE;
	}
    }

    private void countInLineImageFromExt() {
	inLineImageFromExtCnt = 0;

	for (ExtResource extResource : preParsedResponse.getExtCssResourceList()) {
	    inLineImageFromExtCnt += StringUtils.countMatches(extResource.getResponseBody(), INLINE_IMAGE_STYLE);

	    resultBuilder.append("The Number of InLine Image From \"");
	    resultBuilder.append(extResource.getFileName());
	    resultBuilder.append("\": ");
	    resultBuilder.append(inLineImageFromExtCnt);
	    resultBuilder.append(BR);
	}
    }

    private void countInLineImageFromBody() {
	inLineImageFromBodyCnt = 0;

	// <style>
	inLineImageFromBodyCnt += StringUtils.countMatches(preParsedResponse.getDocument().select(STYLE_SELECTOR).toString(), INLINE_IMAGE_STYLE);
	// <.. style=... />
	inLineImageFromBodyCnt += preParsedResponse.getDocument().select(INLINE_IMAGE_SELECTOR).size();

	resultBuilder.append(BR).append("The Number of InLine From Body: ");
	resultBuilder.append(inLineImageFromBodyCnt);
	resultBuilder.append(BR);
    }

    private void summary() {
	resultBuilder.append(BR);
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setGrade(grade);
    }

    public int getExtJsLinkCnt() {
	return extJsLinkCnt;
    }

    public int getExtCssLinkCnt() {
	return extCssLinkCnt;
    }

    public int getCssSpriteFromExtCnt() {
	return cssSpriteFromExtCnt;
    }

    public int getCssSpriteFromBodyCnt() {
	return cssSpriteFromBodyCnt;
    }

    public int getImageMapCnt() {
	return imageMapCnt;
    }

    public int getInLineImageFromExtCnt() {
	return inLineImageFromExtCnt;
    }

    public int getInLineImageFromBodyCnt() {
	return inLineImageFromBodyCnt;
    }

}
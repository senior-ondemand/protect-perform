package com.pnp.module.perform;

import com.pnp.module.perform.yslow.AddExpiresOrCacheControlHeader;
import com.pnp.module.perform.yslow.AvoidCSSExpressions;
import com.pnp.module.perform.yslow.AvoidEmptySrcOrHref;
import com.pnp.module.perform.yslow.AvoidFilters;
import com.pnp.module.perform.yslow.AvoidRedirects;
import com.pnp.module.perform.yslow.ConfigureETags;
import com.pnp.module.perform.yslow.DoNotScaleImagesInHTML;
import com.pnp.module.perform.yslow.GzipComponents;
import com.pnp.module.perform.yslow.MakeAJAXCacheable;
import com.pnp.module.perform.yslow.MakeFaviconIcoSmallAndCacheable;
import com.pnp.module.perform.yslow.MakeJavaScriptAndCSSExternal;
import com.pnp.module.perform.yslow.MinifyJavaScriptAndCSS;
import com.pnp.module.perform.yslow.MinimizeHTTPRequests;
import com.pnp.module.perform.yslow.No404s;
import com.pnp.module.perform.yslow.PutScriptsAtBottom;
import com.pnp.module.perform.yslow.PutStyleSheetsAtTop;
import com.pnp.module.perform.yslow.ReduceCookieSize;
import com.pnp.module.perform.yslow.ReduceDNSLookups;
import com.pnp.module.perform.yslow.ReduceNumberOfDOMElements;
import com.pnp.module.perform.yslow.RemoveDuplicateScripts;
import com.pnp.module.perform.yslow.UseContentDeliveryNetwork;
import com.pnp.module.perform.yslow.UseCookieFreeDomainsForComponents;
import com.pnp.module.perform.yslow.UseGETforAJAXRequests;

public class PerformFactory {

    public static Perform create(PerformRuleType performType) {
	switch (performType) {
	case Y01:
	    return MinimizeHTTPRequests.create();
	case Y02:
	    return UseContentDeliveryNetwork.create();
	case Y03:
	    return AvoidEmptySrcOrHref.create();
	case Y04:
	    return AddExpiresOrCacheControlHeader.create();
	case Y05:
	    return GzipComponents.create();
	case Y06:
	    return PutStyleSheetsAtTop.create();
	case Y07:
	    return PutScriptsAtBottom.create();
	case Y08:
	    return AvoidCSSExpressions.create();
	case Y09:
	    return MakeJavaScriptAndCSSExternal.create();
	case Y10:
	    return ReduceDNSLookups.create();
	case Y11:
	    return MinifyJavaScriptAndCSS.create();
	case Y12:
	    return AvoidRedirects.create();
	case Y13:
	    return RemoveDuplicateScripts.create();
	case Y14:
	    return ConfigureETags.create();
	case Y15:
	    return MakeAJAXCacheable.create();
	case Y16:
	    return UseGETforAJAXRequests.create();
	case Y17:
	    return ReduceNumberOfDOMElements.create();
	case Y18:
	    return No404s.create();
	case Y19:
	    return ReduceCookieSize.create();
	case Y20:
	    return UseCookieFreeDomainsForComponents.create();
	case Y21:
	    return AvoidFilters.create();
	case Y22:
	    return DoNotScaleImagesInHTML.create();
	case Y23:
	    return MakeFaviconIcoSmallAndCacheable.create();

	default:
	    throw new IllegalArgumentException("잘못된 카테고리");
	}
    }

}
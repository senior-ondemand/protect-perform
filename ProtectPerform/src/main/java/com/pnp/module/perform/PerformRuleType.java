package com.pnp.module.perform;

import com.pnp.module.RuleType;

public enum PerformRuleType implements RuleType {
    Y01, Y02, Y03, Y04, Y05, Y06, Y07, Y08, Y09, Y10, Y11, Y12, Y13, Y14, Y15, Y16, Y17, Y18, Y19, Y20, Y21, Y22, Y23
}
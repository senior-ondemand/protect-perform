package com.pnp.module.perform.yslow;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.pnp.module.perform.Perform;

public class AvoidRedirects extends Perform {

    private int responseStatusCode;
    private boolean isRedirected;

    private AvoidRedirects() {

    }

    public static Perform create() {
	return new AvoidRedirects();
    }

    @Override
    public void exe() {

	HttpClient httpclient = HttpClientBuilder.create().disableRedirectHandling().build();
	
	HttpGet httpGet = new HttpGet(targetURL);
	HttpResponse response = null;
	
	try {
	    response = httpclient.execute(httpGet);
	} catch (ClientProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
	preParsedResponse.setTargetUrlResponse(response);
	
	try {
	    response = httpclient.execute(httpGet);
	} catch (ClientProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
	preParsedResponse.setTargetUrlResponse(response);
	
	if (preParsedResponse != null) {
	    if (preParsedResponse.getTargetUrlResponse() != null) {
		responseStatusCode = preParsedResponse.getTargetUrlResponse().getStatusLine().getStatusCode();
		isRedirected = checkRedirectStatusCode(responseStatusCode);
	    }
	}

	if (isRedirected) {
	    setResult(targetURL + " : redirected. ");
	    setSolution("don't use redirection");
	} else
	    setResult("no problem.");
    }

    private boolean checkRedirectStatusCode(int responseStatusCode) {

	if (responseStatusCode == 300 || responseStatusCode == 301 || responseStatusCode == 302 || responseStatusCode == 303
		|| responseStatusCode == 304 || responseStatusCode == 305 || responseStatusCode == 306 || responseStatusCode == 307
		|| responseStatusCode == 308) {

	    setResultGrade(0);

	    return true;
	} else {
	    setResultGrade(100);

	    return false;
	}
    }
    
    public int getIsRedirected() {
	if (isRedirected)
	    return 1;
	return 0;
    }

}

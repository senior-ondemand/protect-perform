package com.pnp.module.perform.yslow;

import com.pnp.module.perform.Perform;

public class ReduceNumberOfDOMElements extends Perform {

    private static final int PERCENTAGE = 100;
    private static final String ALL_SELCTOR = "*";
    private static final int DESIRE_DOM_ELEMENT_CNT = 800;
    private int domElementCnt;
    private int grade;

    private ReduceNumberOfDOMElements() {

    }

    public static Perform create() {
	return new ReduceNumberOfDOMElements();
    }

    @Override
    public void exe() {
	countDomElementCnt();
	summary();
    }

    private void countDomElementCnt() {
	domElementCnt = preParsedResponse.getDocument().select(ALL_SELCTOR).size();
	grade = Math.round(DESIRE_DOM_ELEMENT_CNT / domElementCnt) * PERCENTAGE;
	grade = (grade > 100) ? 100 : grade;

	if (domElementCnt > DESIRE_DOM_ELEMENT_CNT) {
	    resultBuilder.append("There are more than ").append(DESIRE_DOM_ELEMENT_CNT).append(" Dom elements.").append("\n");
	    solutionBuilder.append("Reduce Dom elements").append("\n");
	}
    }

    private void summary() {
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setGrade(grade);
    }

    public int getDomElementCnt() {
	return domElementCnt;
    }

}

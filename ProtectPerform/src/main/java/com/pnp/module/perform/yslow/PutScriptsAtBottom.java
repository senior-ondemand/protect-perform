package com.pnp.module.perform.yslow;

import com.pnp.module.perform.Perform;

public class PutScriptsAtBottom extends Perform {

    private static final int MAXIMUM_THRESHOLD = 10;
    private static final int MAXIMUM_GRADE = 100;

    // count
    private int extJavascriptHeadCount;
    
    public static Perform create() {
	return new PutScriptsAtBottom();
    }

    @Override
    public void exe() {
	int resultGrade;

	extJavascriptHeadCount = preParsedResponse.getExtJsHeadCount();

	resultBuilder
		.append("JavaScript scripts block parallel downloads; that is, when a script is downloading, the browser will not start any other downloads. To help the page load faster, move scripts to the bottom of the page if they are deferrable. <br><br>");
	resultBuilder.append("number of external JavaScript in head tag  : ").append(extJavascriptHeadCount).append("<br>");

	solutionBuilder.append("Write in Script file at the bottom of the page.");

	resultGrade = MAXIMUM_GRADE - (int) (((float) extJavascriptHeadCount / (float) MAXIMUM_THRESHOLD) * 100.0);

	if ( resultGrade < 0)
	    resultGrade = 0;
	
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }
    
    public int getExtJavascriptHeadCount() {
	return extJavascriptHeadCount;
    }
}

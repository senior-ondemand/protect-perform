package com.pnp.module.perform.yslow.y11;

import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

public class YuiCompressorErrorReporter implements ErrorReporter {

    @Override
    public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
	StringBuilder builder = new StringBuilder();
	builder.append("WARNING").append("\n");
	if (line < 0) {
	    builder.append(message);
	} else {
	    builder.append(line).append(":").append(lineOffset).append(":").append(message);
	}

	throw new IllegalArgumentException(builder.toString());
    }

    @Override
    public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
	StringBuilder builder = new StringBuilder();
	builder.append("[ERROR]").append("\n");

	if (line < 0) {
	    builder.append(message);
	} else {
	    builder.append(line).append(":").append(lineOffset).append(":").append(message);
	}
	throw new IllegalArgumentException(builder.toString());
    }

    @Override
    public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
	error(message, sourceName, line, lineSource, lineOffset);
	return new EvaluatorException(message);
    }

}

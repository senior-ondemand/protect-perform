package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class No404s extends Perform {

    private int totalCount;
    private int problemCount;
    private int grade;

    private No404s() {

    }

    public static Perform create() {
	return new No404s();
    }

    @Override
    public void exe() {

	boolean currentStatusCode;

	// check targetURL http response
	ArrayList<ExtResource> jsResourceList = preParsedResponse.getExtJsResourceList();
	ArrayList<ExtResource> cssResourceList = preParsedResponse.getExtCssResourceList();
	ArrayList<String> status404UrlList = preParsedResponse.getStatus404UrlList();
	ArrayList<ImgTag> imgTagList = preParsedResponse.getImgTagList();

	
	totalCount = jsResourceList.size() + cssResourceList.size() + status404UrlList.size();
	problemCount = status404UrlList.size();
	// js list
	for (String resourceUrl : status404UrlList) {
	    appendResultString("resource : " + resourceUrl);
	    appendResultString("problem : 404");
	}

	// img list
	for (ImgTag currentImg : imgTagList) {
	    totalCount++;
	    currentStatusCode = test(currentImg);
	    if (currentStatusCode) {
		problemCount++;
		appendResultString("resource : " + currentImg.getLinkLine());
		appendResultString("problem : 404");
	    }
	}

	grade = (int)((double)(totalCount - problemCount) / (double)totalCount * (double)MAXIMUM_GRADE);

	setResult(resultBuilder.toString());
	setSolution("blah blah");
	setResultGrade(grade);
    }

    private boolean test(ImgTag currentResource) {
	int statuscode;

	statuscode = currentResource.getImgInfo().getStatusCode();

	return check404StatusCode(statuscode);
    }

    private void appendResultString(String result) {
	resultBuilder.append(result);
	resultBuilder.append("\n");
    }

    private boolean check404StatusCode(int responseStatusCode) {
	if (responseStatusCode == 404) {
	    return true;
	} else
	    return false;
    }
    
    public int getProblemCount() {
	return problemCount;
    }
    
    public int getTotalCount() {
	return totalCount;
    }

}

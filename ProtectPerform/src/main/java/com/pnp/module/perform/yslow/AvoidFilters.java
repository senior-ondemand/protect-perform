package com.pnp.module.perform.yslow;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.pnp.data.html.ExtResource;
import com.pnp.module.perform.Perform;

public class AvoidFilters extends Perform {

    private static final int MAXIMUM_THRESHOLD = 10; // (%)

    // count
    private int extJsFindFilter;
    private int extCssFindFilter;
    private int mainBodyFindFilter;
    private int failCount;

    private static final ArrayList<String> filterNameList;

    static {
	filterNameList = new ArrayList<>();
	filterNameList.add("AlphaImageLoader");
    }

    public static Perform create() {
	return new AvoidFilters();
    }

    @Override
    public void exe() {
	int resultGrade;
	int count = 0;

	extCssFindFilter = 0;
	for (ExtResource extResource : preParsedResponse.getExtCssResourceList()) {
	    String responseBody = extResource.getResponseBody();
	    for (String filterName : filterNameList) {
		if ((count = StringUtils.countMatches(responseBody, filterName)) > 0) {
		    resultBuilder.append(extResource.getUrl()).append(" - #").append(count).append("<br>");
		    extCssFindFilter += count;
		}
	    }
	}

	extJsFindFilter = 0;
	for (ExtResource extResource : preParsedResponse.getExtJsResourceList()) {
	    String responseBody = extResource.getResponseBody();
	    for (String filterName : filterNameList) {
		if ((count = StringUtils.countMatches(responseBody, filterName)) > 0) {
		    resultBuilder.append(extResource.getUrl()).append(" - #").append(count).append("<br>");
		    extJsFindFilter += count;
		}
	    }
	}

	mainBodyFindFilter = 0;
	String responseBody = preParsedResponse.getResponseBody();
	for (String filterName : filterNameList) {
	    if ((count = StringUtils.countMatches(responseBody, filterName)) > 0) {
		resultBuilder.append(preParsedResponse.getTargetURL()).append(" - #").append(count).append("<br>");
		mainBodyFindFilter += count;
	    }
	}

	failCount = extCssFindFilter + extJsFindFilter + mainBodyFindFilter;

	if (failCount > 0) {
	    resultBuilder.append("<br>There is a total of ").append(failCount).append(" filters<br>");
	    solutionBuilder.append("Delete the CSS filter such as:<br>");
	    for (String filterName : filterNameList)
		solutionBuilder.append("\t").append(filterName).append(" ");
	    solutionBuilder.append("<br>");
	} else {
	    resultBuilder.append("'AlphaImageLoader' was not used.");
	}

	resultGrade = MAXIMUM_GRADE - (int) (((double) failCount / (double) MAXIMUM_THRESHOLD) * (double) MAXIMUM_GRADE);

	if (resultGrade < 0)
	    resultGrade = 0;

	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(resultGrade);
    }

    public int getExtJsFindFilter() {
	return extJsFindFilter;
    }

    public int getExtCssFindFilter() {
	return extCssFindFilter;
    }

    public int getMainBodyFindFilter() {
	return mainBodyFindFilter;
    }

    public int getTotalFailCount() {
	return failCount;
    }
}

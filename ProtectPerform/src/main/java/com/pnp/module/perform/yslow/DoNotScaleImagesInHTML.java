package com.pnp.module.perform.yslow;

import com.pnp.data.html.ImgInfo;
import com.pnp.data.html.ImgTag;
import com.pnp.module.perform.Perform;

public class DoNotScaleImagesInHTML extends Perform {

    // count
    private int failCount;
    private int totalCount;
    private int grade;
    
    public static Perform create() {
	return new DoNotScaleImagesInHTML();
    }

    @Override
    public void exe() {

	failCount = 0;
	for (ImgTag imgTag : preParsedResponse.getImgTagList()) {
	    ImgInfo imgInfo = imgTag.getImgInfo();
	    int tagWidth = imgTag.getWidth();
	    int tagHeight = imgTag.getHeight();
	    int imgWidth = imgInfo.getWidth();
	    int imgHeight = imgInfo.getHeight();

	    if (((tagWidth != 0) && (imgWidth > tagWidth)) || ((tagHeight != 0) && (imgHeight > tagHeight))) {
		resultBuilder.append("img URL : ").append(imgTag.getUrl()).append("<br>");
		resultBuilder.append("\t img tag    | width: ").append(tagWidth).append(", height: ").append(tagHeight).append("<br>");
		resultBuilder.append("\t real img   | width: ").append(imgWidth).append(", height: ").append(imgHeight).append("<br>");
		resultBuilder.append("\t scale rate | width: ").append(((float) tagWidth / (float) imgWidth * 100.0)).append("%, height: ")
			.append(((float) tagHeight / (float) imgHeight * 100.0)).append("%<br><br>");
		failCount++;
	    }
	}
	totalCount = preParsedResponse.getImgTagList().size();
	
	grade = (int)((double)(totalCount - failCount) / (double)totalCount * (double)MAXIMUM_GRADE);

	resultBuilder.append("There are ").append(failCount).append(" images that are scaled down<br><br>");

	if (failCount > 0)
	    solutionBuilder.append("Taking the match the height and width of the img tag and the original image.");

	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	setResultGrade(grade);
    }
    
    public int getTotalFailCount() {
	return failCount;
    }
    
    public int getTotalCount() {
	return totalCount;
    }
}

package com.pnp.module.perform.yslow;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.mozilla.javascript.EvaluatorException;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.IntResource;
import com.pnp.data.html.Resource;
import com.pnp.module.perform.Perform;
import com.pnp.module.perform.yslow.y11.YuiCompressorErrorReporter;
import com.pnp.module.perform.yslow.y11.YuiOption;
import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

public class MinifyJavaScriptAndCSS extends Perform {

    private enum FileType {
	ExtJs, ExtCss, IntJs, IntCss
    }

    private static Logger logger = LoggerFactory.getLogger(MinifyJavaScriptAndCSS.class);

    private static final int PERCENTAGE = 100;
    private static final int THRESHOLD = 3;
    private static YuiOption yuiOption;

    private int grade;

    static {
	yuiOption = new YuiOption();
    }

    private int totalCompressoinRatioSum;
    private int totalFileCnt;

    private MinifyJavaScriptAndCSS() {
	totalCompressoinRatioSum = 0;
	totalFileCnt = 0;
	grade = 100;
    }

    public static Perform create() {
	return new MinifyJavaScriptAndCSS();
    }

    @Override
    public void exe() {
	//minifyExtJsAndCss();
	//summary();
    }

    private <T extends Resource> void minifyExtJsAndCss() {
	HashMap<FileType, ArrayList<? extends Resource>> extJsAndCssMap = new HashMap<>();
	extJsAndCssMap.put(FileType.ExtJs, preParsedResponse.getExtJsResourceList());
	extJsAndCssMap.put(FileType.ExtCss, preParsedResponse.getExtCssResourceList());
	extJsAndCssMap.put(FileType.IntJs, preParsedResponse.getIntJsResourceList());
	extJsAndCssMap.put(FileType.IntCss, preParsedResponse.getIntCssResourceList());

	String compressedCode = StringUtils.EMPTY;

	for (Entry<FileType, ArrayList<? extends Resource>> entry : extJsAndCssMap.entrySet()) {
	    FileType fileType = entry.getKey();
	    ArrayList<? extends Resource> extResourceList = entry.getValue();

	    for (Resource extResource : extResourceList) {
		try {
		    compressedCode = StringUtils.EMPTY;
		    compressedCode = YuiCompressor(entry.getKey(), extResource.getResponseBody());

		    generateCompressingInfo(fileType, extResource, compressedCode);
		} catch (IllegalArgumentException e) {
		    generateCompressionError(fileType, extResource, e.getMessage());
		}
	    }
	}
    }

    private String YuiCompressor(FileType fileType, String str) {
	InputStream inputStream = new ByteArrayInputStream(str.getBytes());
	Reader reader = new InputStreamReader(inputStream);
	Writer writer = new StringWriter();

	CssCompressor cssCompressor = null;
	JavaScriptCompressor compressor = null;

	try {
	    switch (fileType) {
	    case ExtJs:
	    case IntJs:
		compressor = new JavaScriptCompressor(reader, new YuiCompressorErrorReporter());
		compressor.compress(writer, yuiOption.lineBreakPos, yuiOption.munge, yuiOption.verbose, yuiOption.preserveAllSemiColons,
			yuiOption.disableOptimizations);
		break;
	    case ExtCss:
	    case IntCss:
		cssCompressor = new CssCompressor(reader);
		cssCompressor.compress(writer, yuiOption.lineBreakPos);
		break;

	    default:
		throw new IllegalArgumentException("잘못된 파일 타입");
	    }
	} catch (UnsupportedEncodingException e) {
	} catch (EvaluatorException | IOException e) {
	} finally {
	    IOUtils.closeQuietly(reader);
	    IOUtils.closeQuietly(writer);
	}

	return writer.toString();
    }

    private void generateCompressingInfo(FileType resourceType, Resource resource, String compressedCode) {
	float beforeLength = resource.getResponseBody().length();
	float afterLength = compressedCode.length();
	int compressionRatio = PERCENTAGE - Math.round((afterLength / beforeLength) * PERCENTAGE);

	if (compressionRatio > -1) {
	    totalCompressoinRatioSum += compressionRatio;
	    totalFileCnt++;

	    printFileName(resource, resultBuilder);
	    resultBuilder.append("Before Length: ").append(beforeLength).append(BR);
	    resultBuilder.append("After Length: ").append(afterLength).append(BR);
	    resultBuilder.append("Compression Rate: ").append(compressionRatio).append("%").append(BR);

	    if (compressionRatio > THRESHOLD) {
		solutionBuilder.append("Below File Needs to be Compressed").append(BR);
		printFileName(resource, solutionBuilder);
	    }
	} else {
	    generateCompressionError(resourceType, resource, "Syntax Error");
	}
    }

    private void generateCompressionError(FileType resourceType, Resource resource, String message) {
	printFileName(resource, resultBuilder);
	resultBuilder.append(message).append(BR);
    }

    private void printFileName(Resource resource, StringBuilder builder) {
	if (resource instanceof ExtResource) {
	    builder.append(((ExtResource) resource).getFileName()).append(BR);
	} else if (resource instanceof IntResource) {
	    builder.append(resource.getLinkLine()).append(BR);
	}
    }

    private void summary() {
	setResult(resultBuilder.toString());
	setSolution(solutionBuilder.toString());
	grade = (totalFileCnt > 0) ? 100 - (totalCompressoinRatioSum / totalFileCnt) : 100;
	setGrade(grade);
    }

}
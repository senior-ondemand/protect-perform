package com.pnp.module;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.module.perform.Perform;
import com.pnp.module.perform.PerformFactory;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.service.InspectionType;

public class PerformInspection extends Inspection {

    private static Logger logger = LoggerFactory.getLogger(PerformInspection.class);

    private PerformInspection() {
	super(InspectionType.PERFORM);
    }

    public static PerformInspection create() {
	return new PerformInspection();
    }

    @Override
    public void exe() {
	logger.info("PerformInspection Thread Start");

	Perform perform = PerformFactory.create((PerformRuleType) rule.getType());
	perform.initialize((PerformRuleType) rule.getType(), targetURL, preParsedResponse);
	perform.setTitle(rule.getTitle());
	perform.exe();

	if (socketIOCallback != null) {
	    socketIOCallback.emit(inspectionType.toString(), PNPConverter.performResultToJson(perform));
	}

	scanResultDAO.set(index, perform.getCategory(), perform.getGrade(), userSetting);
    }

}
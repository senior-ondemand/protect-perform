package com.pnp.module;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class StrLib {

    private static final String CARRIAGE_RETURN = "\r\n";
    private static final String RETURN = "\n";

    public static String removeNewLine(String keyString) {
	int indexOfNewLineForWin = keyString.indexOf(CARRIAGE_RETURN);
	int indexOfNewLineForLinux = keyString.indexOf(RETURN);
	int indexOfNewLine = 0;

	if (hasNoReturn(indexOfNewLineForWin, indexOfNewLineForLinux)) {
	    indexOfNewLine = keyString.length();
	} else if (hasEitherReturn(indexOfNewLineForWin, indexOfNewLineForLinux)) {
	    indexOfNewLine = Math.max(indexOfNewLineForWin, indexOfNewLineForLinux);
	} else if (hasBothReturn(indexOfNewLineForWin, indexOfNewLineForLinux)) {
	    indexOfNewLine = Math.min(indexOfNewLineForWin, indexOfNewLineForLinux);
	} else {
	    throw new IllegalArgumentException("개행 문자 위치 오류");
	}

	return keyString.substring(0, indexOfNewLine);
    }

    private static boolean hasBothReturn(int indexOfNewLineForWin, int indexOfNewLineForLinux) {
	return indexOfNewLineForWin > 0 && indexOfNewLineForLinux > 0;
    }

    private static boolean hasEitherReturn(int indexOfNewLineForWin, int indexOfNewLineForLinux) {
	return indexOfNewLineForWin == StringUtils.INDEX_NOT_FOUND || indexOfNewLineForLinux == StringUtils.INDEX_NOT_FOUND;
    }

    private static boolean hasNoReturn(int indexOfNewLineForWin, int indexOfNewLineForLinux) {
	return indexOfNewLineForWin == StringUtils.INDEX_NOT_FOUND && indexOfNewLineForLinux == StringUtils.INDEX_NOT_FOUND;
    }

    public static String removeHtmlComment(String str) {
	return str.replaceAll("(?s)<!--.*?-->", StringUtils.EMPTY);
    }

    public static String removeCssComment(String str) {
	return removeMultiLineComment(str);
    }

    private static String removeMultiLineComment(String str) {
	// responseBody.replaceAll("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)",
	// StringUtils.EMPTY);
	return str;
    }

    public static String removeJsComment(String str) {
	return removeMultiLineComment(removeSingleLineComment(str));
    }

    public static String removeSingleLineComment(String str) {
	return str.replaceAll("(?m)^\\s*//.*$", StringUtils.EMPTY);
    }

    public static String subLine(String string, int lineNo) {
	Scanner scanner = new Scanner(string);
	StringBuilder lineBuilder = new StringBuilder();

	for (int i = 0; (scanner.hasNextLine() && i < lineNo); i++) {
	    lineBuilder.append(scanner.nextLine());
	}

	return lineBuilder.toString();
    }

}
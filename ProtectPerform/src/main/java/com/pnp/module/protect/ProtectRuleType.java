package com.pnp.module.protect;

import com.pnp.module.RuleType;

public enum ProtectRuleType implements RuleType {
    INJ, BAS, XSS, IOR, SMC, SDE, MLA, XRF, KVC, URF
}
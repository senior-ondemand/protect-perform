package com.pnp.module.protect;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.PreParsedResponse;
import com.pnp.data.ProtectPreParsedResponse;
import com.pnp.data.ProtectResult;
import com.pnp.data.TestCase;
import com.pnp.data.html.FormTag;
import com.pnp.data.html.InputTag;
import com.pnp.module.SSlLib;

public class Protect {

    public enum Rule {
	A1(0, "INJ"), A2(1, "BAS"), A3(2, "XSS"), A4(3, "IOR"), A5(4, "SMC"), A6(5, "SDE"), A7(6, "MLA"), A8(7, "XRF"), A9(8, "KVC"), A10(9, "URF");

	private final int ruleIndex;
	private final String ruleName;

	private Rule(int ruleIndex, String ruleName) {
	    this.ruleIndex = ruleIndex;
	    this.ruleName = ruleName;
	}

	public int getRuleIndex() {
	    return ruleIndex;
	}

	public String getRuleName() {
	    return ruleName;
	}
    };

    private static final String DEFUALT_FORM_VALUE = "ProtectnPerform";
    private static Logger logger = LoggerFactory.getLogger(Protect.class);

    private String targetURL;
    private TestCase testCase;
    private PreParsedResponse preParsedResponse;

    private HttpClient httpclient;
    private HttpUriRequest httpMethod;

    private Map<String, String> testCaseHash;
    private URI uri;

    private ProtectResult protectResult;
    private ArrayList<FormTag> formTag;

    public Protect() {

    }

    public Protect(String targetURL, TestCase testCase, PreParsedResponse preParsedResponse) {
	this.targetURL = targetURL;
	this.testCase = testCase;
	this.preParsedResponse = preParsedResponse;

	httpclient = HttpClientBuilder.create().build();
	testCaseHash = testCase.getTestCaseAttack();
    }

    public URI getUri() {
	return uri;
    }

    private void setURI() {
	formTag = ((ProtectPreParsedResponse) preParsedResponse).getFormTagList();

	URIBuilder urib = new URIBuilder();
	URL targetURLFormat = null;
	try {
	    targetURLFormat = new URL(targetURL);
	} catch (MalformedURLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	urib.setScheme(targetURLFormat.getProtocol());
	urib.setHost(targetURLFormat.getHost());

	/**
	 * path setting
	 */
	String path_type = testCaseHash.get("path_type");
	if (path_type == null) {
	    logger.info("path_type is null" + "\nin " + testCase.getTestCaseName());
	} else if (path_type.equalsIgnoreCase("action")) {
	    UrlValidator urlValidator = new UrlValidator();
	    if (urlValidator.isValid(formTag.get(0).getAction())) {
		URL actionURLFormat = null;
		try {
		    actionURLFormat = new URL(formTag.get(0).getAction());
		} catch (MalformedURLException e) {
		    e.printStackTrace();
		}
		urib.setScheme(actionURLFormat.getProtocol());
		urib.setHost(actionURLFormat.getHost());
		urib.setPath("/" + actionURLFormat.getPath());
	    } else {
		// set action path in form
		urib.setPath("/" + formTag.get(0).getAction());
	    }
	} else if (path_type.equalsIgnoreCase("a")) {
	    // set a tag path (Unimplemented)
	    // urib.setPath(formTag.get(0).getATag());
	} else if (path_type.equalsIgnoreCase("path")) {
	    // set static path in redis
	    urib.setPath(testCaseHash.get("path"));
	} else if (path_type.equalsIgnoreCase("default")) {
	    // set default path
	    urib.setPath(targetURLFormat.getPath());
	} else {
	    logger.info(path_type + " is invalid key in path_type" + "\nin " + testCase.getTestCaseName());
	}

	/**
	 * Step 1. parameter setting
	 */

	if (testCaseHash.get("method").equalsIgnoreCase("get")) {
	    urib = getSettingParamsUrib(urib);
	}

	try {
	    uri = urib.build();
	} catch (URISyntaxException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void setMethod() {
	/**
	 * Step 2. method setting
	 */
	String methodName = testCaseHash.get("method");
	if (methodName == null) {
	    logger.info("methodName is null" + "\nin " + testCase.getTestCaseName());
	} else if (methodName.equalsIgnoreCase("GET")) {
	    httpMethod = new HttpGet(uri);
	} else if (methodName.equalsIgnoreCase("POST")) {
	    httpMethod = new HttpPost(uri);
	    try {
		((HttpPost) httpMethod).setEntity(new UrlEncodedFormEntity(getSettingParamsNvps()));
	    } catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	} else if (methodName.equalsIgnoreCase("DELETE")) {
	    httpMethod = new HttpDelete(uri);
	} else if (methodName.equalsIgnoreCase("PUT")) {
	    httpMethod = new HttpPut(uri);
	} else if (methodName.equalsIgnoreCase("HEAD")) {
	    httpMethod = new HttpHead(uri);
	} else if (methodName.equalsIgnoreCase("OPTIONS")) {
	    httpMethod = new HttpOptions(uri);
	} else {
	    logger.info("This method does not exist" + "\nin " + testCase.getTestCaseName());
	}
    }

    private void setHeader() {
	/**
	 * Step 3. header setting
	 */
	int index = 0;
	for (String fieldValue : testCaseHash.keySet()) {
	    if (fieldValue.equalsIgnoreCase("header_param" + index)) {
		httpMethod.setHeader(testCaseHash.get(fieldValue), testCaseHash.get("header_value" + index));
		index++;
	    }
	}
    }

    public void exe() {
	protectResult = new ProtectResult();

	String attackType = testCaseHash.get("attack_type");
	if (attackType != null) {
	    switch (attackType) {
	    case "ssl": {
		protectResult.setResultString(SSlLib.getSslVersion(targetURL));
	    }
	    }
	} else {
	    setURI();
	    setMethod();
	    setHeader();
	    try {
		// proxy 사용
//		HttpHost proxy = new HttpHost("localhost", 8888);
//		DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
//		CloseableHttpClient httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();

		protectResult.setResponse(httpclient.execute(httpMethod));
	    } catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    private URIBuilder getSettingParamsUrib(URIBuilder urib) {
	int index;
	ArrayList<InputTag> inputTag = formTag.get(0).getInputTags();

	for (index = 0; testCaseHash.containsKey("value" + index); index++) {
	    if (testCaseHash.containsValue("param" + index)) {
		// if formField in redis is exist <- priority 1
		urib.setParameter(testCaseHash.get("param" + index), testCaseHash.get("value" + index));
	    } else if (!inputTag.get(index).getName().isEmpty()) {
		// if formField in PreParsedPage is exist <- priority 2
		urib.setParameter(inputTag.get(index).getName(), testCaseHash.get("value" + index));
	    } else {
		logger.info("Input field is not exist" + "\nin " + testCase.getTestCaseName());
		break;
	    }
	}

	// Fill the rest of the form blank
	for (; index < inputTag.size(); index++) {
	    urib.setParameter(inputTag.get(index).getName(), DEFUALT_FORM_VALUE);
	}

	return urib;
    }

    private List<NameValuePair> getSettingParamsNvps() {
	int index;
	ArrayList<InputTag> inputTag = formTag.get(0).getInputTags();
	List<NameValuePair> nvps = new ArrayList<NameValuePair>();

	for (index = 0; testCaseHash.containsKey("value" + index); index++) {
	    if (testCaseHash.containsValue("param" + index)) {
		// if formField in redis is exist <- priority 1
		nvps.add(new BasicNameValuePair(testCaseHash.get("param" + index), testCaseHash.get("value" + index)));
	    } else if (!inputTag.get(index).getName().isEmpty()) {
		// if formField in PreParsedPage is exist <- priority 2
		nvps.add(new BasicNameValuePair(inputTag.get(index).getName(), testCaseHash.get("value" + index)));
	    } else {
		logger.info("Input field is not exist" + "\nin " + testCase.getTestCaseName());
		break;
	    }
	}
	return nvps;
    }

    public ProtectResult getProtectResult() {
	return protectResult;
    }

}
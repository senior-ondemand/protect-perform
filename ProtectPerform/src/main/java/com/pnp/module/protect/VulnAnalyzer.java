package com.pnp.module.protect;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.pnp.common.Common;
import com.pnp.data.ProtectResult;
import com.pnp.module.SSlLib;

public class VulnAnalyzer {

    public enum AnalyzerResult {
	High, Medium, Low
    }

    public boolean analyze(ProtectResult protectResult, Map<String, String> testCaseVerificationMap) {
	String verificationType = testCaseVerificationMap.get("veri_type");
	if (verificationType != null) {
	    switch (verificationType) {
	    case "ssl": {
		return !SSlLib.isSecureVersion(protectResult.getResultString());
	    }
	    }
	}
	HttpResponse response = protectResult.getResponse();
	boolean analyzerResult = false;
	// status code
	if (testCaseVerificationMap.containsKey("status_code")) {
	    int statusCode = response.getStatusLine().getStatusCode();
	    if (Integer.parseInt(testCaseVerificationMap.get("status_code")) == statusCode) {
		analyzerResult = true;
	    }
	}

	// headers
	if (testCaseVerificationMap.containsKey("header_param0")) {
	    String header_param;
	    int headerCheckerAllCount = 0;
	    int headerCheckerCount = 0;
	    for (int idx = 0; (header_param = testCaseVerificationMap.get("header_param" + idx)) != null; idx++) {
		headerCheckerAllCount++;
		for (Header header : response.getAllHeaders()) {
		    if (StringUtils.containsIgnoreCase(header_param, header.getName())) {
			if (StringUtils.containsIgnoreCase(header.getValue(), testCaseVerificationMap.get("header_value" + idx))) {
			    headerCheckerCount++;
			    break;
			}
		    }
		}
	    }

	    if (headerCheckerAllCount == headerCheckerCount)
		analyzerResult = true;
	    else
		analyzerResult = false;
	}

	// body
	if (testCaseVerificationMap.containsKey("body0")) {
	    String responseBody = Common.httpResponseToResponseBody(response);
	    String body_value;

	    switch (testCaseVerificationMap.get("body_type")) {
	    case "equal": {
		for (int idx = 0; (body_value = testCaseVerificationMap.get("body" + idx)) != null; idx++) {
		    if (responseBody.equalsIgnoreCase(body_value)) {
			analyzerResult = true;
		    } else {
			analyzerResult = false;
			break;
		    }
		}
		break;
	    } // body type is "equal"
	    case "contains": {
		for (int idx = 0; (body_value = testCaseVerificationMap.get("body" + idx)) != null; idx++) {
		    if (StringUtils.containsIgnoreCase(responseBody, body_value)) {
			analyzerResult = true;
		    } else {
			analyzerResult = false;
			break;
		    }
		}
		break;
	    } // body type is "contains"
	    case "select": {
		Document document = Jsoup.parse(responseBody);
		for (int idx = 0; (body_value = testCaseVerificationMap.get("body" + idx)) != null; idx++) {
		    if (document.select(body_value).size() > 0) {
			analyzerResult = true;
		    } else {
			analyzerResult = false;
			break;
		    }
		}
		break;
	    }
	    case "regex": {
		for (int idx = 0; (body_value = testCaseVerificationMap.get("body" + idx)) != null; idx++) {
		    Pattern pattern = Pattern.compile(body_value);
		    Matcher matcher = pattern.matcher(responseBody);
		    if (matcher.find()) {
			analyzerResult = true;
		    } else {
			analyzerResult = false;
			break;
		    }
		}
	    }
	    } // body type is "select"
	}

	return analyzerResult;
    }
}
package com.pnp.module;

import com.pnp.service.InspectionType;

public class InspectionFactory {

    public static Inspection create(InspectionType inspectionType) {
	switch (inspectionType) {
	case PROTECT:
	    return ProtectInspection.create();
	case PERFORM:
	    return PerformInspection.create();

	default:
	    throw new IllegalArgumentException("없는 카테고리");
	}
    }
}
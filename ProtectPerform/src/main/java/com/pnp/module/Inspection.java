package com.pnp.module;

import com.pnp.dao.ScanResultDAO;
import com.pnp.data.PreParsedResponse;
import com.pnp.data.Rule;
import com.pnp.data.UserSetting;
import com.pnp.service.InspectionType;
import com.pnp.socketIO.SocketIOCallback;

public abstract class Inspection {

    protected InspectionType inspectionType;
    protected Rule rule;
    protected PreParsedResponse preParsedResponse;
    protected String targetURL;
    protected Integer index;
    protected UserSetting userSetting;
    public SocketIOCallback socketIOCallback;
    public ScanResultDAO scanResultDAO;

    public Inspection(InspectionType perform) {
	this.inspectionType = perform;
    }

    public void initialize(Integer index, UserSetting userSetting , Rule rule, PreParsedResponse preParsedPage, SocketIOCallback socketIOCallback, ScanResultDAO scanResultDAO) {
    this.index = index;
	this.rule = rule;
	this.preParsedResponse = preParsedPage;
	this.userSetting = userSetting;
	this.targetURL = userSetting.getTargetURL();
	this.socketIOCallback = socketIOCallback;
	this.scanResultDAO = scanResultDAO;
    }
    
    public void exe(){}

}
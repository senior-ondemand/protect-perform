package com.pnp.dao;

import java.util.Set;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.ScanResult;
import com.pnp.data.UserSetting;

public class ProtectScanResultDAO extends DAO implements ScanResultDAO {

	public static ProtectScanResultDAO create() {
		return new ProtectScanResultDAO();
	}

	@Override
	public void set(Integer index, String testCase, Integer score, UserSetting userSetting) {
		
		// userip
		// useremail
		// targeturl
			
		String key = "LOG:" + userSetting.getTargetURL() + ":" + index;
		
		//jedis.hset(key, "userip", scanResult.getUserIp());
		//jedis.hset(key, "useremail", scanResult.getUserIp());
		jedis.hset(key, testCase, "" + (score == 1 ? "true" : "false"));
		
	}

	public void setContents(PerformPreParsedResponse parsedResponse) {
		// TODO Auto-generated method stub
		
	}
}

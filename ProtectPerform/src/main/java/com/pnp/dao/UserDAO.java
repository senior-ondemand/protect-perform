package com.pnp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.data.User.Interval;

public class UserDAO extends DAO {

    private static final String ALL = "*";

    public UserDAO() {
	prefix = "USR:";
    }

    public boolean signUp(User user) {
	return hmsetUser(user);
    }

    private HashMap<String, String> userToMap(User user) {
	HashMap<String, String> userMap = new HashMap<>();
	userMap.put(User.Field.Password.toString(), user.getPassword());
	userMap.put(User.Field.Name.toString(), user.getName());
	userMap.put(User.Field.Url.toString(), user.getUrl());
	userMap.put(User.Field.Activation.toString(), user.getActivity());
	userMap.put(User.Field.ActivationLink.toString(), user.getActivationLink());
	userMap.put(User.Field.SignUpDateTime.toString(), user.getSignUpDateTime());
	userMap.put(User.Field.ActivationDateTime.toString(), user.getActivationDateTime());
	userMap.put(User.Field.ScheduleInterval.toString(), user.getScheduleInterval());

	return userMap;
    }

    public User signIn(User user) {
	return getUserByEmail(user);
    }

    public User getUserByEmail(User user) {
	Map<String, String> userFromRedisMap = jedis.hgetAll(prefix + user.getEmail());
	User signInUser = new User(user.getEmail(), userFromRedisMap.get(User.Field.Password.toString()), userFromRedisMap.get(User.Field.Name
		.toString()), userFromRedisMap.get(User.Field.Url.toString()), userFromRedisMap.get(User.Field.Activation.toString()),
		userFromRedisMap.get(User.Field.ActivationLink.toString()), userFromRedisMap.get(User.Field.SignUpDateTime.toString()),
		userFromRedisMap.get(User.Field.ActivationDateTime.toString()), userFromRedisMap.get(User.Field.ScheduleInterval.toString()));
	if (existAccount(signInUser) == false) {
	    signInUser = null;
	}

	return signInUser;
    }

    private boolean existAccount(User signInUser) {
	return (signInUser.getPassword() != null && signInUser.getName() != null && signInUser.getUrl() != null && signInUser.getActivity() != null
		&& signInUser.getActivationLink() != null && signInUser.getSignUpDateTime() != null && signInUser.getScheduleInterval() != null);
    }

    public boolean deactivate(User user) {
	return jedis.del(prefix + user.getEmail()).equals(EXIST);
    }

    public boolean setAccount(User user) {
	return hmsetUser(user);
    }

    private boolean hmsetUser(User user) {
	HashMap<String, String> userMap = userToMap(user);
	return jedis.hmset(prefix + user.getEmail(), userMap).equals(OK);
    }

    public ArrayList<User> getScheduleJobByInterval(Interval interval) {
	ArrayList<User> scheduleUserList = new ArrayList<>();
	Set<String> keyList = jedis.keys(prefix + ALL);
	User user = new User();
	User userFromRedis = new User();

	for (String key : keyList) {
	    user.setEmail(keyToUserEmail(key));
	    userFromRedis = getUserByEmail(user);

	    if (isValidScheduleInterval(interval, userFromRedis)) {
		scheduleUserList.add(userFromRedis);
	    }
	}

	return scheduleUserList;
    }

    private boolean isValidScheduleInterval(Interval interval, User userFromRedis) {
	return userFromRedis.getActivity().equals(Activity.Activated.toString()) && EnumUtils.isValidEnum(Interval.class, interval.toString());
    }

    private String keyToUserEmail(String key) {
	return StringUtils.removeStart(key, prefix);
    }

    public int truncate() {
	Set<String> keys = jedis.keys(prefix + ALL);
	for (String key : keys) {
	    jedis.del(key);
	}

	return keys.size();
    }

    public int count() {
	return jedis.keys(prefix + ALL).size();
    }

}
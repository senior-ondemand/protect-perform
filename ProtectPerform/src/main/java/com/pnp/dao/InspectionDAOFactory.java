package com.pnp.dao;

import com.pnp.service.InspectionType;

public class InspectionDAOFactory {

    public static InspectionDAO create(InspectionType inspectionType) {
	switch (inspectionType) {
	case PROTECT:
	    return ProtectInspectionDAO.create();
	case PERFORM:
	    return PerformInspectionDAO.create();

	default:
	    throw new IllegalArgumentException("없는 카테고리");
	}
    }

}
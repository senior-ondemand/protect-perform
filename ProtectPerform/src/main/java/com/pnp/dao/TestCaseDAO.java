package com.pnp.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.pnp.data.UserSetting;

public class TestCaseDAO extends DAO {
    private final static String ProtectTestCases = "PRO:*:ATK:*";
    private final static String PerformTestCases = "PER:*:INF";

    public TestCaseDAO() {
	super();
    }

    public Map<String, Integer> getNumOfTotalTestCases(UserSetting userSetting) {
	long ProtectListSize = jedis.llen("rule");
	long PerformListSize = jedis.llen("performRules");
	String ruleName;
	Integer numOfProtectKeys = 0;
	Integer numOfPerformKeys = 0;
	
	for (int i = 0; i < ProtectListSize; i++) {
	    if (userSetting.getProtectList().get(i)) {
		ruleName = jedis.lindex("rule", i);
		Set<String> protectKeys = jedis.keys("PRO:" + ruleName + ":ATK:*");
		numOfProtectKeys += protectKeys.size();
	    }
	}
	for (int i = 0; i < PerformListSize; i++) {
	    if (userSetting.getPerformList().get(i)) {
		numOfPerformKeys ++;
	    }
	}
	Map<String, Integer> numOfTotalTestCases = new HashMap<String, Integer>();
	numOfTotalTestCases.put("protect", numOfProtectKeys);
	numOfTotalTestCases.put("perform", numOfPerformKeys);

	return numOfTotalTestCases;
    }
}

package com.pnp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.pnp.data.PerformRule;
import com.pnp.data.ProtectRule;
import com.pnp.data.ProtectTestCase;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.module.perform.PerformRuleType;
import com.pnp.module.protect.ProtectRuleType;

public class PNPCategoriesDAO extends DAO {

    private final static String ProtectCategoryName = "PRO";
    private final static String PerformCategoryName = "PER";
    private final static String attackName = "ATK";
    private final static String informationName = "INF";

    public PNPCategoriesDAO() {
	super();
    }

    public Map<String, Rule> getProtectCategories() {
	Map<String, Rule> protectRulesMap = new LinkedHashMap<String, Rule>();
//		new HashMap<String, Rule>();

	long ProtectListSize = jedis.llen("rule");

	String ruleName;

	for (long i = 0; i < ProtectListSize; i++) {
	    Rule protectRule = new ProtectRule();
	    ArrayList<TestCase> testCases = new ArrayList<TestCase>();

	    // Get key list of rules from redis
	    ruleName = jedis.lindex("rule", i);

	    // Get Attack keys with testCaseNumber from redis
	    Set<String> redisTestCaseHash;
	    redisTestCaseHash = jedis.keys(ProtectCategoryName + ":" + ruleName + ":" + attackName + ":*");
	    for (Iterator<String> it = redisTestCaseHash.iterator(); it.hasNext();) {
		String key = it.next();
		TestCase protectTestCase = new ProtectTestCase();
		protectTestCase.setTestCaseName(key);
		testCases.add(protectTestCase);
	    }

	    String mainKey = ProtectCategoryName + ":" + ruleName;
	    Map<String, String> informationMap = jedis.hgetAll(mainKey + ":" + informationName);
	    protectRule.setTitle(informationMap.get("title"));
	    protectRule.setType(ProtectRuleType.valueOf(ruleName));
	    protectRule.setTestCases(testCases);
	    protectRulesMap.put(ruleName, protectRule);
	}
	return protectRulesMap;
    }

    public Map<String, Rule> getPerformCategories() {
	Map<String, Rule> performRulesMap = new HashMap<String, Rule>();

	long PerformListSize = jedis.llen("performRules");

	String ruleName;

	for (int i = 0; i < PerformListSize; i++) {
	    Rule performRule = new PerformRule();

	    // Get key list of rules from redis
	    ruleName = jedis.lindex("performRules", i);

	    // Get hashmap data of rule
	    Map<String, String> performRuleMap = jedis.hgetAll(PerformCategoryName + ":" + ruleName + ":" + informationName);

	    performRule.setTitle(performRuleMap.get("title"));
	    performRule.setType(PerformRuleType.valueOf(ruleName));
	    performRule.setCategory(PerformCategoryName + ":" + ruleName);
	    performRulesMap.put(ruleName, performRule);
	}

	return performRulesMap;
    }
}
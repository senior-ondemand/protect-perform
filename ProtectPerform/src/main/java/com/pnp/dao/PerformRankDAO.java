package com.pnp.dao;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class PerformRankDAO extends DAO {

    private static final String performTop = "LOG:PER:TOP";

    private static boolean ASC = true;
    private static boolean DESC = false;

    public Map<String, Integer> getPerformRank(String type) {
	String performTopType = performTop + ":" + type;
	Map<String, Integer> map = convertStringIntegerToStringString(jedis.hgetAll(performTopType));
	return sortByComparator(map, DESC);
    }

    public void setPerformRank(String type, String url, int grade) {
	String performTopType = performTop + ":" + type;
	Map<String, Integer> map = convertStringIntegerToStringString(jedis.hgetAll(performTopType));
	map = sortByComparator(map, ASC);
	String minimumKey = map.keySet().iterator().next();
	if (map.get(minimumKey) < grade) {
	    if (map.containsKey(url)) {
		jedis.hdel(performTopType, url);
		jedis.hset(performTopType, url, String.valueOf(grade));
	    } else {
		jedis.hdel(performTopType, minimumKey);
		jedis.hset(performTopType, url, String.valueOf(grade));
	    }
	}
    }

    private static Map<String, Integer> convertStringIntegerToStringString(Map<String, String> map) {
	Map<String, Integer> stringIntegerMap = new HashMap<String, Integer>();
	for (String key : map.keySet()) {
	    stringIntegerMap.put(key, Integer.valueOf(map.get(key)));
	}
	return stringIntegerMap;
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order) {

	List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());

	// Sorting the list based on values
	Collections.sort(list, new Comparator<Entry<String, Integer>>() {
	    public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
		if (order) {
		    return o1.getValue().compareTo(o2.getValue());
		} else {
		    return o2.getValue().compareTo(o1.getValue());
		}
	    }
	});

	// Maintaining insertion order with the help of LinkedList
	Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
	for (Entry<String, Integer> entry : list) {
	    sortedMap.put(entry.getKey(), entry.getValue());
	}

	return sortedMap;
    }
}
package com.pnp.dao;

import com.pnp.service.InspectionType;

public class ScanResultDAOFactory {

	public static ScanResultDAO create(InspectionType inspectionType) {
		switch (inspectionType) {
		case PROTECT:
			return ProtectScanResultDAO.create();
		case PERFORM:
			return PerformScanResultDAO.create();

		default:
			throw new IllegalArgumentException("없는 카테고리");
		}
	}

}
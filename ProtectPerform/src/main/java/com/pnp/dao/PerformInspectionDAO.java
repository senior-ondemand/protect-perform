package com.pnp.dao;

import java.util.HashMap;
import java.util.Map;

import com.pnp.data.PerformRule;
import com.pnp.data.Rule;
import com.pnp.data.UserSetting;
import com.pnp.module.perform.PerformRuleType;

public class PerformInspectionDAO extends DAO implements InspectionDAO {

    private final static String informationName = "INF";
    private final static String categoryName = "PER";

    private PerformInspectionDAO() {
	super();
    }

    public static PerformInspectionDAO create() {
	return new PerformInspectionDAO();
    }

    @Override
    public Map<String, Rule> get(UserSetting userSetting) {
	Map<String, Rule> performRulesMap = new HashMap<String, Rule>();

	int PerformListSize = userSetting.getPerformList().size();

	String ruleName;

	for (int i = 0; i < PerformListSize; i++) {
	    if (userSetting.getPerformList().get(i)) {
		Rule performRule = new PerformRule();

		// Get key list of rules from redis
		ruleName = jedis.lindex("performRules", i);

		// Get hashmap data of rule
		Map<String, String> performRuleMap = jedis.hgetAll(categoryName + ":" + ruleName + ":" + informationName);

		performRule.setTitle(performRuleMap.get("title"));
		performRule.setType(PerformRuleType.valueOf(ruleName));
		performRulesMap.put(ruleName, performRule);
	    }
	}

	return performRulesMap;
    }
}
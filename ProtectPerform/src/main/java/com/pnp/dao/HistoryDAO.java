package com.pnp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HistoryDAO extends DAO {

	public HistoryDAO() {
		super();
	}

	public TreeMap<String, String> getProtectHistoryData(String user_email, String target_url) {
		String key = "LOG:"+target_url+":*";
		Set<String> protectKeys = jedis.keys(key);
		TreeMap<String, String> protectHistoryData = new TreeMap<String, String>();
		Integer protect_score = 0;
		
		Set<String> totalProtect = jedis.keys("PRO:*");
		Integer totalProtectLength = totalProtect.size();
		
		for( String keys : protectKeys ){
			Map<String, String> data = jedis.hgetAll(keys);
						
			if( data.get("user_email").equals(user_email) ){
				
				for (Map.Entry<String, String> entry: data.entrySet()) {
    				if ( entry.getKey().startsWith("PRO") )
    					if( entry.getValue().equals("false") )
    						protect_score++;
    			}
				
				protect_score = (protect_score * 100) / totalProtectLength ;
				
				protectHistoryData.put( (String)data.get("date"), (String)protect_score.toString());
				
				protect_score = 0;
			}
		}
		
		return protectHistoryData;
	}
	
	public TreeMap<String, String> getPerformHistoryData(String user_email, String target_url) {
		String key = "LOG:"+target_url+":*";
		Set<String> performKeys = jedis.keys(key);
		TreeMap<String, String> performHistoryData = new TreeMap<String, String>();
		Integer perform_score = 0;
		
		for( String keys : performKeys ){
			Map<String, String> data = jedis.hgetAll(keys);
			
			if( data.get("user_email").equals(user_email) ){
				
				Integer counter = 0;
				
				for (Map.Entry<String, String> entry: data.entrySet()) {
    				if ( entry.getKey().startsWith("PER") ){
    					perform_score += Integer.parseInt(entry.getValue());
    					counter++;
    				}
    			}
				
				if( counter != 0 )
					perform_score = perform_score / counter; 
				
				performHistoryData.put( (String)data.get("date"), (String)perform_score.toString());
				
				perform_score = 0;
			}
		}
		
		return performHistoryData;
	}

	public Map<String, TreeMap<String, String>> getResourceData(String user_email, String target_url) {
		
		String key = "LOG:"+target_url+":*";
		Set<String> resultKeys = jedis.keys(key);
		Map<String, TreeMap<String, String>> resourceList = new HashMap<String, TreeMap<String, String>>();
		
		TreeMap<String, String> jsNum = new TreeMap<String, String>();
		TreeMap<String, String> jsSize = new TreeMap<String, String>();
		TreeMap<String, String> cssNum = new TreeMap<String, String>();
		TreeMap<String, String> cssSize = new TreeMap<String, String>();
		TreeMap<String, String> imgNum = new TreeMap<String, String>();
		TreeMap<String, String> imgSize = new TreeMap<String, String>();
		
		for( String keys : resultKeys ){
			Map<String, String> data = jedis.hgetAll(keys);
			
			if( data.get("user_email").equals(user_email) ){
				
				jsNum.put( (String)data.get("date"), (String)data.get("js_num"));
				jsSize.put( (String)data.get("date"), (String)data.get("js_size"));
				cssNum.put( (String)data.get("date"), (String)data.get("css_num"));
				cssSize.put( (String)data.get("date"), (String)data.get("css_size"));
				imgNum.put( (String)data.get("date"), (String)data.get("img_num"));
				imgSize.put( (String)data.get("date"), (String)data.get("img_size"));
			}
		}
		
		resourceList.put("js_num", jsNum);
		resourceList.put("js_size", jsSize);
		resourceList.put("css_num", cssNum);
		resourceList.put("css_size", cssSize);
		resourceList.put("img_num", imgNum);
		resourceList.put("img_size", imgSize);
		
		return resourceList;
	}
}

package com.pnp.dao;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.UserSetting;

public interface ScanResultDAO {
	public void set(Integer index, String testCase, Integer score, UserSetting userSetting);

	public void setContents(PerformPreParsedResponse parsedResponse);
}

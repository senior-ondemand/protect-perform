package com.pnp.dao;

import java.util.Map;

import com.pnp.data.Rule;
import com.pnp.data.UserSetting;

public interface InspectionDAO {
    public Map<String, Rule> get(UserSetting userSetting);
}
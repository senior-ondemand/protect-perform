package com.pnp.dao;

import org.vertx.java.core.json.JsonObject;

import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.UserSetting;
import com.pnp.module.PNPConverter;

public class PerformScanResultDAO extends DAO implements ScanResultDAO {

	public static PerformScanResultDAO create() {
		return new PerformScanResultDAO();
	}

	@Override
	public void set(Integer index, String testCase, Integer score, UserSetting userSetting) {

		// userip
		// useremail
		// targeturl
		String key = "LOG:" + userSetting.getTargetURL() + ":" + index;
		
		//jedis.hset(key, "userip", scanResult.getUserIp());
		jedis.hset(key, testCase, Integer.toString(score));

	}
	
	public void setContents(PerformPreParsedResponse parsedResponse){
		JsonObject jsonObject = PNPConverter.contentsListToJson(parsedResponse);
	
	}
}

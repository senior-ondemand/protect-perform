package com.pnp.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.pnp.data.ProtectRule;
import com.pnp.data.ProtectTestCase;
import com.pnp.data.Rule;
import com.pnp.data.TestCase;
import com.pnp.data.UserSetting;
import com.pnp.module.protect.ProtectRuleType;

public class ProtectInspectionDAO extends DAO implements InspectionDAO {

    private final static String informationName = "INF";
    private final static String categoryName = "PRO";
    private final static String attackName = "ATK";
    private final static String verificationName = "VRI";

    private ProtectInspectionDAO() {
	super();
    }

    public static ProtectInspectionDAO create() {
	return new ProtectInspectionDAO();
    }

    @Override
    public Map<String, Rule> get(UserSetting userSetting) {
	Map<String, Rule> protectRulesMap = new LinkedHashMap<String, Rule>();

	long ProtectListSize = jedis.llen("rule");

	String ruleName;

	for (int i = 0; i < ProtectListSize; i++) {
	    if (userSetting.getProtectList().get(i)) {
		Rule protectRule = new ProtectRule();
		ArrayList<TestCase> testCases = new ArrayList<TestCase>();

		// Get key list of rules from redis
		ruleName = jedis.lindex("rule", i);

		// Get Attack keys with testCaseNumber from redis
		Set<String> redisTestCaseHash;
		redisTestCaseHash = new TreeSet<String>(jedis.keys(categoryName + ":" + ruleName + ":" + attackName + ":*"));
		for (Iterator<String> it = redisTestCaseHash.iterator(); it.hasNext();) {
		    String key = it.next();
		    TestCase protectTestCase = new ProtectTestCase();
		    Map<String, String> testCaseMap = jedis.hgetAll(key);
		    protectTestCase.setTestCaseName(testCaseMap.get("title"));
		    protectTestCase.setTestCaseCategory(key);
		    protectTestCase.setTestCaseAttack(testCaseMap);
		    testCases.add(protectTestCase);
		}

		// Get Verification keys with testCaseNumber from redis
		int index = 0;
		redisTestCaseHash = new TreeSet<String>(jedis.keys(categoryName + ":" + ruleName + ":" + verificationName + ":*"));
		for (Iterator<String> it = redisTestCaseHash.iterator(); it.hasNext();) {
		    String key = it.next();
		    testCases.get(index).setTestCaseVerification(jedis.hgetAll(key));
		    index++;
		}

		Map<String, String> informationMap = jedis.hgetAll(categoryName + ":" + ruleName + ":" + informationName);

		protectRule.setTitle(informationMap.get("title"));
		protectRule.setType(ProtectRuleType.valueOf(ruleName));
		protectRule.setTestCases(testCases);
		protectRulesMap.put(ruleName, protectRule);
	    }
	}
	return protectRulesMap;
    }
}
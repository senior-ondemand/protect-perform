package com.pnp.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.pnp.data.UserSetting;

public class IndexDAO extends DAO {

    public IndexDAO() {
	super();
    }

    public Integer getIndex(String key) {
    	Set<String> currentKeys = jedis.keys(key);

    	return currentKeys.size() + 1;
    }

    public void setBaseInfo(Integer index, UserSetting userSetting){
    	Date date = new Date();
    	String dateString = "";

    	String key = "LOG:" + userSetting.getTargetURL() + ":" + index;

    	dateString = new SimpleDateFormat("yyyy-MM-dd").format(date);

		//jedis.hset(key, "userip", scanResult.getUserIp());
		jedis.hset(key, "user_email", userSetting.getUserEmail());
		jedis.hset(key, "date", dateString);
    }
}
package com.pnp.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.pnp.data.SearchResultData;

public class PNPSearchDAO extends DAO {

    private final static String ProtectCategoryName = "PRO";
    private final static String PerformCategoryName = "PER";
    private final static String attackName = "ATK";
    private final static String informationName = "INF";

    public PNPSearchDAO() {
	super();
    }

    public ArrayList<SearchResultData> searchInCategory(String keyword) {
	ArrayList<SearchResultData> searchResultDataList = new ArrayList<SearchResultData>();

	if (keyword.contains("PRO")) {
	    Set<String> redisKeys = jedis.keys("*" + keyword + "*");
	    if (redisKeys == null)
		return null;
	    for (Iterator<String> it = redisKeys.iterator(); it.hasNext();) {
		String key = it.next();
		SearchResultData searchResultData = new SearchResultData();
		searchResultData.setCategory(key);
		searchResultData.setType("protect");

		Map<String, String> testCase = jedis.hgetAll(key);
		searchResultData.setTitle(testCase.get("title"));
		searchResultData.setDescription("None");
		// searchResultData.setDescription(testCase.get("description"));
		searchResultDataList.add(searchResultData);
	    }
	} else if (keyword.contains("PER")) {
	    Map<String, String> performRule = jedis.hgetAll(keyword + ":" + informationName);
	    if (performRule == null)
		return null;
	    SearchResultData searchResultData = new SearchResultData();
	    searchResultData.setCategory(keyword);
	    searchResultData.setType("perform");
	    searchResultData.setTitle(performRule.get("title"));
	    searchResultData.setDescription(performRule.get("vulnDescription"));
	    searchResultDataList.add(searchResultData);
	} else {
	    return null;
	}

	return searchResultDataList;
    }

    public ArrayList<SearchResultData> searchInTitle(String keyword) {
	ArrayList<SearchResultData> searchResultDataList = new ArrayList<SearchResultData>();

	long ProtectListSize = jedis.llen("rule");

	String ruleName;

	for (long i = 0; i < ProtectListSize; i++) {

	    // Get key list of rules from redis
	    ruleName = jedis.lindex("rule", i);

	    // Get Attack keys with testCaseNumber from redis
	    Set<String> redisTestCaseHash;
	    redisTestCaseHash = jedis.keys(ProtectCategoryName + ":" + ruleName + ":" + attackName + ":*");
	    for (Iterator<String> it = redisTestCaseHash.iterator(); it.hasNext();) {
		String key = it.next();
		Map<String, String> testCaseMap = jedis.hgetAll(key);
		System.out.println(testCaseMap.get("title"));
		if (StringUtils.containsIgnoreCase(testCaseMap.get("title"), keyword)) {
		    SearchResultData searchResultData = new SearchResultData();
		    searchResultData.setCategory(key);
		    searchResultData.setType("protect");
		    searchResultData.setTitle(testCaseMap.get("title"));
		    searchResultData.setDescription("None");
		    // searchResultData.setDescription(testCase.get("description"));
		    searchResultDataList.add(searchResultData);
		}
	    }
	}

	long PerformListSize = jedis.llen("performRules");

	for (int i = 0; i < PerformListSize; i++) {
	    // Get key list of rules from redis
	    ruleName = jedis.lindex("performRules", i);

	    // Get hashmap data of rule
	    Map<String, String> performRule = jedis.hgetAll(PerformCategoryName + ":" + ruleName + ":" + informationName);

	    if (StringUtils.containsIgnoreCase(performRule.get("title"), keyword)) {
		SearchResultData searchResultData = new SearchResultData();
		// 데이터 추가하면 open
		searchResultData.setCategory(PerformCategoryName + ":" + ruleName);
		searchResultData.setType("perform");
		searchResultData.setTitle(performRule.get("title"));
		searchResultData.setDescription(performRule.get("vulnDescription"));
		searchResultDataList.add(searchResultData);
	    }
	}

	return searchResultDataList;
    }
}
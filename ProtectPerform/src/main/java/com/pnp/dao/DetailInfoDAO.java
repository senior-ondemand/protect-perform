package com.pnp.dao;

import com.pnp.data.DetailInfo;

public interface DetailInfoDAO {
    public DetailInfo get(String ruleName);
}

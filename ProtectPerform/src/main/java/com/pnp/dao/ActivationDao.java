package com.pnp.dao;

import java.util.HashMap;
import java.util.Map;

import com.pnp.data.Activation;

public class ActivationDao extends DAO {

    public ActivationDao() {
	prefix = "ACT:";
    }

    public boolean insertActivation(Activation activation) {
	HashMap<String, String> hashMap = new HashMap<>();
	hashMap.put(Activation.Field.UserEmail.toString(), activation.getUserEmail());

	return jedis.hmset(prefix + activation.getActivationLink(), hashMap).equals(OK);
    }

    public boolean existsActivationLink(Activation activation) {
	return jedis.exists(prefix + activation.getActivationLink());
    }

    public boolean delete(Activation activation) {
	return jedis.del(prefix + activation.getActivationLink()).equals(EXIST);
    }

    public Activation getActivationByLink(Activation activation) {
	Map<String, String> activationFromRedisMap = jedis.hgetAll(prefix + activation.getActivationLink());
	Activation activationFromRedis = new Activation(activation.getActivationLink(), activationFromRedisMap.get(Activation.Field.UserEmail
		.toString()));

	if (existsActivation(activationFromRedis) == false) {
	    activationFromRedis = null;
	}

	return activationFromRedis;
    }

    private boolean existsActivation(Activation activationFromRedis) {
	return activationFromRedis.getUserEmail() != null;
    }
}

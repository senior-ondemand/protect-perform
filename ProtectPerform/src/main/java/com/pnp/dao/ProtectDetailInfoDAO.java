package com.pnp.dao;

import java.util.ArrayList;
import java.util.Map;

import com.pnp.data.DetailInfo;
import com.pnp.data.ProtectDetailInfo;

public class ProtectDetailInfoDAO extends DAO implements DetailInfoDAO {
    private final static String informationName = "INF";
    private final static String classificationName = "CLS:NAM";
    private final static String classificationValue = "CLS:VAL";
    private final static String referenceName = "REF:NAM";
    private final static String referenceValue = "REF:VAL";
    private final static String verificationName = "VRI";

    public ProtectDetailInfoDAO() {
	super();
    }

    public static ProtectDetailInfoDAO create() {
	return new ProtectDetailInfoDAO();
    }

    @Override
    public DetailInfo get(String category) {
	ProtectDetailInfo protectDetailInfo = new ProtectDetailInfo();

	protectDetailInfo.setType("protect");
	String[] splitCategory = category.split(":");
	String categoryName = splitCategory[0];
	String mainKey = categoryName + ":" + splitCategory[1];
	String attackCategoryKey = category;
	String verificationCategoryKey = categoryName + ":" + splitCategory[1] + ":" + verificationName + ":" + splitCategory[3];

	// PRO:[ruleName]:INF
	Map<String, String> informationMap = jedis.hgetAll(mainKey + ":" + informationName);
	protectDetailInfo.setTitle(informationMap.get("title"));
	protectDetailInfo.setDescription(informationMap.get("vulnDescription"));
	protectDetailInfo.setRemedy(informationMap.get("remedy"));

	// PRO:[ruleName]:CLS:NAM, PRO:[ruleName]:CLS:VAL
	ArrayList<String> classificationNameList = new ArrayList<String>(jedis.lrange(mainKey + ":" + classificationName, 0,
		(jedis.llen(mainKey + ":" + classificationName))));
	protectDetailInfo.setClassificationNameList(classificationNameList);
	ArrayList<String> classificationValueList = new ArrayList<String>(jedis.lrange(mainKey + ":" + classificationValue, 0,
		(jedis.llen(mainKey + ":" + classificationValue))));
	protectDetailInfo.setClassificationValueList(classificationValueList);
	ArrayList<String> referenceNameList = new ArrayList<String>(jedis.lrange(mainKey + ":" + referenceName, 0,
		(jedis.llen(mainKey + ":" + referenceName))));
	protectDetailInfo.setReferenceNameList(referenceNameList);
	ArrayList<String> referenceValueList = new ArrayList<String>(jedis.lrange(mainKey + ":" + referenceValue, 0,
		(jedis.llen(mainKey + ":" + referenceValue))));
	protectDetailInfo.setReferenceValueList(referenceValueList);

	// PRO:[ruleName]:ATK:[testCaseNum] == category
	Map<String, String> attackTestCase = jedis.hgetAll(attackCategoryKey);
	Map<String, String> verificationTestCase = jedis.hgetAll(verificationCategoryKey);

	protectDetailInfo.setTestCaseTitle(attackTestCase.get("title"));
	protectDetailInfo.setLevel(attackTestCase.get("level"));
	protectDetailInfo.setCategory(category);

	// Get attackPattern
	String attackPattern = "";
	for (Object key : attackTestCase.keySet()) {
	    attackPattern += key.toString() + ":" + attackTestCase.get(key) + " ";
	}
	if (!attackPattern.equals(""))
	    protectDetailInfo.setAttackPattern(attackPattern);

	// Get verificationPattern
	String verificationPattern = "";
	for (Object key : verificationTestCase.keySet()) {
	    verificationPattern += key.toString() + ":" + verificationTestCase.get(key) + " ";
	}
	if (!verificationPattern.equals(""))
	    protectDetailInfo.setVerificationPattern(verificationPattern);

	return protectDetailInfo;
    }
}

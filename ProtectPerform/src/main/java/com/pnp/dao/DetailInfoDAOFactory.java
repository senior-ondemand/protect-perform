package com.pnp.dao;

import com.pnp.service.InspectionType;

public class DetailInfoDAOFactory {

    public static DetailInfoDAO create(InspectionType inspectionType) {
	switch (inspectionType) {
	case PROTECT:
	    return ProtectDetailInfoDAO.create();
	case PERFORM:
	    return PerformDetailInfoDAO.create();

	default:
	    throw new IllegalArgumentException("없는 카테고리");
	}
    }

}
package com.pnp.dao;

import redis.clients.jedis.Jedis;

public abstract class DAO {
    private final String DB_HOST = "protectnperform.com";
    protected static final String OK = "OK";
    protected static final Long EXIST = 1L;

    protected String prefix;
    protected Jedis jedis;

    public DAO() {
	jedis = new Jedis(DB_HOST);
    }
}
package com.pnp.dao;

import java.util.ArrayList;
import java.util.Map;

import com.pnp.data.DetailInfo;
import com.pnp.data.PerformDetailInfo;

public class PerformDetailInfoDAO extends DAO implements DetailInfoDAO {
    private final static String categoryName = "PER";
    private final static String informationName = "INF";
    private final static String classificationName = "CLS:NAM";
    private final static String classificationValue = "CLS:VAL";
    private final static String referenceName = "REF:NAM";
    private final static String referenceValue = "REF:VAL";

    public PerformDetailInfoDAO() {
	super();
    }

    public static PerformDetailInfoDAO create() {
	return new PerformDetailInfoDAO();
    }

    @Override
    public DetailInfo get(String category) {
	PerformDetailInfo protectDetailInfo = new PerformDetailInfo();

	protectDetailInfo.setType("perform");
	// PRO:[ruleName]:INF
	String mainKey = category;
	Map<String, String> informationMap = jedis.hgetAll(mainKey + ":" + informationName);
	protectDetailInfo.setCategory(mainKey);
	protectDetailInfo.setTitle(informationMap.get("title"));
	protectDetailInfo.setDescription(informationMap.get("vulnDescription"));
	protectDetailInfo.setRemedy(informationMap.get("remedy"));

	// PRO:[ruleName]:REF:NAM, PRO:[ruleName]:REF:VAL
	ArrayList<String> referenceNameList = new ArrayList<String>(jedis.lrange(mainKey + ":" + referenceName, 0,
		(jedis.llen(mainKey + ":" + referenceName))));
	protectDetailInfo.setReferenceNameList(referenceNameList);
	ArrayList<String> referenceValueList = new ArrayList<String>(jedis.lrange(mainKey + ":" + referenceValue, 0,
		(jedis.llen(mainKey + ":" + referenceValue))));
	protectDetailInfo.setReferenceValueList(referenceValueList);

	return protectDetailInfo;
    }
}

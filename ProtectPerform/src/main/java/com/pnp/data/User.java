package com.pnp.data;

import org.apache.commons.lang3.StringUtils;

public class User {

    public enum Field {
	Email, Password, Name, Url, Activation, ActivationLink, SignUpDateTime, ActivationDateTime, ScheduleInterval
    }

    public enum Activity {
	Activated, Inactivated
    }

    public enum Interval {
	Daily, Weekly, Monthly
    }

    private String email = StringUtils.EMPTY;
    private String password = StringUtils.EMPTY;
    private String rePassword = StringUtils.EMPTY;
    private String name = StringUtils.EMPTY;
    private String url = StringUtils.EMPTY;
    private String activity = Activity.Inactivated.toString();
    private String activationLink = StringUtils.EMPTY;
    private String signUpDateTime = StringUtils.EMPTY;
    private String activationDateTime = StringUtils.EMPTY;
    private String scheduleInterval = StringUtils.EMPTY;

    public User() {

    }

    public User(String email, String password, String name, String url, String activateion, String activationLink, String signUpDateTime,
	    String activationDateTime, String scheduleInterval) {
	this.email = email;
	this.password = password;
	this.name = name;
	this.url = url;
	this.activity = activateion;
	this.activationLink = activationLink;
	this.signUpDateTime = signUpDateTime;
	this.activationDateTime = activationDateTime;
	this.scheduleInterval = scheduleInterval;
    }

    @Override
    public String toString() {
	StringBuilder stringBuilder = new StringBuilder();
	stringBuilder.append("Email: ").append(email).append("\n");
	stringBuilder.append("Password: ").append(password).append("\n");
	stringBuilder.append("RePassword: ").append(rePassword).append("\n");
	stringBuilder.append("Name: ").append(name).append("\n");
	stringBuilder.append("Url: ").append(url).append("\n");
	stringBuilder.append("Activation: ").append(activity).append("\n");
	stringBuilder.append("ActivationLink: ").append(activationLink).append("\n");
	stringBuilder.append("SignUpDateTime: ").append(signUpDateTime).append("\n");
	stringBuilder.append("ActivationDateTime: ").append(activationDateTime).append("\n");
	stringBuilder.append("ScheduleInterval: ").append(scheduleInterval).append("\n");

	return stringBuilder.toString();
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getRePassword() {
	return rePassword;
    }

    public void setRePassword(String rePassword) {
	this.rePassword = rePassword;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public String getActivity() {
	return activity;
    }

    public void setActivity(String activity) {
	this.activity = activity;
    }

    public String getActivationLink() {
	return activationLink;
    }

    public void setActivationLink(String activationLink) {
	this.activationLink = activationLink;
    }

    public String getSignUpDateTime() {
	return signUpDateTime;
    }

    public void setSignUpDateTime(String signUpDateTime) {
	this.signUpDateTime = signUpDateTime;
    }

    public String getActivationDateTime() {
	return activationDateTime;
    }

    public void setActivationDateTime(String activationDateTime) {
	this.activationDateTime = activationDateTime;
    }

    public String getScheduleInterval() {
	return scheduleInterval;
    }

    public void setScheduleInterval(String scheduleInterval) {
	this.scheduleInterval = scheduleInterval;
    }

}

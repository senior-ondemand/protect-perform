package com.pnp.data;

import org.apache.http.HttpResponse;

public class ProtectResult {

    private String resultString;
    private HttpResponse response;

    public String getResultString() {
	return resultString;
    }

    public void setResultString(String resultString) {
	this.resultString = resultString;
    }

    public HttpResponse getResponse() {
	return response;
    }

    public void setResponse(HttpResponse response) {
	this.response = response;
    }

}

package com.pnp.data;

import java.util.ArrayList;

public class UserSetting {
    private ArrayList<Boolean> protectList;
    private ArrayList<Boolean> performList;
    private String targetURL;
    private String scanType;
    private String userEmail;

    @Override
    public String toString() {
	StringBuilder stringBuilder = new StringBuilder();
	stringBuilder.append("ProtectList: ").append(protectList.toString()).append("\n");
	stringBuilder.append("PerformList: ").append(performList.toString()).append("\n");
	stringBuilder.append("targetUrl: ").append(targetURL).append("\n");
	stringBuilder.append("scanType: ").append(scanType).append("\n");
	stringBuilder.append("userEmail: ").append(userEmail).append("\n");

	return stringBuilder.toString();
    }

    public ArrayList<Boolean> getProtectList() {
	return protectList;
    }

    public void setProtectList(ArrayList<Boolean> protectList) {
	this.protectList = protectList;
    }

    public ArrayList<Boolean> getPerformList() {
	return performList;
    }

    public void setPerformList(ArrayList<Boolean> performList) {
	this.performList = performList;
    }

    public String getTargetURL() {
	return targetURL;
    }

    public void setTargetURL(String targetURL) {
	this.targetURL = targetURL;
    }

    public String getUserEmail() {
	return userEmail;
    }

    public void setUserEmail(String userID) {
	this.userEmail = userID;
    }

    public String getScanType() {
	return scanType;
    }

    public void setScanType(String scanType) {
	this.scanType = scanType;
    }
}

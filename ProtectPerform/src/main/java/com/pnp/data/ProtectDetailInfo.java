package com.pnp.data;

public class ProtectDetailInfo extends DetailInfo {
    private String testCaseTitle;
    private String url;
    private String attackPattern;
    private String verificationPattern;
    private String level;

    public String getTestCaseTitle() {
	return testCaseTitle;
    }

    public void setTestCaseTitle(String testCaseTitle) {
	this.testCaseTitle = testCaseTitle;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public String getAttackPattern() {
	return attackPattern;
    }

    public void setAttackPattern(String attackPattern) {
	this.attackPattern = attackPattern;
    }

    public String getVerificationPattern() {
	return verificationPattern;
    }

    public void setVerificationPattern(String verificationPattern) {
	this.verificationPattern = verificationPattern;
    }

    public String getLevel() {
	return level;
    }

    public void setLevel(String level) {
	this.level = level;
    }

}

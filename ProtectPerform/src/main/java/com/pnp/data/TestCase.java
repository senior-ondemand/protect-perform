package com.pnp.data;

import java.util.Map;

public interface TestCase {

    public String getTestCaseName();

    public void setTestCaseName(String testCaseName);

    public String getTestCaseCategory();

    public void setTestCaseCategory(String testCaseCategory);

    public Map<String, String> getTestCaseAttack();

    public void setTestCaseAttack(Map<String, String> testCaseAttack);

    public Map<String, String> getTestCaseVerification();

    public void setTestCaseVerification(Map<String, String> testCaseVerification);
}
package com.pnp.data.html;

import java.util.ArrayList;

public class FormTag {

    public static final String HTTP_POST_METHOD = "POST";
    public static final String HTTP_GET_METHOD = "GET";

    private String action;
    private String method;
    private String name;
    private String id;

    private ArrayList<InputTag> inputTags;

    public String getAction() {
	return action;
    }

    public void setAction(String action) {
	this.action = action;
    }

    public String getMethod() {
	return method;
    }

    public void setMethod(String method) {
	this.method = method;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public ArrayList<InputTag> getInputTags() {
	return inputTags;
    }

    public void setInputTags(ArrayList<InputTag> inputTags) {
	this.inputTags = inputTags;
    }
}
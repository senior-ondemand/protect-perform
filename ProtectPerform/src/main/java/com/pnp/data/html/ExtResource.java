package com.pnp.data.html;

import org.apache.commons.lang3.StringUtils;

public class ExtResource extends Resource {

    private String absUrl = StringUtils.EMPTY;
    private String fileName = StringUtils.EMPTY;

    public String getAbsUrl() {
	return absUrl;
    }

    public void setAbsUrl(String absUrl) {
	this.absUrl = absUrl;
    }

    public String getFileName() {
	return fileName;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

}
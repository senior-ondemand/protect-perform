package com.pnp.data.html;

import org.apache.http.Header;

public class ImgInfo {
    private int width;
    private int height;
    private int size;
    private String mimeType;
    private Header[] responseHeader;
    private int statusCode;

    public int getWidth() {
	return width;
    }

    public void setWidth(int width) {
	this.width = width;
    }

    public int getHeight() {
	return height;
    }

    public void setHeight(int height) {
	this.height = height;
    }

    public int getSize() {
	return size;
    }

    public void setSize(int size) {
	this.size = size;
    }

    public String getMimeType() {
	return mimeType;
    }

    public void setMimeType(String mimeType) {
	this.mimeType = mimeType;
    }

    public Header[] getResponseHeader() {
	return responseHeader;
    }

    public void setResponseHeader(Header[] responseHeader) {
	this.responseHeader = responseHeader;
    }

    public int getStatusCode() {
	return statusCode;
    }

    public void setStatusCode(int statudCode) {
	this.statusCode = statudCode;
    }
}

package com.pnp.data.html;

public class ImgTag {
    private String url;
    private String urlDomain;
    private String linkLine;
    private int width;
    private int height;
    private ImgInfo imgInfo;

    public ImgInfo getImgInfo() {
	return imgInfo;
    }

    public void setImgInfo(ImgInfo imgInfo) {
	this.imgInfo = imgInfo;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public String getUrlDomain() {
	return urlDomain;
    }

    public void setUrlDomain(String urlDomain) {
	this.urlDomain = urlDomain;
    }

    public int getWidth() {
	return width;
    }

    public void setWidth(int width) {
	this.width = width;
    }

    public int getHeight() {
	return height;
    }

    public void setHeight(int height) {
	this.height = height;
    }

    public String getLinkLine() {
	return linkLine;
    }

    public void setLinkLine(String linkLine) {
	this.linkLine = linkLine;
    }

}

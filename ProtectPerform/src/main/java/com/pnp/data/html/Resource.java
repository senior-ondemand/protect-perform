package com.pnp.data.html;

import org.apache.http.Header;

public abstract class Resource {

    private String responseBody;
    private String linkLine;

    private String url = "";
    private int statusCode = 0;
    private Header[] headers = null;
    private String domain;
    private int size;

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public String getResponseBody() {
	return responseBody;
    }

    public void setResponseBody(String contents) {
	this.responseBody = contents;
    }

    public String getLinkLine() {
	return linkLine;
    }

    public void setLinkLine(String linkLine) {
	this.linkLine = linkLine;
    }

    public int getStatusCode() {
	return statusCode;
    }

    public void setStatusCode(int statusCode) {
	this.statusCode = statusCode;
    }

    public Header[] getHeaders() {
	return headers;
    }

    public void setHeaders(Header[] headers) {
	this.headers = headers;
    }

    public String getDomain() {
	return domain;
    }

    public void setDomain(String domain) {
	this.domain = domain;
    }

    public int getSize() {
	return size;
    }

    public void setSize(int size) {
	this.size = size;
    }

}
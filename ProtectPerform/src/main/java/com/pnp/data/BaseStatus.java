package com.pnp.data;

public class BaseStatus {
	private String serverInfo;
	private String start;
	private Integer protectNumTestCases;
	private Integer performNumTestCases;
	
	public String getServerInfo() {
		return serverInfo;
	}
	public void setServerInfo(String serverInfo) {
		this.serverInfo = serverInfo;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public Integer getProtectNumTestCases() {
		return protectNumTestCases;
	}
	public void setProtectNumTestCases(Integer protectNumTestCases) {
		this.protectNumTestCases = protectNumTestCases;
	}
	public Integer getPerformNumTestCases() {
		return performNumTestCases;
	}
	public void setPerformNumTestCases(Integer performNumTestCases) {
		this.performNumTestCases = performNumTestCases;
	}	
	
}

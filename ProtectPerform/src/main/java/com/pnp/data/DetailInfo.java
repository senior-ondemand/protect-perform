package com.pnp.data;

import java.util.ArrayList;

public abstract class DetailInfo {
    private static final String eventSubname = "DetailInfoResult";
    private String type;
    private String title;
    private String category;
    private String description;
    private String remedy;
    private ArrayList<String> classificationNameList;
    private ArrayList<String> classificationValueList;
    private ArrayList<String> referenceNameList;
    private ArrayList<String> referenceValueList;

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getEventName() {
	return type + eventSubname;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getCategory() {
	return category;
    }

    public void setCategory(String category) {
	this.category = category;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String vulnDescription) {
	this.description = vulnDescription;
    }

    public String getRemedy() {
	return remedy;
    }

    public void setRemedy(String remedy) {
	this.remedy = remedy;
    }

    public ArrayList<String> getClassificationNameList() {
	return classificationNameList;
    }

    public void setClassificationNameList(ArrayList<String> classificationNameList) {
	this.classificationNameList = classificationNameList;
    }

    public ArrayList<String> getClassificationValueList() {
	return classificationValueList;
    }

    public void setClassificationValueList(ArrayList<String> classificationValueList) {
	this.classificationValueList = classificationValueList;
    }

    public ArrayList<String> getReferenceNameList() {
	return referenceNameList;
    }

    public void setReferenceNameList(ArrayList<String> referenceNameList) {
	this.referenceNameList = referenceNameList;
    }

    public ArrayList<String> getReferenceValueList() {
	return referenceValueList;
    }

    public void setReferenceValueList(ArrayList<String> referenceValueList) {
	this.referenceValueList = referenceValueList;
    }

}

package com.pnp.data;

import java.util.ArrayList;

import com.pnp.module.RuleType;
import com.pnp.module.protect.ProtectRuleType;

public class ProtectRule implements Rule {
    private ArrayList<TestCase> protectTestCases;
    private String category;
    private String title;
    private ProtectRuleType protectRuleType;

    @Override
    public ArrayList<TestCase> getTestCases() {
	return protectTestCases;
    }

    @Override
    public void setTestCases(ArrayList<TestCase> testCase) {
	this.protectTestCases = testCase;
    }

    @Override
    public String getTitle() {
	return title;
    }

    @Override
    public void setTitle(String title) {
	this.title = title;
    }

    @Override
    public RuleType getType() {
	return protectRuleType;
    }

    @Override
    public void setType(RuleType ruleType) {
	this.protectRuleType = (ProtectRuleType) ruleType;
    }

    @Override
    public String getCategory() {
	return category;
    }

    @Override
    public void setCategory(String category) {
	this.category = category;
    }
}
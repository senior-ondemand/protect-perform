package com.pnp.data;

import java.util.Map;

public class ProtectTestCase implements TestCase {
    private Map<String, String> protectTestCaseAttack;
    private Map<String, String> protectTestCaseVerification;
    private String testCaseName;
    private String testCaseCategory;

    @Override
    public String getTestCaseName() {
	return testCaseName;
    }

    @Override
    public void setTestCaseAttack(Map<String, String> testCaseAttack) {
	this.protectTestCaseAttack = testCaseAttack;
    }

    @Override
    public Map<String, String> getTestCaseVerification() {
	return protectTestCaseVerification;
    }

    @Override
    public void setTestCaseVerification(Map<String, String> testCaseVerification) {
	this.protectTestCaseVerification = testCaseVerification;
    }

    @Override
    public void setTestCaseName(String testCaseName) {
	this.testCaseName = testCaseName;
    }

    @Override
    public Map<String, String> getTestCaseAttack() {
	return protectTestCaseAttack;
    }

    @Override
    public String getTestCaseCategory() {
	return testCaseCategory;
    }

    @Override
    public void setTestCaseCategory(String testCaseCategory) {
	this.testCaseCategory = testCaseCategory;
    }
}

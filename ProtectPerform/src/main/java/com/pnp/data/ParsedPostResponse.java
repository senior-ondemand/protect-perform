package com.pnp.data;

import java.util.ArrayList;

public interface ParsedPostResponse {

    public abstract void setMatchingResult(boolean matchingResult);

    public abstract void setStatusCode(String statusCode);

    public abstract String getStatusCode();

    public abstract void setHeaderResultList(ArrayList<String> headerResultList);

    public abstract ArrayList<String> getHeaderResultList();

    public abstract void setBodyResultList(ArrayList<String> bodyResultList);

    public abstract ArrayList<String> getBodyResultList();

    public abstract void setStatusCodeResult(String statusCodeResult);

    public abstract String getStatusCodeResult();

    public abstract boolean isMatchingResult();

}

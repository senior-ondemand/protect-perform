package com.pnp.data;

public interface ScanResult {

	public String getTargetUrl();

	public void setTargetUrl(String targetUrl);
	
	public String getTestCase();

	public void setTestCase(String testCase);

	public Integer getScore();

	public void setScore(Integer score);

}
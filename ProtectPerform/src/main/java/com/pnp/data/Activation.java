package com.pnp.data;

import org.apache.commons.lang3.StringUtils;

public class Activation {

    public enum Field {
	UserEmail
    }

    private String activationLink = StringUtils.EMPTY;
    private String userEmail = StringUtils.EMPTY;

    public Activation() {

    }

    public Activation(String activationLink, String userEmail) {
	this.activationLink = activationLink;
	this.userEmail = userEmail;
    }

    @Override
    public String toString() {
	StringBuilder stringBuilder = new StringBuilder();
	stringBuilder.append("ActivationLink: ").append(activationLink).append("\n");
	stringBuilder.append("UserEmail: ").append(userEmail).append("\n");

	return stringBuilder.toString();
    }

    public String getActivationLink() {
	return activationLink;
    }

    public void setActivationLink(String activationLink) {
	this.activationLink = activationLink;
    }

    public String getUserEmail() {
	return userEmail;
    }

    public void setUserEmail(String userEmail) {
	this.userEmail = userEmail;
    }

}

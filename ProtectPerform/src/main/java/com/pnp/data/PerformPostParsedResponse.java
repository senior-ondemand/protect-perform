package com.pnp.data;

import java.util.ArrayList;

public class PerformPostParsedResponse implements ParsedPostResponse {

    @Override
    public void setMatchingResult(boolean matchingResult) {
    }

    public void setStatusCode(Integer statusCode) {
    }

    @Override
    public String getStatusCode() {
	return null;
    }

    @Override
    public void setStatusCode(String statusCode) {
    }

    @Override
    public void setHeaderResultList(ArrayList<String> headerResultList) {
    }

    @Override
    public ArrayList<String> getHeaderResultList() {
	return null;
    }

    @Override
    public void setBodyResultList(ArrayList<String> bodyResultList) {
    }

    @Override
    public ArrayList<String> getBodyResultList() {
	return null;
    }

    @Override
    public void setStatusCodeResult(String statusCodeResult) {
    }

    @Override
    public String getStatusCodeResult() {
	return null;
    }

    @Override
    public boolean isMatchingResult() {
	return false;
    }

}

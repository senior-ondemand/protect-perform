package com.pnp.data;

import java.util.ArrayList;

public class ProtectPostParsedResponse implements ParsedPostResponse {
    private boolean matchingResult;
    private String statusCodeResult;
    private ArrayList<String> bodyResultList;
    private ArrayList<String> headerResultList;

    @Override
    public boolean isMatchingResult() {
	return matchingResult;
    }

    @Override
    public void setMatchingResult(boolean matchingResult) {
	this.matchingResult = matchingResult;
    }

    @Override
    public String getStatusCode() {
	return statusCodeResult;
    }

    @Override
    public void setStatusCode(String statusCode) {
	this.statusCodeResult = statusCode;
    }

    @Override
    public String getStatusCodeResult() {
	return statusCodeResult;
    }

    @Override
    public void setStatusCodeResult(String statusCodeResult) {
	this.statusCodeResult = statusCodeResult;
    }

    @Override
    public ArrayList<String> getBodyResultList() {
	return bodyResultList;
    }

    @Override
    public void setBodyResultList(ArrayList<String> bodyResultList) {
	this.bodyResultList = bodyResultList;
    }

    @Override
    public ArrayList<String> getHeaderResultList() {
	return headerResultList;
    }

    @Override
    public void setHeaderResultList(ArrayList<String> headerResultList) {
	this.headerResultList = headerResultList;
    }

}

package com.pnp.data;

import java.util.ArrayList;

import com.pnp.module.RuleType;
import com.pnp.module.perform.PerformRuleType;

public class PerformRule implements Rule {
    private String title;
    private String category;
    private PerformRuleType performRuleType;

    @Override
    public ArrayList<TestCase> getTestCases() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void setTestCases(ArrayList<TestCase> testCase) {
	// TODO Auto-generated method stub

    }

    @Override
    public String getTitle() {
	return title;
    }

    @Override
    public void setTitle(String title) {
	this.title = title;
    }

    @Override
    public RuleType getType() {
	return performRuleType;
    }

    @Override
    public void setType(RuleType ruleType) {
	this.performRuleType = (PerformRuleType) ruleType;
    }

    @Override
    public String getCategory() {
	return category;
    }

    @Override
    public void setCategory(String category) {
	this.category = category;
    }
}
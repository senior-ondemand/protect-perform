package com.pnp.data;

import java.util.ArrayList;

import com.pnp.data.html.FormTag;

public class ProtectPreParsedResponse implements PreParsedResponse {
    ArrayList<FormTag> formTagList;

    public ArrayList<FormTag> getFormTagList() {
	return formTagList;
    }

    public void setFormTagList(ArrayList<FormTag> formTagList) {
	this.formTagList = formTagList;
    }
}
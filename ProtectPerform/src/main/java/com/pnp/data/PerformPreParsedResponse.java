package com.pnp.data;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.jsoup.nodes.Document;

import com.pnp.data.html.ExtResource;
import com.pnp.data.html.ImgInfo;
import com.pnp.data.html.ImgTag;
import com.pnp.data.html.IntResource;

public class PerformPreParsedResponse implements PreParsedResponse {

    private Document document;
    private ArrayList<ImgTag> imgTagList;
    private ImgInfo faviconInfo;
    private String responseBody;
    private HttpResponse targetUrlResponse;
    private ArrayList<ExtResource> extJSResourceList;
    private ArrayList<ExtResource> extCSSResourceList;
    private ArrayList<IntResource> intJSResourceList;
    private ArrayList<IntResource> intCSSResourceList;
    private ArrayList<String> status404UrlList;
    private int inlineCssSize;
    private int inlineJsSize;
    private int extCssAllCount;
    private int extCssHeadCount;
    private int extJsAllCount;
    private int extJsHeadCount;
    private String targetURL;

    public ArrayList<ImgTag> getImgTagList() {
	return imgTagList;
    }

    public void setImgTagList(ArrayList<ImgTag> imgTagList) {
	this.imgTagList = imgTagList;
    }

    public ImgInfo getFaviconInfo() {
	return faviconInfo;
    }

    public void setFaviconInfo(ImgInfo faviconInfo) {
	this.faviconInfo = faviconInfo;
    }

    public String getResponseBody() {
	return responseBody;
    }

    public void setResponseBody(String responseBody) {
	this.responseBody = responseBody;
    }

    public HttpResponse getTargetUrlResponse() {
	return targetUrlResponse;
    }

    public void setTargetUrlResponse(HttpResponse targetUrlResponse) {
	this.targetUrlResponse = targetUrlResponse;
    }

    public Document getDocument() {
	return document;
    }

    public void setDocument(Document document) {
	this.document = document;
    }

    public ArrayList<ExtResource> getExtJsResourceList() {
	return extJSResourceList;
    }

    public void setExtJSResourceList(ArrayList<ExtResource> setExtJSResourceList) {
	this.extJSResourceList = setExtJSResourceList;
    }

    public ArrayList<ExtResource> getExtCssResourceList() {
	return extCSSResourceList;
    }

    public void setExtCSSResourceList(ArrayList<ExtResource> setExtCSSResourceList) {
	this.extCSSResourceList = setExtCSSResourceList;
    }

    public ArrayList<IntResource> getIntJsResourceList() {
	return intJSResourceList;
    }

    public void setIntJsResourceList(ArrayList<IntResource> intJSResourceList) {
	this.intJSResourceList = intJSResourceList;
    }

    public ArrayList<IntResource> getIntCssResourceList() {
	return intCSSResourceList;
    }

    public void setIntCssResourceList(ArrayList<IntResource> intCSSResourceList) {
	this.intCSSResourceList = intCSSResourceList;
    }

    public int getInlineCssSize() {
	return inlineCssSize;
    }

    public void setInlineCssSize(int inlineCSSCount) {
	this.inlineCssSize = inlineCSSCount;
    }

    public int getInlineJsSize() {
	return inlineJsSize;
    }

    public void setInlineJsSize(int inlineJSCount) {
	this.inlineJsSize = inlineJSCount;
    }

    public int getExtCssAllCount() {
	return extCssAllCount;
    }

    public void setExtCssAllCount(int extCSSAllCount) {
	this.extCssAllCount = extCSSAllCount;
    }

    public int getExtCssHeadCount() {
	return extCssHeadCount;
    }

    public void setExtCssHeadCount(int extCSSHeadCount) {
	this.extCssHeadCount = extCSSHeadCount;
    }

    public int getExtJsAllCount() {
	return extJsAllCount;
    }

    public void setExtJsAllCount(int extJSAllCount) {
	this.extJsAllCount = extJSAllCount;
    }

    public int getExtJsHeadCount() {
	return extJsHeadCount;
    }

    public void setExtJsHeadCount(int extJSHeadCount) {
	this.extJsHeadCount = extJSHeadCount;
    }

    public String getTargetURL() {
	return targetURL;
    }

    public void setTargetURL(String targetURL) {
	this.targetURL = targetURL;
    }

    public ArrayList<String> getStatus404UrlList() {
        return status404UrlList;
    }

    public void setStatus404UrlList(ArrayList<String> status404UrlList) {
        this.status404UrlList = status404UrlList;
    }
    
}
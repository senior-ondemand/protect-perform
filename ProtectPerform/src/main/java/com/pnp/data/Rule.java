package com.pnp.data;

import java.util.ArrayList;

import com.pnp.module.RuleType;

public interface Rule {
    public ArrayList<TestCase> getTestCases();

    public void setTestCases(ArrayList<TestCase> testCase);

    public String getTitle();

    public void setTitle(String title);

    public RuleType getType();

    public void setType(RuleType performType);

    public String getCategory();

    public void setCategory(String category);
}
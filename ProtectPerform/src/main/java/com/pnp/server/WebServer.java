package com.pnp.server;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.MultiMap;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;
import org.vertx.java.core.sockjs.EventBusBridgeHook;
import org.vertx.java.platform.Verticle;

import com.pnp.controller.IllegalUserDataException;
import com.pnp.controller.UserController;
import com.pnp.data.Activation;
import com.pnp.data.User;
import com.pnp.service.ScheduleService;
import com.pnp.socketIO.SocketIO;

public class WebServer extends Verticle {

    private static final String LOCAL_HOST = "0:0:0:0:0:0:0:1";

    private static Logger logger = LoggerFactory.getLogger(WebServer.class);

    private static final int WEB_PORT = 80;
    private static final int SOCKET_IO_PORT = 9191;

    private static final String GET_METHOD = "GET";
    private static final String POST_METHOD = "POST";

    private static final String STATUS = "status";
    private static final String OK = "ok";
    private static final String TRUE = "t:";
    private static final String FALSE = "f:server:Contact to Admin";

    private static final String ROOT = "/";
    private static final String WEB_ROOT = "views/";
    private static final String INDEX_FILE = "index.html";

    private static final String SIGN_UP = "/signUp";
    private static final String SIGN_IN = "/signIn";
    private static final String SIGN_OUT = "/signOut";
    private static final String DEACTIVATE = "/deactivate";
    private static final String SETACCOUNT = "/setAccount";
    private static final String ACTIVATE_ACCOUNT = "/activateAccount";

    private static final String USER_EMAIL = "userEmail";
    private static final String USER_PASSWORD = "userPassword";
    private static final String USER_RE_PASSWORD = "userRePassword";
    private static final String USER_NAME = "userName";
    private static final String USER_URL = "userUrl";

    private static final String DATA = "data";

    private UserController userController;
    private SessionHelper sessionHelper;
    private HttpServerRequest request;

    private MultiMap postMap;
    private String filePath;

    @Override
    public void start() {
	userController = new UserController();
	sessionHelper = new SessionHelper(vertx);

	SocketIO socketIOProtect = new SocketIO(vertx, SOCKET_IO_PORT, sessionHelper);
	socketIOProtect.start();

	final HttpServer server = vertx.createHttpServer();
	container.deployModule("com.campudus~session-manager~2.0.1-final", new JsonObject(), 1, new Handler<AsyncResult<String>>() {

	    @Override
	    public void handle(AsyncResult<String> event) {
		logger.info("Session: " + event.succeeded());
		server.requestHandler(new Handler<HttpServerRequest>() {

		    @Override
		    public void handle(HttpServerRequest httpServerRequest) {
			request = httpServerRequest;
			request.expectMultiPart(true);
			request.endHandler(new Handler<Void>() {

			    @Override
			    public void handle(Void event) {
				String requestMethod = request.method();
				filePath = WEB_ROOT;

				switch (requestMethod) {
				case GET_METHOD:
				    get();
				    break;

				case POST_METHOD:
				    post();
				    break;

				default:
				    throw new IllegalArgumentException("吏�썝�섏� �딅뒗 Http Method" + requestMethod);
				}
			    }
			});
		    }
		});

		JsonArray permitted = new JsonArray();
		permitted.add(new JsonObject());

		EventBusBridgeHook hook = new ServerHook(logger);
		vertx.createSockJSServer(server).setHook(hook).bridge(new JsonObject().putString("prefix", "/eventbus"), permitted, permitted);
		server.listen(WEB_PORT);
		logger.info("Server Listening");
	    }
	});
    }

    private void get() {
	String requestPath = request.path();

	switch (requestPath) {
	case ROOT:
	    filePath += INDEX_FILE;
	    request.response().sendFile(filePath);
	    break;

	case ACTIVATE_ACCOUNT:
	    activateAccount();
	    break;

	case "/s":
	    schedule();
	    break;

	default:
	    filePath += requestPath;
	    request.response().sendFile(filePath);
	    break;
	}
    }

    private void schedule() {
	if (request.remoteAddress().getAddress().getHostAddress().equals(LOCAL_HOST)) {
	    logger.info("Schedule");
	    ScheduleService scheduleService = new ScheduleService();
	    scheduleService.startDaily();
	}
	redirect(ROOT);
    }

    private void activateAccount() {
	filePath += INDEX_FILE;
	Activation activation = new Activation();

	String activationLink = request.params().get("activationLink");
	activation.setActivationLink(activationLink);
	logger.info("Activation: " + activation.toString());
	userController.activateAccount(activation);
	redirect(ROOT);
    }

    private void post() {
	logger.info("Post Method");
	setFormAttributes();
	userController.initialize(request, sessionHelper);

	switch (request.path()) {
	case SIGN_UP:
	    signUp();
	    break;

	case SIGN_IN:
	    signIn();
	    break;

	case SIGN_OUT:
	    signOut();
	    break;

	case DEACTIVATE:
	    deactivate();
	    break;

	case SETACCOUNT:
	    setAccount();
	    break;

	default:
	    throw new IllegalArgumentException("�덇� �섏� �딆� Post �붿껌" + request.path());
	}
    }

    private void setFormAttributes() {
	postMap = request.formAttributes();
    }

    private void signUp() {
	String resultString = userController.signUp(generateSignUpUser());
	logger.info("SignUp :" + resultString);
	request.response().end(resultString);
    }

    private User generateSignUpUser() {
	User user = new User();
	user.setEmail(postMap.get(USER_EMAIL));
	user.setPassword(postMap.get(USER_PASSWORD));
	user.setRePassword(postMap.get(USER_RE_PASSWORD));
	user.setName(postMap.get(USER_NAME));
	user.setUrl(postMap.get(USER_URL));

	return user;
    }

    private void signIn() {
	User userFromRedis;
	try {
	    userFromRedis = userController.signIn(generateSignInUser());
	    if (userFromRedis != null) {
		JsonObject userJsonObject = userToJsonObject(userFromRedis);
		sessionHelper.putSessionData(request, DATA, userJsonObject, new Handler<JsonObject>() {

		    @Override
		    public void handle(JsonObject event) {
			if (event.getString(STATUS).equals(OK)) {
			    logger.info("SignIn: true");
			    request.response().end(TRUE);
			} else {
			    logger.info("SignIn: false");
			    request.response().end(FALSE);
			}

			logger.info("SignIn Reulst: " + event);
		    }
		});
	    } else {
		logger.info("SignIn: false");
		request.response().end(FALSE);
	    }
	} catch (IllegalUserDataException e) {
	    logger.info("SignIn :" + e.getMessage());
	    request.response().end(e.getMessage());
	}
    }

    private User generateSignInUser() {
	User user = new User();
	user.setEmail(postMap.get(USER_EMAIL));
	user.setPassword(postMap.get(USER_PASSWORD));

	return user;
    }

    private JsonObject userToJsonObject(User user) {
	JsonObject jsonObject = new JsonObject().putString(USER_EMAIL, user.getEmail()).putString(USER_NAME, user.getName())
		.putString(USER_URL, user.getUrl());

	return jsonObject;

    }

    private void signOut() {
	sessionHelper.destroySession(request, new Handler<JsonObject>() {

	    @Override
	    public void handle(JsonObject event) {
		logger.info("SignOut: " + event);
		redirect(ROOT);
	    }
	});
    }

    private void deactivate() {
	sessionHelper.withSessionData(request, DATA, new Handler<JsonObject>() {

	    @Override
	    public void handle(JsonObject sessionData) {
		User user = new User();
		user.setEmail(sessionData.getObject(DATA).getObject(DATA).getString(USER_EMAIL));
		user.setPassword(postMap.get(USER_PASSWORD));

		boolean result = userController.deactivate(user);

		if (result) {
		    sessionHelper.destroySession(request, new Handler<JsonObject>() {

			@Override
			public void handle(JsonObject event) {
			    logger.info("Deactivate: " + event);
			    redirect(ROOT);
			}
		    });
		} else {
		    logger.info("Deactivate: false");
		    redirect(ROOT);
		}
	    }
	});
    }

    private void setAccount() {
	userController.setAccount(generateSetAccountUser());
    }

    private User generateSetAccountUser() {
	User user = new User();
	user.setEmail(postMap.get(USER_EMAIL));
	user.setPassword(postMap.get(USER_PASSWORD));
	user.setRePassword(postMap.get(USER_RE_PASSWORD));
	user.setName(postMap.get(USER_NAME));
	user.setUrl(postMap.get(USER_URL));

	return user;
    }

    private void redirect(String url) {
	request.response().setStatusCode(301);
	request.response().putHeader("Location", url);
	request.response().sendFile(filePath);
    }

}

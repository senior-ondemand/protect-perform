package com.pnp.service;

import java.util.Map;

import org.vertx.java.core.json.JsonObject;

import com.pnp.dao.PerformRankDAO;
import com.pnp.module.PNPConverter;

public class RankService extends Service {

    private PerformRankDAO performRankDAO;
    private String url;
    private int grade;
    

    public RankService() {
	performRankDAO = new PerformRankDAO();
    }

    public RankService(JsonObject jsonData) {
	performRankDAO = new PerformRankDAO();
	url = PNPConverter.urlToHost(jsonData.getString("url"));
	grade = jsonData.getInteger("grade");
    }

    public Map<String, Integer> getPerformRank(String type) {
	return performRankDAO.getPerformRank(type);
    }

    public void setPerformUserRank() {
	performRankDAO.setPerformRank("USR", url, grade);
    }

}
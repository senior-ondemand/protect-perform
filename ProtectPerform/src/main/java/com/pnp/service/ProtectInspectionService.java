package com.pnp.service;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.UserSetting;
import com.pnp.socketIO.SocketIOCallback;

public class ProtectInspectionService extends InspectionService implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ProtectInspectionService.class);

    public ProtectInspectionService(UserSetting userSetting, SocketIOCallback socketIOCallback, Integer index) {
	super(InspectionType.PROTECT, userSetting, socketIOCallback, index);
    }

    @Override
    public void run() {
	logger.info("ProtectInspectionService Thread Start");

	super.run();
    }

}
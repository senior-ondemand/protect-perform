package com.pnp.service;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import org.mindrot.jbcrypt.BCrypt;

import com.pnp.controller.IllegalUserDataException;
import com.pnp.controller.IllegalUserDataException.UserDataField;
import com.pnp.dao.ActivationDao;
import com.pnp.dao.UserDAO;
import com.pnp.data.Activation;
import com.pnp.data.User;
import com.pnp.data.User.Activity;
import com.pnp.module.DateLib;
import com.pnp.module.EmailLib;
import com.pnp.module.UrlLib;
import com.pnp.module.Validator;

public class UserService extends Service {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    private static final String PNP_HOME = "http://protectnperform.com";
    private static final String SALT;
    private static final int ACTIVATION_LENGTH = 20;

    private final UserDAO userDAO;
    private final ActivationDao activationDao;

    static {
	SALT = BCrypt.gensalt();
    }

    public UserService() {
	userDAO = new UserDAO();
	activationDao = new ActivationDao();
    }

    public boolean signUp(User user) throws IllegalUserDataException {
	boolean signUpResult = false;
	boolean sendActivationEmailResult = false;

	if (Validator.validateSignUpInput(user) && !existsAccount(user)) {
	    String hashPassword = BCrypt.hashpw(user.getPassword(), SALT);
	    user.setPassword(hashPassword);
	    user.setSignUpDateTime(DateLib.currentDateString());
	    String activationLink = generateUniqueActivationLink();
	    user.setActivationLink(activationLink);
	    signUpResult = userDAO.signUp(user);

	    startActivationSession(user);
	    sendActivationEmailResult = sendActivationEmail(user);
	}

	return signUpResult && sendActivationEmailResult;
    }

    public boolean existsAccount(User user) throws IllegalUserDataException {
	if (userDAO.getUserByEmail(user) != null) {
	    throw new IllegalUserDataException(UserDataField.UserEmail, "User Email already Exists");
	}

	return false;
    }

    private String generateUniqueActivationLink() {
	String activationLink = StringUtils.EMPTY;
	Activation activation = new Activation();

	do {
	    activationLink = RandomStringUtils.randomAlphanumeric(ACTIVATION_LENGTH);
	    activation.setActivationLink(activationLink);
	} while (activationDao.existsActivationLink(activation) == true);

	return activationLink;
    }

    private void startActivationSession(User user) {
	Activation activation = new Activation();
	activation.setActivationLink(user.getActivationLink());
	activation.setUserEmail(user.getEmail());
	activationDao.insertActivation(activation);
    }

    private boolean sendActivationEmail(User user) {
	String domain = UrlLib.getHost(user.getUrl());
	String whois = UrlLib.getWhois(domain);
	ArrayList<String> emailList = UrlLib.getEmailList(whois);

	for (String email : emailList) {
	    EmailLib.sendEmailByGmail(email, generateEmailText(user));
	}

	return true;
    }

    private String generateEmailText(User user) {
	StringBuilder stringBuilder = new StringBuilder();
	stringBuilder.append("Click the link below to activate your account:").append("\n");
	stringBuilder.append(PNP_HOME).append("/activateAccount?activationLink=").append(user.getActivationLink()).append("\n\n");
	stringBuilder.append("Your account details:").append("\n");
	stringBuilder.append("Login: ").append(user.getEmail()).append("\n");
	stringBuilder.append("Thank you!").append("\n\n");
	stringBuilder.append("Protect & Perform").append("\n");
	stringBuilder.append("This email is generated automatically, please do not reply.  If you need further assistance, please visit us at:")
		.append("\n");
	stringBuilder.append("http://www.protectnperform.com").append("\n");

	return stringBuilder.toString();
    }

    public User signIn(User user) throws IllegalUserDataException {
	User userFromRedis = userDAO.signIn(user);

	if (Validator.validateSignInInput(user) && isActivatedUser(userFromRedis) && Validator.validateWithStoredPassword(user, userFromRedis)) {
	    return userFromRedis;
	}

	return null;
    }

    private boolean isActivatedUser(User userFromRedis) throws IllegalUserDataException {
	if (userFromRedis == null) {
	    throw new IllegalUserDataException(UserDataField.UserEmail, "Your Account does not Exist");
	} else if (userFromRedis.getActivity().equals(Activity.Activated.toString()) == false) {
	    throw new IllegalUserDataException(UserDataField.UserEmail, "Your Account is not Activated Yet");
	}

	return true;
    }

    public boolean activateAccount(Activation activation) {
	boolean result = false;

	Activation activationFromRedis = activationDao.getActivationByLink(activation);

	User user = new User();
	user.setEmail(activationFromRedis.getUserEmail());
	User userFromRedis = userDAO.getUserByEmail(user);

	if (isValidActivation(activationFromRedis) && isValidUser(userFromRedis, activationFromRedis)) {
	    activationDao.delete(activationFromRedis);

	    userFromRedis.setActivity(Activity.Activated.toString());
	    userFromRedis.setActivationDateTime(DateLib.currentDateString());
	    result = userDAO.setAccount(userFromRedis);
	}

	return result;
    }

    private boolean isValidActivation(Activation activationFromRedis) {
	boolean result = true;

	if (activationFromRedis == null) {
	    result = false;
	}

	return result;
    }

    private boolean isValidUser(User userFromRedis, Activation activationFromRedis) {
	boolean result = true;

	if (userFromRedis == null) {
	    result = false;
	}

	if (!userFromRedis.getActivity().equals(Activity.Inactivated.toString())) {
	    result = false;
	}

	if (!userFromRedis.getActivationLink().equals(activationFromRedis.getActivationLink())) {
	    result = false;
	}

	return result;
    }

    public boolean setAccount(User user) throws IllegalUserDataException {
	boolean result = false;

	if (Validator.validateSignUpInput(user)) {
	    String hashPassword = BCrypt.hashpw(user.getPassword(), SALT);
	    user.setPassword(hashPassword);

	    result = userDAO.setAccount(user);
	}

	return result;
    }

    public boolean deactivate(User user) {
	boolean result = false;

	User userFromRedis = userDAO.getUserByEmail(user);

	if (BCrypt.checkpw(user.getPassword(), userFromRedis.getPassword())) {
	    result = userDAO.deactivate(user);
	}

	return result;
    }
}
package com.pnp.service;

public enum InspectionType {
    PROTECT, PERFORM
}
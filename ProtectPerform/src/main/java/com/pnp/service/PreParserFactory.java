package com.pnp.service;

import com.pnp.module.parser.PerformPreParser;
import com.pnp.module.parser.PreParser;
import com.pnp.module.parser.ProtectPreParser;

public class PreParserFactory {

    public static PreParser create(InspectionType inspectionType) {
	switch (inspectionType) {
	case PROTECT:
	    return ProtectPreParser.create();
	case PERFORM:
	    return PerformPreParser.create();

	default:
	    throw new IllegalArgumentException("잘못된 카테고리");
	}
    }

}
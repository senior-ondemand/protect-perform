package com.pnp.service;

import java.util.ArrayList;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.dao.IndexDAO;
import com.pnp.dao.UserDAO;
import com.pnp.data.User;
import com.pnp.data.User.Interval;
import com.pnp.data.UserSetting;
import com.pnp.module.JsonObjectGenerator;
import com.pnp.module.PNPConverter;

public class ScheduleService {

    private static Logger logger = LoggerFactory.getLogger(ScheduleService.class);

    private final UserDAO userDAO;

    public ScheduleService() {
	userDAO = new UserDAO();
    }

    public int startDaily() {
	return start(Interval.Daily);
    }

    public int startWeekly() {
	return start(Interval.Weekly);
    }

    public int startMonthly() {
	return start(Interval.Monthly);
    }

    private int start(Interval interval) {
	ArrayList<User> scheduleUserList = userDAO.getScheduleJobByInterval(interval);
	for (User user : scheduleUserList) {
	    JsonObjectGenerator jsonObjectGenerator = new JsonObjectGenerator();
	    UserSetting userSetting = PNPConverter.JsonToUserSetting(jsonObjectGenerator.createWithAllTestCaseInProtectAndPerform());
	    // userSetting.setTargetURL(user.getUrl());
	    userSetting.setTargetURL("http://www.naver.com");
	    userSetting.setUserEmail(user.getEmail());
	    logger.info("UserSetting: " + userSetting);

	    Integer index = 1;
	    IndexDAO indexDAO = new IndexDAO();
		index = indexDAO.getIndex("LOG:" + userSetting.getTargetURL() + ":*");


	    // ProtectInspectionService(userSetting, socketIOCallback);
	    InspectionService performInsepctionService = new PerformInspectionService(userSetting, null, index);

	    // new Thread((Runnable) protectInspectionService).start();
	    new Thread((Runnable) performInsepctionService).start();
	}

	return scheduleUserList.size();
    }

}

package com.pnp.service;

import java.util.Map;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.dao.IndexDAO;
import com.pnp.dao.InspectionDAO;
import com.pnp.dao.InspectionDAOFactory;
import com.pnp.dao.ScanResultDAO;
import com.pnp.dao.ScanResultDAOFactory;
import com.pnp.data.PerformPreParsedResponse;
import com.pnp.data.Rule;
import com.pnp.data.UserSetting;
import com.pnp.module.Inspection;
import com.pnp.module.InspectionFactory;
import com.pnp.module.PNPConverter;
import com.pnp.module.parser.PreParser;
import com.pnp.socketIO.SocketIOCallback;

public abstract class InspectionService extends Service {

    private static Logger logger = LoggerFactory.getLogger(InspectionService.class);

    private final UserSetting userSetting;
    private final InspectionDAO inspectionDAO;
    private final ScanResultDAO scanResultDAO;
    private final IndexDAO indexDAO;
    private final PreParser preParser;
    private final InspectionType inspectionType;
    private Map<String, Rule> rules;
    private final SocketIOCallback socketIOCallback;
    private final Integer index;

    public InspectionService(InspectionType inspectionType, UserSetting userSetting, SocketIOCallback socketIOCallback, Integer index) {
	this.inspectionType = inspectionType;
	this.userSetting = userSetting;
	this.socketIOCallback = socketIOCallback;
	this.index = index;

	indexDAO = new IndexDAO();
	scanResultDAO = ScanResultDAOFactory.create(inspectionType);
	inspectionDAO = InspectionDAOFactory.create(inspectionType);
	preParser = PreParserFactory.create(inspectionType);
	preParser.setTargetURL(userSetting.getTargetURL());
    }

    protected void run() {
	logger.info("InspectionService Run");
	rules = inspectionDAO.get(userSetting);
	preParser.preParseResponse();

	if (inspectionType == InspectionType.PERFORM) {
	    if (socketIOCallback != null)
		socketIOCallback.emit("contentsList", PNPConverter.contentsListToJson((PerformPreParsedResponse) preParser.getParsedResponse()));

	    scanResultDAO.setContents((PerformPreParsedResponse) preParser.getParsedResponse());
	}

	inspectTargetURL(index);
    }

    private void inspectTargetURL(Integer index) {
	logger.info("InspectionTargetUrl Start");
	for (String key : rules.keySet()) {
	    Inspection inspection = InspectionFactory.create(inspectionType);

	    inspection.initialize(index, userSetting, rules.get(key), preParser.getParsedResponse(), socketIOCallback, scanResultDAO);
	    inspection.exe();
	}
    }
}
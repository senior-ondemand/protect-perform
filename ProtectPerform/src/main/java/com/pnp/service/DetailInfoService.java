package com.pnp.service;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import org.vertx.java.core.json.JsonObject;

import com.pnp.dao.DetailInfoDAO;
import com.pnp.dao.DetailInfoDAOFactory;
import com.pnp.data.DetailInfo;
import com.pnp.module.PNPConverter;

public class DetailInfoService extends Service {
    private static Logger logger = LoggerFactory.getLogger(DetailInfoService.class);

    private final String category;
    private final InspectionType inspectionType;
    private final DetailInfoDAO detailInfoDAO;

    public DetailInfoService(JsonObject jsonData) {
	this.category = PNPConverter.JsonToCategory(jsonData);
	this.inspectionType = PNPConverter.JsonToType(jsonData);
	this.detailInfoDAO = DetailInfoDAOFactory.create(inspectionType);
    }

    public DetailInfo getDetailInfo() {
	return detailInfoDAO.get(category);
    }

    public InspectionType getInspectionType() {
	return inspectionType;
    }
}
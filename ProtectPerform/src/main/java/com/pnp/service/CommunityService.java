package com.pnp.service;

import java.util.ArrayList;
import java.util.Map;

import org.vertx.java.core.json.JsonObject;

import com.pnp.dao.PNPCategoriesDAO;
import com.pnp.dao.PNPSearchDAO;
import com.pnp.data.Rule;
import com.pnp.data.SearchInputData;
import com.pnp.data.SearchResultData;
import com.pnp.module.PNPConverter;

public class CommunityService extends Service {

    private PNPCategoriesDAO pnpCategoriesDAO;
    private PNPSearchDAO pnpSearchDAO;
    private SearchInputData searchInputData;

    public CommunityService() {
	pnpCategoriesDAO = new PNPCategoriesDAO();
    }

    public CommunityService(JsonObject jsonData) {
	pnpSearchDAO = new PNPSearchDAO();
	searchInputData = PNPConverter.JsonToSearchInputData(jsonData);
    }

    public Map<String, Rule> getProtectCategories() {
	return pnpCategoriesDAO.getProtectCategories();
    }

    public Map<String, Rule> getPerformCategories() {
	return pnpCategoriesDAO.getPerformCategories();
    }

    public ArrayList<SearchResultData> getSearchData() {
	if (searchInputData.getSearchType().equalsIgnoreCase("Title"))
	    return pnpSearchDAO.searchInTitle(searchInputData.getKeyword());
	else if (searchInputData.getSearchType().equalsIgnoreCase("Category"))
	    return pnpSearchDAO.searchInCategory(searchInputData.getKeyword());
	else
	    return null;
    }

}
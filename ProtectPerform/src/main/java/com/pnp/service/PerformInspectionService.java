package com.pnp.service;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.data.UserSetting;
import com.pnp.socketIO.SocketIOCallback;

public class PerformInspectionService extends InspectionService implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(PerformInspectionService.class);

    public PerformInspectionService(UserSetting userSetting, SocketIOCallback socketIOCallback, Integer index) {
	super(InspectionType.PERFORM, userSetting, socketIOCallback, index);
    }

    @Override
    public void run() {
	logger.info("PerformInsepctionService Thread Start");

	super.run();
    }

}
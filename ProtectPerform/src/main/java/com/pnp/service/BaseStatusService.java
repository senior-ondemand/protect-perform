package com.pnp.service;

import java.util.Date;
import java.util.Map;

import org.vertx.java.core.logging.Logger;
import org.vertx.java.core.logging.impl.LoggerFactory;

import com.pnp.common.Common;
import com.pnp.dao.TestCaseDAO;
import com.pnp.data.BaseStatus;
import com.pnp.data.UserSetting;

public class BaseStatusService extends Service {
    private static Logger logger = LoggerFactory.getLogger(BaseStatusService.class);
    private final UserSetting userSetting;
    private final TestCaseDAO totalTestCaseDAO;

    public BaseStatusService(UserSetting userSetting) {
	this.userSetting = userSetting;
	this.totalTestCaseDAO = new TestCaseDAO();
    }

    public BaseStatus getBaseStatus() {
	BaseStatus baseStatus = new BaseStatus();

	Map<String, Integer> numTotalCases = totalTestCaseDAO.getNumOfTotalTestCases(userSetting);
	Date date = new Date();
	String serverInfo = "";
	String start = date.toString();

	serverInfo = Common.getServerInfoFromURL(userSetting.getTargetURL());

	baseStatus.setServerInfo(serverInfo);
	baseStatus.setStart(start);
	baseStatus.setPerformNumTestCases(numTotalCases.get("perform"));
	baseStatus.setProtectNumTestCases(numTotalCases.get("protect"));

	return baseStatus;
    }
}